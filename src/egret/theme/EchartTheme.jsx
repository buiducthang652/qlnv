import echarts from "echarts";

export const EchartTheme = (MuiTheme) => ({
  backgroundColor: MuiTheme.palette.background.paper,
});
