import React from "react";
import { Dialog, Button, DialogActions } from "@material-ui/core";

const ConfirmationDialog = ({
  open,
  onConfirmDialogClose,
  text,
  title = "confirm",
  onYesClick,
  Yes,
  No,
}) => {
  return (
    <Dialog
      maxWidth="xs"
      fullWidth={true}
      open={open}
      onClose={onConfirmDialogClose}
    >
      <div className="pt-24 px-20 pb-8">
        <h4 className="capitalize">{title}</h4>
        {text
          .split("\n")
          .filter((txt) => txt.trim() !== "")
          .map((txt, index) => (
            <p className="p-0 m-0">{txt}</p>
          ))}
        <DialogActions>
          <div className="flex flex-center w-100 flex-middle">
            {Yes && (
              <Button onClick={onYesClick} variant="contained" color="primary">
                {Yes}
              </Button>
            )}
            {No && (
              <Button
                onClick={onConfirmDialogClose}
                className={Yes ? "ml-16" : ""}
                variant="contained"
                color="secondary"
              >
                {No}
              </Button>
            )}
          </div>
        </DialogActions>
      </div>
    </Dialog>
  );
};

export default ConfirmationDialog;
