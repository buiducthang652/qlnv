import React from "react";
import { Redirect } from "react-router-dom";
import sessionRoutes from "./views/sessions/SessionRoutes";
import dashboardRoutes from "./views/dashboard/DashboardRoutes";
import EmployeeRoutes from "./views/employee/EmployeeRoutes";
import ApprovedRoutes from "./views/approved/ApprovedRoutes";
import DevelopmentRoutes from "./views/development/EmployeeRoutes";
import EndEmployeeRoutes from "./views/endEmployee/EmployeeRoutes";
import ConstantList from "./appConfig";

const redirectRoute = [
  {
    path: ConstantList.ROOT_PATH,
    exact: true,
    component: () => <Redirect to={ConstantList.HOME_PAGE} />, //Luôn trỏ về HomePage được khai báo trong appConfig
  },
];

const errorRoute = [
  {
    component: () => <Redirect to={ConstantList.ROOT_PATH + "session/404"} />,
  },
];

const routes = [
  ...sessionRoutes,
  ...dashboardRoutes,
  ...EmployeeRoutes,
  ...ApprovedRoutes,
  ...DevelopmentRoutes,
  ...EndEmployeeRoutes,
  ...errorRoute,
];

export default routes;
