export const GET_FAMILY = "GET_FAMILY";
export const GET_FAMILY_SUCCEEDED = "GET_FAMILY_SUCCEEDED";

export const GET_FAMILY_BY_ID = "GET_FAMILY_BY_ID";
export const GET_FAMILY_BY_ID_SUCCEEDED = "GET_FAMILY_BY_ID_SUCCEEDED";

export const SAVE_FAMILY = "SAVE_FAMILY";
export const SAVE_FAMILY_SUCCEEDED = "SAVE_FAMILY_SUCCEEDED";

export const UPDATE_FAMILY = "UPDATE_FAMILY";
export const UPDATE_FAMILY_SUCCEEDED = "UPDATE_FAMILY_SUCCEEDED";

export const DELETE_FAMILY = "DELETE_FAMILY";
export const DELETE_FAMILY_SUCCEEDED = "DELETE_FAMILY_SUCCEEDED";

export const getFamily = (payload) => {
  return {
    type: GET_FAMILY,
    payload,
  };
};

export const getFamilySucceeded = (payload) => {
  return {
    type: GET_FAMILY_SUCCEEDED,
    payload,
  };
};

export const getFamilyById = (payload) => {
  return {
    type: GET_FAMILY_BY_ID,
    payload,
  };
};

export const getFamilyByIdSucceeded = (payload) => {
  return {
    type: GET_FAMILY_BY_ID_SUCCEEDED,
    payload,
  };
};

export const saveFamily = (id, payload) => {
  return {
    type: SAVE_FAMILY,
    id,
    payload,
  };
};

export const saveFamilySucceeded = (payload) => {
  return {
    type: SAVE_FAMILY_SUCCEEDED,
    payload,
  };
};

export const updateFamily = (payload) => {
  return {
    type: UPDATE_FAMILY,
    payload,
  };
};

export const updateFamilySucceeded = (payload) => {
  return {
    type: UPDATE_FAMILY_SUCCEEDED,
    payload,
  };
};

export const deleteFamily = (payload) => {
  return {
    type: DELETE_FAMILY,
    payload,
  };
};

export const deleteFamilySucceeded = (payload) => {
  return {
    type: DELETE_FAMILY_SUCCEEDED,
    payload,
  };
};
