export const GET_CERTIFICATE = "GET_CERTIFICATE";
export const GET_CERTIFICATE_SUCCEEDED = "GET_CERTIFICATE_SUCCEEDED";

export const GET_CERTIFICATE_BY_ID = "GET_CERTIFICATE_BY_ID";
export const GET_CERTIFICATE_BY_ID_SUCCEEDED =
  "GET_CERTIFICATE_BY_ID_SUCCEEDED";

export const SAVE_CERTIFICATE = "SAVE_CERTIFICATE";
export const SAVE_CERTIFICATE_SUCCEEDED = "SAVE_CERTIFICATE_SUCCEEDED";

export const UPDATE_CERTIFICATE = "UPDATE_CERTIFICATE";
export const UPDATE_CERTIFICATE_SUCCEEDED = "UPDATE_CERTIFICATE_SUCCEEDED";

export const DELETE_CERTIFICATE = "DELETE_CERTIFICATE";
export const DELETE_CERTIFICATE_SUCCEEDED = "DELETE_CERTIFICATE_SUCCEEDED";

export const getCertificate = (payload) => {
  return {
    type: GET_CERTIFICATE,
    payload,
  };
};

export const getCertificateSucceeded = (payload) => {
  return {
    type: GET_CERTIFICATE_SUCCEEDED,
    payload,
  };
};

export const getCertificateById = (payload) => {
  return {
    type: GET_CERTIFICATE_BY_ID,
    payload,
  };
};

export const getCertificateByIdSucceeded = (payload) => {
  return {
    type: GET_CERTIFICATE_BY_ID_SUCCEEDED,
    payload,
  };
};

export const saveCertificate = (id, payload) => {
  return {
    type: SAVE_CERTIFICATE,
    id,
    payload,
  };
};

export const saveCertificateSucceeded = (payload) => {
  return {
    type: SAVE_CERTIFICATE_SUCCEEDED,
    payload,
  };
};

export const updateCertificate = (payload) => {
  return {
    type: UPDATE_CERTIFICATE,
    payload,
  };
};

export const updateCertificateSucceeded = (payload) => {
  return {
    type: UPDATE_CERTIFICATE_SUCCEEDED,
    payload,
  };
};

export const deleteCertificate = (payload) => {
  return {
    type: DELETE_CERTIFICATE,
    payload,
  };
};

export const deleteCertificateSucceeded = (payload) => {
  return {
    type: DELETE_CERTIFICATE_SUCCEEDED,
    payload,
  };
};
