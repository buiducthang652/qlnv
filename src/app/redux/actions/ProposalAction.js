export const GET_BY_CURRENT_LEADER_PROPOSAL = "GET_BY_CURRENT_LEADER_PROPOSAL";
export const GET_BY_CURRENT_LEADER_PROPOSAL_SUCCEEDED =
  "GET_BY_CURRENT_LEADER_PROPOSAL_SUCCEEDED";

export const getByCurrentLeaderProposal = () => {
  return {
    type: GET_BY_CURRENT_LEADER_PROPOSAL,
  };
};

export const getByCurrentLeaderProposalSucceeded = (payload) => {
  return {
    type: GET_BY_CURRENT_LEADER_PROPOSAL_SUCCEEDED,
    payload,
  };
};

export const SAVE_PROPOSAL = "SAVE_PROPOSAL";
export const SAVE_PROPOSAL_SUCCEEDED = "SAVE_PROPOSAL_SUCCEEDED";

export const saveProposal = (id, payload) => {
  return {
    type: SAVE_PROPOSAL,
    id,
    payload,
  };
};

export const saveProposalSucceeded = (payload) => {
  return {
    type: SAVE_PROPOSAL_SUCCEEDED,
    payload,
  };
};

export const GET_BY_EMPLOYEE_ID_PROPOSAL = "GET_BY_EMPLOYEE_ID_PROPOSAL";
export const GET_BY_EMPLOYEE_ID_PROPOSAL_SUCCEEDED =
  "GET_BY_EMPLOYEE_ID_PROPOSAL_SUCCEEDED";

export const getByEmployeeIdProposal = (payload) => {
  return {
    type: GET_BY_EMPLOYEE_ID_PROPOSAL,
    payload,
  };
};

export const getByEmployeeIdProposalSucceeded = (payload) => {
  return {
    type: GET_BY_EMPLOYEE_ID_PROPOSAL_SUCCEEDED,
    payload,
  };
};

export const DELETE_PROPOSAL = "DELETE_PROPOSAL";
export const DELETE_PROPOSAL_SUCCEEDED = "DELETE_PROPOSAL_SUCCEEDED";

export const deleteProposal = (payload) => {
  return {
    type: DELETE_PROPOSAL,
    payload,
  };
};

export const deleteProposalSucceeded = (payload) => {
  return {
    type: DELETE_PROPOSAL_SUCCEEDED,
    payload,
  };
};

export const UPDATE_PROPOSAL = "UPDATE_PROPOSAL";
export const UPDATE_PROPOSAL_SUCCEEDED = "UPDATE_PROPOSAL_SUCCEEDED";

export const updateProposal = (payload) => {
  return {
    type: UPDATE_PROPOSAL,
    payload,
  };
};

export const updateProposalSucceeded = (payload) => {
  return {
    type: UPDATE_PROPOSAL_SUCCEEDED,
    payload,
  };
};
