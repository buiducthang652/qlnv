export const GET_ALL_LEADER = "GET_ALL_LEADER";
export const GET_ALL_LEADER_SUCCEEDED = "GET_ALL_LEADER_SUCCEEDED";

export const getAllLeader = () => {
  return {
    type: GET_ALL_LEADER,
  };
};

export const getAllLeaderSucceeded = (payload) => {
  return {
    type: GET_ALL_LEADER_SUCCEEDED,
    payload,
  };
};
