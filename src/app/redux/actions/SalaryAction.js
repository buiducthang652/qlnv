export const GET_BY_CURRENT_LEADER_SALARY = "GET_BY_CURRENT_LEADER_SALARY";
export const GET_BY_CURRENT_LEADER_SALARY_SUCCEEDED =
  "GET_BY_CURRENT_LEADER_SALARY_SUCCEEDED";

export const getByCurrentLeaderSalary = () => {
  return {
    type: GET_BY_CURRENT_LEADER_SALARY,
  };
};

export const getByCurrentLeaderSalarySucceeded = (payload) => {
  return {
    type: GET_BY_CURRENT_LEADER_SALARY_SUCCEEDED,
    payload,
  };
};

export const SAVE_SALARY = "SAVE_SALARY";
export const SAVE_SALARY_SUCCEEDED = "SAVE_SALARY_SUCCEEDED";

export const saveSalary = (id, payload) => {
  return {
    type: SAVE_SALARY,
    id,
    payload,
  };
};

export const saveSalarySucceeded = (payload) => {
  return {
    type: SAVE_SALARY_SUCCEEDED,
    payload,
  };
};

export const GET_BY_EMPLOYEE_ID = "GET_BY_EMPLOYEE_ID";
export const GET_BY_EMPLOYEE_ID_SUCCEEDED = "GET_BY_EMPLOYEE_ID_SUCCEEDED";

export const getByEmployeeIdSalary = (payload) => {
  return {
    type: GET_BY_EMPLOYEE_ID,
    payload,
  };
};

export const getByEmployeeIdSucceeded = (payload) => {
  return {
    type: GET_BY_EMPLOYEE_ID_SUCCEEDED,
    payload,
  };
};

export const DELETE_SALARY = "DELETE_SALARY";
export const DELETE_SALARY_SUCCEEDED = "DELETE_SALARY_SUCCEEDED";

export const deleteSalary = (payload) => {
  return {
    type: DELETE_SALARY,
    payload,
  };
};

export const deleteSalarySucceeded = (payload) => {
  return {
    type: DELETE_SALARY_SUCCEEDED,
    payload,
  };
};

export const UPDATE_SALARY = "UPDATE_SALARY";
export const UPDATE_SALARY_SUCCEEDED = "UPDATE_SALARY_SUCCEEDED";

export const updateSalary = (payload) => {
  return {
    type: UPDATE_SALARY,
    payload,
  };
};

export const updateSalarySucceeded = (payload) => {
  return {
    type: UPDATE_SALARY_SUCCEEDED,
    payload,
  };
};
