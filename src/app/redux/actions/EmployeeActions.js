export const SEARCH_EMPLOYEE = "SEARCH_EMPLOYEE";
export const SEARCH_EMPLOYEE_SUCCEEDED = "SEARCH_EMPLOYEE_SUCCEEDED";

export const SAVE_EMPLOYEE = "SAVE_EMPLOYEE";
export const SAVE_EMPLOYEE_SUCCEEDED = "SAVE_EMPLOYEE_SUCCEEDED";

export const UPDATE_EMPLOYEE = "UPDATE_EMPLOYEE";
export const UPDATE_EMPLOYEE_SUCCEEDED = "UPDATE_EMPLOYEE_SUCCEEDED";

export const UPDATE_EMPLOYEE_DELETE = "UPDATE_EMPLOYEE_DELETE";
export const UPDATE_EMPLOYEE_DELETE_SUCCEEDED =
  "UPDATE_EMPLOYEE_DELETE_SUCCEEDED";

export const DELETE_EMPLOYEE = "DELETE_EMPLOYEE";
export const DELETE_EMPLOYEE_SUCCEEDED = "DELETE_EMPLOYEE_SUCCEEDED";

export const GET_EMPLOYEE_BY_ID = "GET_EMPLOYEE_BY_ID";
export const GET_EMPLOYEE_BY_ID_SUCCEEDED = "GET_EMPLOYEE_BY_ID_SUCCEEDED";

export const searchEmployees = (payload) => {
  return {
    type: SEARCH_EMPLOYEE,
    payload,
  };
};

export const searchEmployeesSucceeded = (payload) => {
  return {
    type: SEARCH_EMPLOYEE_SUCCEEDED,
    payload,
  };
};

export const saveEmployees = (payload) => {
  return {
    type: SAVE_EMPLOYEE,
    payload,
  };
};

export const saveEmployeeSucceeded = (payload) => {
  return {
    type: SAVE_EMPLOYEE_SUCCEEDED,
    payload,
  };
};

export const updateEmployees = (payload) => {
  return {
    type: UPDATE_EMPLOYEE,
    payload,
  };
};

export const updateEmployeeSucceeded = (payload) => {
  return {
    type: UPDATE_EMPLOYEE_SUCCEEDED,
    payload,
  };
};

export const updateEmployeeDelete = (payload) => {
  return {
    type: UPDATE_EMPLOYEE_DELETE,
    payload,
  };
};

export const updateEmployeeDeleteSucceeded = (payload) => {
  return {
    type: UPDATE_EMPLOYEE_DELETE_SUCCEEDED,
    payload,
  };
};

export const deleteEmployees = (payload) => {
  return {
    type: DELETE_EMPLOYEE,
    payload,
  };
};

export const deleteEmployeeSucceeded = (payload) => {
  return {
    type: DELETE_EMPLOYEE_SUCCEEDED,
    payload,
  };
};

export const getEmployeeById = (payload) => {
  return {
    type: GET_EMPLOYEE_BY_ID,
    payload,
  };
};

export const getEmployeeByIdSucceeded = (payload) => {
  return {
    type: GET_EMPLOYEE_BY_ID_SUCCEEDED,
    payload,
  };
};
