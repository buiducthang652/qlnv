export const GET_BY_CURRENT_LEADER_PROCESS = "GET_BY_CURRENT_LEADER_PROCESS";
export const GET_BY_CURRENT_LEADER_PROCESS_SUCCEEDED =
  "GET_BY_CURRENT_LEADER_PROCESS_SUCCEEDED";

export const getByCurrentLeaderProcess = () => {
  return {
    type: GET_BY_CURRENT_LEADER_PROCESS,
  };
};

export const getByCurrentLeaderProcessSucceeded = (payload) => {
  return {
    type: GET_BY_CURRENT_LEADER_PROCESS_SUCCEEDED,
    payload,
  };
};

export const SAVE_PROCESS = "SAVE_PROCESS";
export const SAVE_PROCESS_SUCCEEDED = "SAVE_PROCESS_SUCCEEDED";

export const saveProcess = (id, payload) => {
  return {
    type: SAVE_PROCESS,
    id,
    payload,
  };
};

export const saveProcessSucceeded = (payload) => {
  return {
    type: SAVE_PROCESS_SUCCEEDED,
    payload,
  };
};

export const GET_BY_EMPLOYEE_ID_PROCESS = "GET_BY_EMPLOYEE_ID_PROCESS";
export const GET_BY_EMPLOYEE_ID_PROCESS_SUCCEEDED =
  "GET_BY_EMPLOYEE_ID_PROCESS_SUCCEEDED";

export const getByEmployeeIdProcess = (payload) => {
  return {
    type: GET_BY_EMPLOYEE_ID_PROCESS,
    payload,
  };
};

export const getByEmployeeIdProcessSucceeded = (payload) => {
  return {
    type: GET_BY_EMPLOYEE_ID_PROCESS_SUCCEEDED,
    payload,
  };
};

export const DELETE_PROCESS = "DELETE_PROCESS";
export const DELETE_PROCESS_SUCCEEDED = "DELETE_PROCESS_SUCCEEDED";

export const deleteProcess = (payload) => {
  return {
    type: DELETE_PROCESS,
    payload,
  };
};

export const deleteProcessSucceeded = (payload) => {
  return {
    type: DELETE_PROCESS_SUCCEEDED,
    payload,
  };
};

export const UPDATE_PROCESS = "UPDATE_PROCESS";
export const UPDATE_PROCESS_SUCCEEDED = "UPDATE_PROCESS_SUCCEEDED";

export const updateProcess = (payload) => {
  return {
    type: UPDATE_PROCESS,
    payload,
  };
};

export const updateProcessSucceeded = (payload) => {
  return {
    type: UPDATE_PROCESS_SUCCEEDED,
    payload,
  };
};
