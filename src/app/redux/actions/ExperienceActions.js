export const SAVE_EXPERIENCE = "SAVE_EXPERIENCE";
export const SAVE_EXPERIENCE_SUCCEEDED = "SAVE_EXPERIENCE_SUCCEEDED";

export const saveExperience = (id, payload) => {
  return {
    type: SAVE_EXPERIENCE,
    id,
    payload,
  };
};

export const saveExperienceSucceeded = (payload) => {
  return {
    type: SAVE_EXPERIENCE_SUCCEEDED,
    payload,
  };
};

export const GET_EXPERIENCE = "GET_EXPERIENCE";
export const GET_EXPERIENCE_SUCCEEDED = "GET_EXPERIENCE_SUCCEEDED";

export const getExperience = (payload) => {
  return {
    type: GET_EXPERIENCE,
    payload,
  };
};

export const getExperienceSucceeded = (payload) => {
  return {
    type: GET_EXPERIENCE_SUCCEEDED,
    payload,
  };
};

export const DELETE_EXPERIENCE = "DELETE_EXPERIENCE";
export const DELETE_EXPERIENCE_SUCCEEDED = "DELETE_EXPERIENCE_SUCCEEDED";

export const deleteExperience = (payload) => {
  return {
    type: DELETE_EXPERIENCE,
    payload,
  };
};

export const deleteExperienceSucceeded = (payload) => {
  return {
    type: DELETE_EXPERIENCE_SUCCEEDED,
    payload,
  };
};

export const UPDATE_EXPERIENCE = "UPDATE_EXPERIENCE";
export const UPDATE_EXPERIENCE_SUCCEEDED = "UPDATE_EXPERIENCE_SUCCEEDED";

export const updateExperience = (payload) => {
  return {
    type: UPDATE_EXPERIENCE,
    payload,
  };
};

export const updateExperienceSucceeded = (payload) => {
  return {
    type: UPDATE_EXPERIENCE_SUCCEEDED,
    payload,
  };
};
