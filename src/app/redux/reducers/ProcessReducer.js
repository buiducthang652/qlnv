import {
  GET_BY_CURRENT_LEADER_PROCESS_SUCCEEDED,
  GET_BY_EMPLOYEE_ID_PROCESS_SUCCEEDED,
  SAVE_PROCESS_SUCCEEDED,
  DELETE_PROCESS_SUCCEEDED,
  UPDATE_PROCESS_SUCCEEDED,
} from "../actions/ProcessAction";

const initialState = {
  processList: [],
};

const ProcessReducer = function (state = initialState, action) {
  switch (action.type) {
    case GET_BY_CURRENT_LEADER_PROCESS_SUCCEEDED: {
      return {
        ...state,
        processList: action.payload,
      };
    }

    case GET_BY_EMPLOYEE_ID_PROCESS_SUCCEEDED: {
      return {
        ...state,
        processList: action.payload,
      };
    }

    case SAVE_PROCESS_SUCCEEDED: {
      return {
        ...state,
        processList: [...action.payload, ...state.processList],
      };
    }

    case DELETE_PROCESS_SUCCEEDED: {
      const newProcess = state.processList.filter(
        (item) => item.id !== action.payload
      );

      return {
        ...state,
        processList: newProcess,
      };
    }

    case UPDATE_PROCESS_SUCCEEDED: {
      if (
        action.payload?.processStatus === "5" ||
        action.payload?.processStatus === "3" ||
        action.payload?.processStatus === "4"
      ) {
        const newProcess = state.processList.filter(
          (item) => item.id !== action.payload?.id
        );

        return {
          ...state,
          processList: newProcess,
        };
      } else {
        const newProcess = state.processList.map((item) =>
          item.id === action.payload?.id ? action.payload : item
        );

        return {
          ...state,
          processList: newProcess,
        };
      }
    }

    default: {
      return state;
    }
  }
};

export default ProcessReducer;
