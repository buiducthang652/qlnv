import {
  SEARCH_EMPLOYEE_SUCCEEDED,
  SAVE_EMPLOYEE_SUCCEEDED,
  UPDATE_EMPLOYEE_SUCCEEDED,
  UPDATE_EMPLOYEE_DELETE_SUCCEEDED,
  DELETE_EMPLOYEE_SUCCEEDED,
  GET_EMPLOYEE_BY_ID_SUCCEEDED,
} from "../actions/EmployeeActions";

const initialState = {
  employeeList: [],
  totalElements: null,
  employee: [],
};

const EmployeeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_EMPLOYEE_SUCCEEDED:
      return {
        ...state,
        employeeList: action?.payload?.data,
        totalElements: action?.payload?.totalElements,
      };
    case GET_EMPLOYEE_BY_ID_SUCCEEDED:
      return {
        ...state,
        employee: action?.payload,
      };
    case SAVE_EMPLOYEE_SUCCEEDED:
      const newEmployeeList = Array.isArray(action.payload)
        ? action.payload
        : [action.payload, ...state.employeeList];

      return {
        ...state,
        employeeList: newEmployeeList,
        employee: action.payload,
        totalElements:
          state.totalElements != null ? state.totalElements + 1 : 1,
      };

    case UPDATE_EMPLOYEE_SUCCEEDED:
      const updateEmployeeList = state.employeeList.map((item) =>
        item.id === action.payload?.id ? action.payload : item
      );

      return {
        ...state,
        employeeList: updateEmployeeList,
        employee: action?.payload,
      };

    case UPDATE_EMPLOYEE_DELETE_SUCCEEDED:
      const updateEmployeeDeleteList = state.employeeList.filter(
        (item) => item.id !== action.payload?.id
      );

      return {
        ...state,
        employeeList: updateEmployeeDeleteList,
        employee: action?.payload,
      };

    case DELETE_EMPLOYEE_SUCCEEDED:
      const deleteEmployee = state.employeeList?.filter(
        (item) => item?.id !== action?.payload
      );

      return {
        ...state,
        employeeList: deleteEmployee,
        totalElements: state?.totalElements - 1,
        employee: [],
      };
    default:
      return { ...state };
  }
};

export default EmployeeReducer;
