import {
  GET_FAMILY_SUCCEEDED,
  SAVE_FAMILY_SUCCEEDED,
  DELETE_FAMILY_SUCCEEDED,
  UPDATE_FAMILY_SUCCEEDED,
  GET_FAMILY_BY_ID_SUCCEEDED,
} from "../actions/FamilyActions";

const initialState = {
  familyList: [],
  familyById: [],
};

const FamilyReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_FAMILY_SUCCEEDED:
      return {
        ...state,
        familyList: action.payload,
      };
    case SAVE_FAMILY_SUCCEEDED:
      var newFamilyList = [...action.payload, ...state.familyList];

      return {
        ...state,
        familyList: newFamilyList,
      };

    case DELETE_FAMILY_SUCCEEDED:
      var newFamilyList = state.familyList.filter(
        (item) => item?.id !== action?.payload?.id
      );

      return {
        ...state,
        familyList: newFamilyList,
      };

    case UPDATE_FAMILY_SUCCEEDED:
      var newFamilyList = state.familyList.map((item) =>
        action?.payload?.id === item?.id ? action.payload : item
      );

      return {
        ...state,
        familyList: newFamilyList,
      };
    case GET_FAMILY_BY_ID_SUCCEEDED:
      return {
        ...state,
        familyById: action.payload,
      };
    default:
      return { ...state };
  }
};

export default FamilyReducer;
