import { GET_ALL_LEADER_SUCCEEDED } from "../actions/LeaderActions";

const initialState = {
  leaderList: [],
};

const LeaderReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_LEADER_SUCCEEDED:
      return {
        ...state,
        leaderList: action.payload,
      };

    default:
      return { ...state };
  }
};

export default LeaderReducer;
