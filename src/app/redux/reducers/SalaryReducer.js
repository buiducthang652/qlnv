import {
  GET_BY_CURRENT_LEADER_SALARY_SUCCEEDED,
  GET_BY_EMPLOYEE_ID_SUCCEEDED,
  SAVE_SALARY_SUCCEEDED,
  DELETE_SALARY_SUCCEEDED,
  UPDATE_SALARY_SUCCEEDED,
} from "../actions/SalaryAction";

const initialState = {
  salaryList: [],
};

const SalaryReducer = function (state = initialState, action) {
  switch (action.type) {
    case GET_BY_CURRENT_LEADER_SALARY_SUCCEEDED: {
      return {
        ...state,
        salaryList: action.payload,
      };
    }

    case GET_BY_EMPLOYEE_ID_SUCCEEDED: {
      return {
        ...state,
        salaryList: action.payload,
      };
    }

    case SAVE_SALARY_SUCCEEDED: {
      return {
        ...state,
        salaryList: [...action.payload, ...state.salaryList],
      };
    }

    case DELETE_SALARY_SUCCEEDED: {
      const newSalary = state.salaryList.filter(
        (item) => item.id !== action.payload
      );

      return {
        ...state,
        salaryList: newSalary,
      };
    }

    case UPDATE_SALARY_SUCCEEDED: {
      if (
        action.payload?.salaryIncreaseStatus === 5 ||
        action.payload?.salaryIncreaseStatus === 3 ||
        action.payload?.salaryIncreaseStatus === 4
      ) {
        const newSalary = state.salaryList.filter(
          (item) => item.id !== action.payload?.id
        );

        return {
          ...state,
          salaryList: newSalary,
        };
      } else {
        const newSalary = state.salaryList.map((item) =>
          item.id === action.payload?.id ? action.payload : item
        );

        return {
          ...state,
          salaryList: newSalary,
        };
      }
    }

    default: {
      return state;
    }
  }
};

export default SalaryReducer;
