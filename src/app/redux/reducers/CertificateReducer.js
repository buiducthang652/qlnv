import {
  GET_CERTIFICATE_SUCCEEDED,
  SAVE_CERTIFICATE_SUCCEEDED,
  DELETE_CERTIFICATE_SUCCEEDED,
  UPDATE_CERTIFICATE_SUCCEEDED,
  GET_CERTIFICATE_BY_ID_SUCCEEDED,
} from "../actions/CertificateActions";

const initialState = {
  certificateList: [],
  certificateById: [],
};

const CertificateReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CERTIFICATE_SUCCEEDED:
      return {
        ...state,
        certificateList: action.payload,
      };

    case SAVE_CERTIFICATE_SUCCEEDED:
      var newCertificateList = [...action.payload, ...state.certificateList];

      return {
        ...state,
        certificateList: newCertificateList,
      };

    case DELETE_CERTIFICATE_SUCCEEDED:
      var newCertificateListDelete = state.certificateList.filter(
        (item) => action?.payload?.id !== item?.id
      );

      return {
        ...state,
        certificateList: newCertificateListDelete,
      };

    case UPDATE_CERTIFICATE_SUCCEEDED:
      var newCertificateListUpdate = state.certificateList.map((item) =>
        action?.payload?.id === item?.id ? action.payload : item
      );

      return {
        ...state,
        certificateList: newCertificateListUpdate,
      };

    case GET_CERTIFICATE_BY_ID_SUCCEEDED:
      return {
        ...state,
        certificateById: action.payload,
      };
    default:
      return { ...state };
  }
};

export default CertificateReducer;
