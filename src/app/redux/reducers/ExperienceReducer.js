import {
  SAVE_EXPERIENCE_SUCCEEDED,
  GET_EXPERIENCE_SUCCEEDED,
  DELETE_EXPERIENCE_SUCCEEDED,
  UPDATE_EXPERIENCE_SUCCEEDED,
} from "../actions/ExperienceActions";

const initialState = {
  experienceList: [],
};

const ExperienceReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_EXPERIENCE_SUCCEEDED:
      return {
        ...state,
        experienceList: action?.payload,
      };
    case SAVE_EXPERIENCE_SUCCEEDED:
      var newExperienceList = action.payload
        ? [...action.payload, ...state.experienceList]
        : state.experienceList;

      return {
        ...state,
        experienceList: newExperienceList,
      };

    case DELETE_EXPERIENCE_SUCCEEDED:
      var newExperienceList = state?.experienceList.filter(
        (item) => item?.id !== action?.payload
      );

      return {
        ...state,
        experienceList: newExperienceList,
      };

    case UPDATE_EXPERIENCE_SUCCEEDED:
      var newExperienceList = state?.experienceList.map((item) =>
        action?.payload?.id === item?.id ? action?.payload : item
      );

      return {
        ...state,
        experienceList: newExperienceList,
      };

    default:
      return { ...state };
  }
};

export default ExperienceReducer;
