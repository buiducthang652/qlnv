import {
  GET_BY_CURRENT_LEADER_PROPOSAL_SUCCEEDED,
  GET_BY_EMPLOYEE_ID_PROPOSAL_SUCCEEDED,
  SAVE_PROPOSAL_SUCCEEDED,
  DELETE_PROPOSAL_SUCCEEDED,
  UPDATE_PROPOSAL_SUCCEEDED,
} from "../actions/ProposalAction";

const initialState = {
  proposalList: [],
};

const ProposalReducer = function (state = initialState, action) {
  switch (action.type) {
    case GET_BY_CURRENT_LEADER_PROPOSAL_SUCCEEDED: {
      return {
        ...state,
        proposalList: action.payload,
      };
    }

    case GET_BY_EMPLOYEE_ID_PROPOSAL_SUCCEEDED: {
      return {
        ...state,
        proposalList: action.payload,
      };
    }

    case SAVE_PROPOSAL_SUCCEEDED: {
      return {
        ...state,
        proposalList: [...action.payload, ...state.proposalList],
      };
    }

    case DELETE_PROPOSAL_SUCCEEDED: {
      const newProposal = state.proposalList.filter(
        (item) => item.id !== action.payload
      );

      return {
        ...state,
        proposalList: newProposal,
      };
    }

    case UPDATE_PROPOSAL_SUCCEEDED: {
      if (
        action.payload?.proposalStatus === 5 ||
        action.payload?.proposalStatus === 3 ||
        action.payload?.proposalStatus === 4
      ) {
        const newProposal = state.proposalList.filter(
          (item) => item.id !== action.payload?.id
        );

        return {
          ...state,
          proposalList: newProposal,
        };
      } else {
        const newProposal = state.proposalList.map((item) =>
          item.id === action.payload?.id ? action.payload : item
        );

        return {
          ...state,
          proposalList: newProposal,
        };
      }
    }

    default: {
      return state;
    }
  }
};

export default ProposalReducer;
