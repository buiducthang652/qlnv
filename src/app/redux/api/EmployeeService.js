import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/employee/";

export const searchByPage = (searchObject) => {
  var url = API_PATH + "search";
  return axios.get(url, {
    params: searchObject,
  });
};

export const getEmployeeById = (id) => {
  var url = API_PATH + id;
  return axios.get(url);
};

export const saveEmployee = (employee) => {
  return axios.post(API_PATH, employee);
};

export const updateEmployee = (employee) => {
  var url = API_PATH + employee.id;
  return axios.put(url, employee);
};

export const deleteEmployee = (id) => {
  var url = API_PATH + id;
  return axios.delete(url);
};

export const uploadImageEmployee = (file) => {
  const url = API_PATH + "upload-image";
  const formData = new FormData();
  formData.append("file", file);
  return axios.post(url, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
};
