import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/salary-increase";

export const getByCurrentLeader = () => {
  var url = API_PATH + "/current-leader";
  return axios.get(url);
};

export const saveSalary = (id, salary) => {
  var url = API_PATH + "?employeeId=" + id;
  return axios.post(url, [salary]);
};

export const getByEmployeeId = (id) => {
  var url = API_PATH + "?employeeId=" + id;
  return axios.get(url);
};

export const deleteSalary = (id) => {
  var url = API_PATH + "/" + id;
  return axios.delete(url);
};

export const updateSalary = (salary) => {
  var url = API_PATH + "/" + salary?.id;
  return axios.put(url, salary);
};
