import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/employee-family";

export const getFamily = (id) => {
  var url = API_PATH + "?employeeId=" + id;
  return axios.get(url);
};

export const getFamilyById = (id) => {
  const url = API_PATH + "?employeeId=" + id;
  return axios.get(url);
};

export const saveFamily = (id, fam) => {
  var url = API_PATH + "?employeeId=" + id;

  return axios.post(url, fam);
};

export const deleteFamily = (id) => {
  var url = API_PATH + "/" + id;

  return axios.delete(url);
};

export const updateFamily = (fam) => {
  var url = API_PATH + "/" + fam?.id;

  return axios.put(url, fam);
};
