import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/experience";

export const getExperienceByEmployeeId = (id) => {
  var url = API_PATH + "?employeeId=" + id;
  return axios.get(url);
};

export const saveExperience = (id, employee) => {
  var url = API_PATH + "?employeeId=" + id;
  return axios.post(url, [employee]);
};

export const deleteExperience = (id) => {
  var url = API_PATH + "/" + id;
  return axios.delete(url);
};

export const updateExperience = (employee) => {
  var url = API_PATH + "/" + employee.id;
  return axios.put(url, employee);
};
