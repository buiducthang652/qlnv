import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/certificate";

export const getCertificate = (id) => {
  var url = API_PATH + "?employeeId=" + id;
  return axios.get(url);
};

export const getCertificateById = (id) => {
  var url = API_PATH + "?employeeId=" + id;
  return axios.get(url);
};

export const saveCertificate = (id, cer) => {
  var url = API_PATH + "?employeeId=" + id;
  return axios.post(url, cer);
};

export const deleteCertificate = (id) => {
  var url = API_PATH + "/" + id;
  return axios.delete(url);
};

export const updateCertificate = (cer) => {
  var url = API_PATH + "/" + cer?.id;
  return axios.put(url, cer);
};
