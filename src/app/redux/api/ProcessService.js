import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/process";

export const getByCurrentLeader = () => {
  var url = API_PATH + "/current-leader";
  return axios.get(url);
};

export const saveProcess = (id, Process) => {
  var url = API_PATH + "?employeeId=" + id;
  return axios.post(url, [Process]);
};

export const getByEmployeeId = (id) => {
  var url = API_PATH + "?employeeId=" + id;
  return axios.get(url);
};

export const deleteProcess = (id) => {
  var url = API_PATH + "/" + id;
  return axios.delete(url);
};

export const updateProcess = (Process) => {
  var url = API_PATH + "/" + Process?.id;
  return axios.put(url, Process);
};
