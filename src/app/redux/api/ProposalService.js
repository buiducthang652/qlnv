import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/proposal";

export const getByCurrentLeader = () => {
  var url = API_PATH + "/current-leader";
  return axios.get(url);
};

export const saveProposal = (id, Proposal) => {
  var url = API_PATH + "?employeeId=" + id;
  return axios.post(url, [Proposal]);
};

export const getByEmployeeId = (id) => {
  var url = API_PATH + "?employeeId=" + id;
  return axios.get(url);
};

export const deleteProposal = (id) => {
  var url = API_PATH + "/" + id;
  return axios.delete(url);
};

export const updateProposal = (Proposal) => {
  var url = API_PATH + "/" + Proposal?.id;
  console.log(Proposal);
  return axios.put(url, Proposal);
};
