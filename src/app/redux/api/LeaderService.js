import axios from "axios";
import ConstantList from "../../appConfig";
const API_PATH = ConstantList.API_ENPOINT + "/leader";

export const getAllLeader = () => {
  return axios.get(API_PATH);
};
