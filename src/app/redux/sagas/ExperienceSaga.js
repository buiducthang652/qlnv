import { takeEvery, put, call } from "redux-saga/effects";

import {
  SAVE_EXPERIENCE,
  GET_EXPERIENCE,
  DELETE_EXPERIENCE,
  UPDATE_EXPERIENCE,
  saveExperienceSucceeded,
  getExperienceSucceeded,
  deleteExperienceSucceeded,
  updateExperienceSucceeded,
} from "../actions/ExperienceActions";

import {
  saveExperience,
  getExperienceByEmployeeId,
  deleteExperience,
  updateExperience,
} from "../api/ExperienceService";

import { toast } from "react-toastify";

function* getAllExperience(action) {
  try {
    const res = yield call(getExperienceByEmployeeId, action?.payload);
    yield put(getExperienceSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* saveExperienceSaga(action) {
  try {
    const res = yield call(saveExperience, action?.id, action?.payload);
    toast.success("Thêm mới kinh nghiệm làm việc thành công.");
    yield put(saveExperienceSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* deleteExperienceSaga(action) {
  try {
    yield call(deleteExperience, action?.payload);
    toast.success("Xóa kinh nghiệm làm việc thành công.");
    yield put(deleteExperienceSucceeded(action?.payload));
  } catch (e) {
    toast.error(e.message);
  }
}

function* updateExperienceSaga(action) {
  try {
    const res = yield call(updateExperience, action?.payload);
    toast.success("Cập nhật thành công.");
    yield put(updateExperienceSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

export default function* ExperienceSaga() {
  yield takeEvery(GET_EXPERIENCE, getAllExperience);
  yield takeEvery(SAVE_EXPERIENCE, saveExperienceSaga);
  yield takeEvery(DELETE_EXPERIENCE, deleteExperienceSaga);
  yield takeEvery(UPDATE_EXPERIENCE, updateExperienceSaga);
}
