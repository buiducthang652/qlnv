import { takeEvery, put, call } from "redux-saga/effects";

import {
  GET_BY_CURRENT_LEADER_PROCESS,
  GET_BY_EMPLOYEE_ID_PROCESS,
  SAVE_PROCESS,
  UPDATE_PROCESS,
  DELETE_PROCESS,
  getByCurrentLeaderProcessSucceeded,
  getByEmployeeIdProcessSucceeded,
  saveProcessSucceeded,
  updateProcessSucceeded,
  deleteProcessSucceeded,
} from "../actions/ProcessAction";

import {
  getByCurrentLeader,
  getByEmployeeId,
  saveProcess,
  updateProcess,
  deleteProcess,
} from "../api/ProcessService";

import { toast } from "react-toastify";

function* getByCurrentLeaderSaga() {
  try {
    const res = yield call(getByCurrentLeader);
    yield put(getByCurrentLeaderProcessSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* saveProcessSaga(action) {
  try {
    const res = yield call(saveProcess, action.id, action.payload);
    toast.success("Lưu thành công!!!");
    yield put(saveProcessSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* getByEmployeeIdSaga(action) {
  try {
    const res = yield call(getByEmployeeId, action.payload);
    yield put(getByEmployeeIdProcessSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* deleteProcessSaga(action) {
  try {
    yield call(deleteProcess, action.payload);
    toast.success("Xóa thành công!!!");
    yield put(deleteProcessSucceeded(action.payload));
  } catch (e) {
    toast.error(e.message);
  }
}

function* updateProcessSaga(action) {
  try {
    yield call(updateProcess, action.payload);
    toast.success("Sửa thành công!!!");
    yield put(updateProcessSucceeded(action.payload));
  } catch (e) {
    toast.error(e.message);
  }
}

export default function* ProcessSaga() {
  yield takeEvery(GET_BY_CURRENT_LEADER_PROCESS, getByCurrentLeaderSaga);
  yield takeEvery(GET_BY_EMPLOYEE_ID_PROCESS, getByEmployeeIdSaga);
  yield takeEvery(SAVE_PROCESS, saveProcessSaga);
  yield takeEvery(UPDATE_PROCESS, updateProcessSaga);
  yield takeEvery(DELETE_PROCESS, deleteProcessSaga);
}
