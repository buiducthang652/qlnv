import { takeEvery, put, call } from "redux-saga/effects";

import {
  GET_BY_CURRENT_LEADER_PROPOSAL,
  SAVE_PROPOSAL,
  GET_BY_EMPLOYEE_ID_PROPOSAL,
  DELETE_PROPOSAL,
  UPDATE_PROPOSAL,
  getByCurrentLeaderProposalSucceeded,
  saveProposalSucceeded,
  getByEmployeeIdProposalSucceeded,
  deleteProposalSucceeded,
  updateProposalSucceeded,
} from "../actions/ProposalAction";

import {
  getByCurrentLeader,
  getByEmployeeId,
  saveProposal,
  deleteProposal,
  updateProposal,
} from "../api/ProposalService";

import { toast } from "react-toastify";

function* getByCurrentLeaderSaga() {
  try {
    const res = yield call(getByCurrentLeader);
    yield put(getByCurrentLeaderProposalSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* saveProposalSaga(action) {
  try {
    const res = yield call(saveProposal, action.id, action.payload);
    toast.success("Lưu thành công!!!");
    yield put(saveProposalSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* getByEmployeeIdSaga(action) {
  try {
    const res = yield call(getByEmployeeId, action.payload);
    yield put(getByEmployeeIdProposalSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* deleteProposalSaga(action) {
  try {
    yield call(deleteProposal, action.payload);
    toast.success("Xóa thành công!!!");
    yield put(deleteProposalSucceeded(action.payload));
  } catch (e) {
    toast.error(e.message);
  }
}

function* updateProposalSaga(action) {
  try {
    yield call(updateProposal, action.payload);
    toast.success("Sửa thành công!!!");
    yield put(updateProposalSucceeded(action.payload));
  } catch (e) {
    toast.error(e.message);
  }
}

export default function* ProposalSaga() {
  yield takeEvery(GET_BY_CURRENT_LEADER_PROPOSAL, getByCurrentLeaderSaga);
  yield takeEvery(SAVE_PROPOSAL, saveProposalSaga);
  yield takeEvery(GET_BY_EMPLOYEE_ID_PROPOSAL, getByEmployeeIdSaga);
  yield takeEvery(UPDATE_PROPOSAL, updateProposalSaga);
  yield takeEvery(DELETE_PROPOSAL, deleteProposalSaga);
}
