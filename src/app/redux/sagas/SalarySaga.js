import { takeEvery, put, call } from "redux-saga/effects";

import {
  GET_BY_CURRENT_LEADER_SALARY,
  SAVE_SALARY,
  GET_BY_EMPLOYEE_ID,
  DELETE_SALARY,
  UPDATE_SALARY,
  getByCurrentLeaderSalarySucceeded,
  saveSalarySucceeded,
  getByEmployeeIdSucceeded,
  deleteSalarySucceeded,
  updateSalarySucceeded,
} from "../actions/SalaryAction";

import {
  getByCurrentLeader,
  saveSalary,
  getByEmployeeId,
  deleteSalary,
  updateSalary,
} from "../api/SalaryService";

import { toast } from "react-toastify";

function* getByCurrentLeaderSaga() {
  try {
    const res = yield call(getByCurrentLeader);
    yield put(getByCurrentLeaderSalarySucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* saveSalarySaga(action) {
  try {
    const res = yield call(saveSalary, action.id, action.payload);
    toast.success("Lưu thành công!!!");
    yield put(saveSalarySucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* getByEmployeeIdSaga(action) {
  try {
    const res = yield call(getByEmployeeId, action.payload);
    yield put(getByEmployeeIdSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* deleteSalarySaga(action) {
  try {
    yield call(deleteSalary, action.payload);
    toast.success("Xóa thành công!!!");
    yield put(deleteSalarySucceeded(action.payload));
  } catch (e) {
    toast.error(e.message);
  }
}

function* updateSalarySaga(action) {
  try {
    yield call(updateSalary, action.payload);
    toast.success("Sửa thành công!!!");
    yield put(updateSalarySucceeded(action.payload));
  } catch (e) {
    toast.error(e.message);
  }
}

export default function* SalarySaga() {
  yield takeEvery(GET_BY_CURRENT_LEADER_SALARY, getByCurrentLeaderSaga);
  yield takeEvery(SAVE_SALARY, saveSalarySaga);
  yield takeEvery(GET_BY_EMPLOYEE_ID, getByEmployeeIdSaga);
  yield takeEvery(DELETE_SALARY, deleteSalarySaga);
  yield takeEvery(UPDATE_SALARY, updateSalarySaga);
}
