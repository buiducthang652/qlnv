import { takeEvery, put, call } from "redux-saga/effects";

import {
  GET_CERTIFICATE,
  SAVE_CERTIFICATE,
  DELETE_CERTIFICATE,
  UPDATE_CERTIFICATE,
  GET_CERTIFICATE_BY_ID,
  getCertificateSucceeded,
  saveCertificateSucceeded,
  deleteCertificateSucceeded,
  updateCertificateSucceeded,
  getCertificateByIdSucceeded,
} from "../actions/CertificateActions";

import {
  getCertificate,
  saveCertificate,
  deleteCertificate,
  updateCertificate,
  getCertificateById,
} from "../api/CertificateService";

import { toast } from "react-toastify";

function* getCertificateSaga(action) {
  try {
    const res = yield call(getCertificate, action.payload);
    yield put(getCertificateSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* saveCertificateSaga(action) {
  try {
    const res = yield call(saveCertificate, action?.id, action?.payload);
    toast.success("Thêm văn bằng thành công!");
    yield put(saveCertificateSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* deleteCertificateSaga(action) {
  try {
    yield call(deleteCertificate, action?.payload?.id);
    toast.success("Xóa văn bằng thành công");
    yield put(deleteCertificateSucceeded(action.payload));
  } catch (e) {
    toast.error(e.message);
  }
}

function* updateCertificateSaga(action) {
  try {
    yield call(updateCertificate, action.payload);
    toast.success("Cập nhật văn bằng thành công");
    yield put(updateCertificateSucceeded(action.payload));
  } catch (e) {
    toast.error(e.message);
  }
}

function* getCertificateByIdSaga(action) {
  try {
    const res = yield call(getCertificateById, action.payload);
    yield put(getCertificateByIdSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

export default function* CertificateSaga() {
  yield takeEvery(GET_CERTIFICATE, getCertificateSaga);
  yield takeEvery(SAVE_CERTIFICATE, saveCertificateSaga);
  yield takeEvery(DELETE_CERTIFICATE, deleteCertificateSaga);
  yield takeEvery(UPDATE_CERTIFICATE, updateCertificateSaga);
  yield takeEvery(GET_CERTIFICATE_BY_ID, getCertificateByIdSaga);
}
