import { takeEvery, put, call } from "redux-saga/effects";

import {
  GET_FAMILY,
  SAVE_FAMILY,
  DELETE_FAMILY,
  UPDATE_FAMILY,
  GET_FAMILY_BY_ID,
  getFamilySucceeded,
  saveFamilySucceeded,
  deleteFamilySucceeded,
  updateFamilySucceeded,
  getFamilyByIdSucceeded,
} from "../actions/FamilyActions";

import {
  getFamily,
  saveFamily,
  deleteFamily,
  updateFamily,
  getFamilyById,
} from "../api/FamilyService";

import { toast } from "react-toastify";

function* getAllFamily(action) {
  try {
    const res = yield call(getFamily, action.payload);
    yield put(getFamilySucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* getAllFamilyById(action) {
  try {
    const res = yield call(getFamilyById, action.payload);
    yield put(getFamilyByIdSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* saveFamilySaga(action) {
  try {
    const res = yield call(saveFamily, action?.id, action?.payload);
    toast.success("Thêm quan hệ thành công!");
    yield put(saveFamilySucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* deleteFamilySaga(action) {
  try {
    yield call(deleteFamily, action?.payload?.id);
    toast.success("Xóa quan hệ thành công.");
    yield put(deleteFamilySucceeded(action.payload));
  } catch (e) {
    toast.error(e.message);
  }
}

function* updateFamilySaga(action) {
  try {
    yield call(updateFamily, action.payload);
    toast.success("Cập nhật nhân viên thành công");
    yield put(updateFamilySucceeded(action.payload));
  } catch (e) {
    toast.error(e.message);
  }
}

export default function* FamilySaga() {
  yield takeEvery(GET_FAMILY, getAllFamily);
  yield takeEvery(SAVE_FAMILY, saveFamilySaga);
  yield takeEvery(DELETE_FAMILY, deleteFamilySaga);
  yield takeEvery(UPDATE_FAMILY, updateFamilySaga);
  yield takeEvery(GET_FAMILY_BY_ID, getAllFamilyById);
}
