import { all } from "redux-saga/effects";

import EmployeeSaga from "./EmployeeSaga.js";
import CertificateSaga from "./CertificateSaga.js";
import FamilySaga from "./FamilySaga.js";
import ExperienceSaga from "./ExperienceSaga.js";
import LeaderSaga from "./LeaderSaga.js";
import SalarySaga from "./SalarySaga.js";
import ProcessSaga from "./ProcessSaga.js";
import ProposalSaga from "./ProposalSaga.js";

export default function* RootSaga() {
  yield all([
    EmployeeSaga(),
    CertificateSaga(),
    FamilySaga(),
    ExperienceSaga(),
    LeaderSaga(),
    SalarySaga(),
    ProcessSaga(),
    ProposalSaga(),
  ]);
}
