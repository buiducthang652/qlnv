import { takeEvery, put, call } from "redux-saga/effects";

import { API_ENPOINT } from "app/appConfig";
import {
  SEARCH_EMPLOYEE,
  SAVE_EMPLOYEE,
  DELETE_EMPLOYEE,
  UPDATE_EMPLOYEE,
  UPDATE_EMPLOYEE_DELETE,
  GET_EMPLOYEE_BY_ID,
  searchEmployeesSucceeded,
  saveEmployeeSucceeded,
  deleteEmployeeSucceeded,
  updateEmployeeSucceeded,
  updateEmployeeDeleteSucceeded,
  getEmployeeByIdSucceeded,
} from "../actions/EmployeeActions";

import {
  searchByPage,
  saveEmployee,
  updateEmployee,
  deleteEmployee,
  uploadImageEmployee,
  getEmployeeById,
} from "../api/EmployeeService";

import { toast } from "react-toastify";

function* uploadAvatar(file) {
  try {
    const res = yield call(uploadImageEmployee, file);
    const imgUrl = yield API_ENPOINT + "/public/image/" + res?.data?.name;
    return imgUrl;
  } catch (e) {
    toast.error(e.message);
  }
}

function* searchEmployeeByPage(action) {
  try {
    const res = yield call(searchByPage, action.payload);
    yield put(searchEmployeesSucceeded(res?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* saveEmployeeByPage(action) {
  try {
    const urlImg = action.payload.file
      ? yield call(uploadAvatar, action.payload.file)
      : "";
    const newEmployee = {
      ...action.payload,
      image: urlImg,
    };

    const res = yield call(saveEmployee, newEmployee);
    toast.success("Thêm nhân viên thành công");
    yield put(saveEmployeeSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

function* deleteEmployeeByPage(action) {
  try {
    yield call(deleteEmployee, action?.payload);
    toast.success("Xóa nhân viên thành công");
    yield put(deleteEmployeeSucceeded(action?.payload));
  } catch (e) {
    toast.error(e.message);
  }
}

function* updateEmployeeByPage(action) {
  try {
    const urlImg = action?.payload?.file
      ? yield call(uploadAvatar, action?.payload?.file)
      : action?.payload?.image;
    const newEmployee = {
      ...action.payload,
      image: urlImg,
    };

    yield call(updateEmployee, newEmployee);
    toast.success("Cập nhật nhân viên thành công");
    yield put(updateEmployeeSucceeded(newEmployee));
  } catch (e) {
    toast.error(e.message);
  }
}

function* updateEmployeeDeleteByPage(action) {
  try {
    const urlImg = action?.payload?.file
      ? yield call(uploadAvatar, action?.payload?.file)
      : action?.payload?.image;
    const newEmployee = {
      ...action.payload,
      image: urlImg,
    };

    yield call(updateEmployee, newEmployee);
    toast.success("Cập nhật nhân viên thành công");
    yield put(updateEmployeeDeleteSucceeded(newEmployee));
  } catch (e) {
    toast.error(e.message);
  }
}

function* getEmployeeByIdPage(action) {
  try {
    const res = yield call(getEmployeeById, action.payload);
    yield put(getEmployeeByIdSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

export default function* EmployeeSaga() {
  yield takeEvery(SEARCH_EMPLOYEE, searchEmployeeByPage);
  yield takeEvery(SAVE_EMPLOYEE, saveEmployeeByPage);
  yield takeEvery(DELETE_EMPLOYEE, deleteEmployeeByPage);
  yield takeEvery(UPDATE_EMPLOYEE, updateEmployeeByPage);
  yield takeEvery(UPDATE_EMPLOYEE_DELETE, updateEmployeeDeleteByPage);
  yield takeEvery(GET_EMPLOYEE_BY_ID, getEmployeeByIdPage);
}
