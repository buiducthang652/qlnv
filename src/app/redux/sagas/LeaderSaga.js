import { takeEvery, put, call } from "redux-saga/effects";

import {
  GET_ALL_LEADER,
  getAllLeaderSucceeded,
} from "../actions/LeaderActions";

import { getAllLeader } from "../api/LeaderService";

import { toast } from "react-toastify";

function* getAllLeaderSaga() {
  try {
    const res = yield call(getAllLeader);
    yield put(getAllLeaderSucceeded(res?.data?.data));
  } catch (e) {
    toast.error(e.message);
  }
}

export default function* LeaderSaga() {
  yield takeEvery(GET_ALL_LEADER, getAllLeaderSaga);
}
