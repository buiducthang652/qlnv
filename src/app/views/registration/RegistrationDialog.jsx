import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
  Dialog,
  Button,
  DialogActions,
  DialogTitle,
  DialogContent,
  Tabs,
  Tab,
  Box,
  Typography,
  Grid,
} from "@material-ui/core";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { toast } from "react-toastify";
import "../../../styles/views/_loadding.scss";
import "react-toastify/dist/ReactToastify.css";
import "../../../styles/views/_style.scss";
import "../../../styles/views/_cv.scss";
import PropTypes from "prop-types";
import CVDialog from "./Tab/CVDialog";
import CurriculumVitae from "./Tab/CurriculumVitae";
import CertificateDialog from "./Tab/CertificateDialog";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import useTheme from "@material-ui/core/styles/useTheme";
import LeaderShipDialog from "./LeadershipDialog";
import ApproveDialog from "../component/StatusDialog/ApproveDialog";
import AdditionalRequestDialog from "../component/StatusDialog/AdditionalRequestDialog";
import RefuseDialog from "../component/StatusDialog/RefuseDialog";

toast.configure({
  autoClose: 1000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-reg-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      className="w-100"
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const RegistrationDialog = ({
  open,
  handleShowRegistration,
  handleShowEditorDialog,
  employeeInfo,
  setEmployeeInfo,
  pending,
  isView,
}) => {
  const [value, setValue] = useState(0);
  const lg = useMediaQuery(useTheme().breakpoints.up("lg"));
  const [showLeaderShipDialog, setShowLeaderShipDialog] = useState(false);
  const [showApproveDialog, setShowApproveDialog] = useState(false);
  const [showRefuseDialog, setShowRefuseDialog] = useState(false);
  const [showAdditionalRequestDialog, setShowAdditionalRequestDialog] =
    useState(false);
  // const [isPending, setIsPending] = useState(false);

  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth={"lg"}
      fullWidth={true}
    >
      <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
        Hồ sơ nhân viên
      </DialogTitle>

      <DialogContent>
        {/* Trình lãnh đạo */}
        {showLeaderShipDialog && (
          <LeaderShipDialog
            open={showLeaderShipDialog}
            setShowLeaderShipDialog={setShowLeaderShipDialog}
            handleShowRegistration={handleShowRegistration}
            handleShowEditorDialog={handleShowEditorDialog}
            employeeInfo={employeeInfo}
            setEmployeeInfo={setEmployeeInfo}
          />
        )}

        {/* Phê duyệt */}
        {showApproveDialog && (
          <ApproveDialog
            open={showApproveDialog}
            setShowApproveDialog={setShowApproveDialog}
            handleShowRegistration={handleShowRegistration}
            item={employeeInfo}
            setItem={setEmployeeInfo}
            isPending={false}
          />
        )}

        {/* Bổ sung */}
        {showAdditionalRequestDialog && (
          <AdditionalRequestDialog
            open={showAdditionalRequestDialog}
            setShowAdditionalRequestDialog={setShowAdditionalRequestDialog}
            handleShowRegistration={handleShowRegistration}
            item={employeeInfo}
            setItem={setEmployeeInfo}
            isPending={false}
          />
        )}

        {/* Kết thúc */}
        {showRefuseDialog && (
          <RefuseDialog
            open={showRefuseDialog}
            setShowRefuseDialog={setShowRefuseDialog}
            handleShowRegistration={handleShowRegistration}
            item={employeeInfo}
            setItem={setEmployeeInfo}
            isPending={false}
          />
        )}

        <Grid container>
          <Grid
            item
            lg={2}
            md={12}
            sm={12}
            xs={12}
            className={lg ? "posi" : ""}
          >
            <Tabs
              value={value}
              onChange={(event, newValue) => {
                setValue(newValue);
              }}
              orientation={lg ? "vertical" : "horizontal"}
              indicatorColor="primary"
              textColor="primary"
              variant="scrollable"
              aria-label="Vertical tabs example"
              className={lg ? "vertical_left" : ""}
            >
              <Tab label="CV" {...a11yProps(0)} />
              <Tab label="Sơ yếu lý lịch" {...a11yProps(1)} />
              <Tab label="Văn bằng" {...a11yProps(2)} />
            </Tabs>
          </Grid>

          <Grid item lg={10} md={12} sm={12} xs={12}>
            <TabPanel value={value} index={0}>
              <CVDialog
                employeeInfo={employeeInfo}
                setEmployeeInfo={setEmployeeInfo}
                isView={!isView}
              />
            </TabPanel>
            <TabPanel value={value} index={1}>
              <CurriculumVitae employeeInfo={employeeInfo} />
            </TabPanel>
            <TabPanel value={value} index={2}>
              <CertificateDialog employeeInfo={employeeInfo} />
            </TabPanel>
          </Grid>
        </Grid>
      </DialogContent>

      <DialogActions className="justifyCenter">
        {pending && (
          <>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                setShowApproveDialog(true);
              }}
            >
              Phê duyệt
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                setShowAdditionalRequestDialog(true);
                // setIsPending(true);
              }}
            >
              Yêu cầu bổ sung
            </Button>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => {
                setShowRefuseDialog(true);
              }}
            >
              Từ chối
            </Button>
          </>
        )}

        {isView && (
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              setShowLeaderShipDialog(true);
            }}
          >
            Trình lãnh đạo
          </Button>
        )}

        <Button
          variant="contained"
          color="secondary"
          onClick={() => {
            handleShowRegistration(false, employeeInfo, false);
          }}
        >
          Hủy
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default RegistrationDialog;
