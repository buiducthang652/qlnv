import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Grid, Avatar, Icon, Button } from "@material-ui/core";
import { TEAMS, GENDER } from "app/views/component/constants";
import { format, parse, isAfter } from "date-fns";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { updateEmployees } from "../../../redux/actions/EmployeeActions";
import { ConfirmationDialog } from "egret";
import {
  getExperience,
  saveExperience,
  deleteExperience,
  updateExperience,
} from "../../../redux/actions/ExperienceActions";
import "../../../../styles/views/_cv.scss";
import { renderIconButton } from "../../utilities/renderButton";
import { formatMY } from "app/views/utilities/formatDate";

const CVDialog = ({ employeeInfo, setEmployeeInfo, isView }) => {
  const { experienceList } = useSelector((state) => ({
    experienceList: state.experience.experienceList,
  }));

  const [isEdit1, setIsEdit1] = useState(false);
  const [isEdit2, setIsEdit2] = useState(false);
  const [isEdit3, setIsEdit3] = useState(false);
  const [isEdit4, setIsEdit4] = useState(false);
  const [experience, setExperience] = useState();
  const [experienceId, setExperienceId] = useState();
  const [shouldOpenConfirmationDialog, setShouldOpenConfirmationDialog] =
    useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    if (employeeInfo.id) {
      dispatch(getExperience(employeeInfo.id));
    }
  }, [employeeInfo]);

  useEffect(() => {
    ValidatorForm.addValidationRule("isStartDate", (value) => {
      if (!value) {
        return true;
      }
      if (experience?.endDate) {
        const currentDate = new Date(experience?.endDate).setHours(
          23,
          59,
          59,
          59
        );
        const selectedDate = parse(value, "yyyy-MM-dd", new Date());
        return isAfter(currentDate, selectedDate);
      }
      return true;
    });

    ValidatorForm.addValidationRule("isEndDate", (value) => {
      if (!value) {
        return true;
      }
      if (experience?.startDate) {
        const currentDate = new Date(experience?.startDate).setHours(
          0,
          0,
          0,
          0
        );
        const selectedDate = parse(value, "yyyy-MM-dd", new Date());
        return isAfter(selectedDate, currentDate);
      }
      return true;
    });
    return () => {
      ValidatorForm.removeValidationRule("isStartDate");
      ValidatorForm.removeValidationRule("isEndDate");
    };
  }, [experience]);

  const handleFormSubmit = (e) => {
    dispatch(updateEmployees(employeeInfo));
    setIsEdit1(false);
    setIsEdit2(false);
    setIsEdit3(false);
  };

  const handleFormSubmitExperience = (e) => {
    if (experienceId) {
      setExperienceId();
      dispatch(updateExperience(experience));
    } else {
      dispatch(saveExperience(employeeInfo.id, experience));
    }
    setIsEdit4(false);
  };

  const handleChangeValue = (e) => {
    setEmployeeInfo({
      ...employeeInfo,
      [e.target.name]: e.target.value,
    });
  };

  const handleChangeExperience = (e) => {
    setExperience({
      ...experience,
      [e.target.name]: e.target.value,
    });
  };

  const handleChangeDateExperience = (e) => {
    const timestamp = parse(e.target.value, "yyyy-MM-dd", new Date()).getTime();
    setExperience({
      ...experience,
      [e.target.name]: timestamp,
    });
  };

  return (
    <>
      {shouldOpenConfirmationDialog && (
        <ConfirmationDialog
          title={"Xác nhận"}
          open={shouldOpenConfirmationDialog}
          onConfirmDialogClose={() => {
            setExperience();
            setShouldOpenConfirmationDialog(false);
          }}
          onYesClick={() => {
            dispatch(deleteExperience(experienceId));
            setExperience();
            setShouldOpenConfirmationDialog(false);
          }}
          text={"Bạn có chắc chắn xóa!!!"}
          Yes={"Xóa"}
          No={"Hủy"}
        />
      )}

      <Grid className="bg" container spacing={1}>
        <Grid container>
          <Grid container className="cv_container_left">
            <Grid className="cv_avatar" item>
              <Avatar src={employeeInfo?.image || ""} />
            </Grid>
          </Grid>

          <Grid item className="cv_container_right pdt-49">
            <Grid className="cv_head" container lg={12} md={12} sm={12} xs={12}>
              <Grid className="cv_name" item lg={12} md={12} sm={12} xs={12}>
                {employeeInfo?.name || ""}
              </Grid>
              <Grid className="cv_posion" item lg={12} md={12} sm={12} xs={12}>
                {TEAMS.find((team) => team.CODE === employeeInfo?.team)?.NAME ||
                  ""}
              </Grid>
            </Grid>
            <Grid className="px-30" container lg={12} md={12} sm={12} xs={12}>
              <Grid className="cv_icon" item lg={5} md={5} sm={5} xs={5}>
                <Icon fontSize="small">wc</Icon>
                {GENDER.find((gender) => gender.CODE === employeeInfo?.gender)
                  ?.NAME || ""}
              </Grid>
              <Grid className="cv_icon ml-64" item lg={5} md={5} sm={5} xs={5}>
                <Icon fontSize="small">email</Icon>
                {employeeInfo?.email || ""}
              </Grid>
              <Grid className="cv_icon" item lg={5} md={5} sm={5} xs={5}>
                <Icon fontSize="small">event</Icon>
                {employeeInfo?.dateOfBirth
                  ? format(new Date(employeeInfo?.dateOfBirth), "dd/MM/yyyy")
                  : null}
              </Grid>
              <Grid className="cv_icon ml-64" item lg={5} md={5} sm={5} xs={5}>
                <Icon fontSize="small">phone</Icon>
                {employeeInfo?.phone || ""}
              </Grid>
              <Grid className="cv_icon" item lg={12} md={12} sm={12} xs={12}>
                <Icon fontSize="small">business</Icon>
                {employeeInfo?.address || ""}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid container>
          <Grid container className="cv_container_left">
            <Grid className="cv_left cv_skill" container>
              <ValidatorForm
                onSubmit={(e) => {
                  handleFormSubmit(e);
                }}
              >
                <h5>
                  KỸ NĂNG{" "}
                  {isEdit1 !== isView
                    ? ""
                    : renderIconButton("edit", "secondary", () => {
                        setIsEdit1(true);
                      })}
                </h5>
                {isEdit1 ? (
                  <>
                    <TextValidator
                      onChange={handleChangeValue}
                      fullWidth
                      multiline
                      className=""
                      type="text"
                      name="skill"
                      value={employeeInfo?.skill || ""}
                      size="small"
                      variant="outlined"
                      placeholder="Nhập kỹ năng của bạn"
                      validators={["maxStringLength:1000"]}
                      errorMessages={["Tối đa 1000 ký tự"]}
                    />
                    <Grid item className="btn-center">
                      <Button variant="contained" color="primary" type="submit">
                        Lưu
                      </Button>

                      <Button
                        variant="contained"
                        color="secondary"
                        className="ml-5"
                        onClick={() => {
                          setIsEdit1(false);
                        }}
                      >
                        Hủy
                      </Button>
                    </Grid>
                  </>
                ) : (
                  <ul className="work">
                    {employeeInfo?.skill
                      ? employeeInfo?.skill
                          .split("\n")
                          .filter((skill) => skill.trim() !== "")
                          .map((skill, index) => <li key={index}>{skill}</li>)
                      : "Không có"}
                  </ul>
                )}
              </ValidatorForm>
            </Grid>
          </Grid>
          <Grid container className="cv_container_right">
            <Grid
              className="cv_target cv_right"
              container
              lg={12}
              md={12}
              sm={12}
              xs={12}
            >
              <h5>MỤC TIÊU NGHỀ NGHIỆP</h5>
              <p>
                <Icon className="quote_top" fontSize="small">
                  format_quote
                </Icon>
                Áp dụng những kinh nghiệm về kỹ năng bán hàng và sự hiểu biết về
                thị trường để trở thành một nhân viên bán hàng chuyên nghiệp,
                mang đến nhiều giá trị cho khách hàng. Từ đó giúp Công ty tăng
                số lượng khách hàng và mở rộng tập khách hàng.
                <Icon className="quote_bot" fontSize="small">
                  format_quote
                </Icon>
              </p>
            </Grid>
          </Grid>
        </Grid>

        <Grid container>
          <Grid container className="cv_container_left">
            <Grid className="cv_left nnth" container>
              <h5>NGOẠI NGỮ</h5>
              <div className="start_container">
                <span>Tiếng anh</span>
                <p className="start_c">
                  <span className="start"></span>
                  <span className="start"></span>
                  <span className="start"></span>
                </p>
              </div>
            </Grid>
            <Grid className="cv_left nnth th" container>
              <h5 className="pt-12">TIN HỌC</h5>
              <div className="start_container">
                <span>Word</span>
                <p className="start_c">
                  <span className="start"></span>
                  <span className="start"></span>
                  <span className="start"></span>
                </p>
              </div>
              <div className="start_container">
                <span>Excel</span>
                <p className="start_c">
                  <span className="start"></span>
                  <span className="start"></span>
                  <span className="start none-start"></span>
                </p>
              </div>
            </Grid>
            <Grid className="cv_left cv_skill pt-18" container>
              <ValidatorForm
                onSubmit={(e) => {
                  handleFormSubmit(e);
                }}
              >
                <h5 className="pt-2">
                  HOẠT ĐỘNG{" "}
                  {isEdit2 !== isView
                    ? ""
                    : renderIconButton("edit", "secondary", () => {
                        setIsEdit2(true);
                      })}
                </h5>
                {isEdit2 ? (
                  <>
                    <TextValidator
                      onChange={handleChangeValue}
                      fullWidth
                      multiline
                      className=""
                      type="text"
                      name="activity"
                      value={employeeInfo?.activity || ""}
                      size="small"
                      variant="outlined"
                      placeholder="Nhập hoạt động của bạn"
                      validators={["maxStringLength:1000"]}
                      errorMessages={["Tối đa 1000 ký tự"]}
                    />
                    <Grid item className="btn-center">
                      <Button variant="contained" color="primary" type="submit">
                        Lưu
                      </Button>
                      <Button
                        variant="contained"
                        color="secondary"
                        className="ml-5"
                        onClick={() => {
                          setIsEdit2(false);
                        }}
                      >
                        Hủy
                      </Button>
                    </Grid>
                  </>
                ) : (
                  <ul className="work">
                    {employeeInfo?.activity
                      ? employeeInfo?.activity
                          .split("\n")
                          .filter((activity) => activity.trim() !== "")
                          .map((activity, index) => (
                            <li key={index}>{activity}</li>
                          ))
                      : "Không có"}
                  </ul>
                )}
              </ValidatorForm>
            </Grid>
            <Grid className="cv_left cv_skill" container>
              <h5>SỞ THÍCH</h5>
              <ul className="work">
                <li className="">Học ngoại ngữ mới</li>
                <li className="">
                  Chơi rubik, chơi game suy luận, chiến thuật
                </li>
              </ul>
            </Grid>
            <Grid className="cv_left cv_skill" container>
              <h5>NGƯỜI LIÊN HỆ</h5>
              <ul className="work">
                <li className="">Lê thị thu Hiền</li>
              </ul>
            </Grid>
          </Grid>
          <Grid item className="cv_container_right">
            <ValidatorForm
              onSubmit={(e) => {
                handleFormSubmitExperience(e);
              }}
            >
              <Grid
                className="cv_experience kn cv_right cv_left mt-14"
                container
                lg={12}
                md={12}
                sm={12}
                xs={12}
              >
                <h5>
                  KINH NGHIỆM LÀM VIỆC{" "}
                  {isEdit4 !== isView
                    ? ""
                    : renderIconButton("add_circle", "secondary", () => {
                        setExperience();
                        setIsEdit4(true);
                      })}
                </h5>
                {isEdit4 ? (
                  <>
                    <Grid container spacing={3}>
                      <Grid item xs={12}>
                        <TextValidator
                          className="w-100 mb-6"
                          label={
                            <span className="font exper">
                              <span style={{ color: "red" }}> * </span>
                              Tên công ty
                            </span>
                          }
                          onChange={handleChangeExperience}
                          type="text"
                          name="companyName"
                          value={experience?.companyName || ""}
                          validators={["required", "maxStringLength:200"]}
                          errorMessages={[
                            "Không được bỏ trống!",
                            "Tối đa 200 ký tự",
                          ]}
                          variant="outlined"
                          size="small"
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <TextValidator
                          className="w-100 mb-6"
                          label={
                            <span className="font exper">
                              <span style={{ color: "red" }}> * </span>
                              Ngày bất đầu
                            </span>
                          }
                          onChange={handleChangeDateExperience}
                          type="date"
                          name="startDate"
                          value={
                            experience?.startDate
                              ? format(
                                  new Date(experience?.startDate),
                                  "yyyy-MM-dd"
                                )
                              : null
                          }
                          InputLabelProps={{ shrink: true }}
                          validators={["required", "isStartDate"]}
                          errorMessages={[
                            "Không được bỏ trống!",
                            "Không vượt quá ngày kết thúc.",
                          ]}
                          variant="outlined"
                          size="small"
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <TextValidator
                          className="w-100 mb-6"
                          label={
                            <span className="font exper">
                              <span style={{ color: "red" }}> * </span>
                              Ngày kết thúc
                            </span>
                          }
                          onChange={handleChangeDateExperience}
                          type="date"
                          name="endDate"
                          value={
                            experience?.endDate
                              ? format(
                                  new Date(experience?.endDate),
                                  "yyyy-MM-dd"
                                )
                              : null
                          }
                          InputLabelProps={{ shrink: true }}
                          validators={["required", "isEndDate"]}
                          errorMessages={[
                            "Không được bỏ trống!",
                            "Không trước ngày bắt đầu.",
                          ]}
                          variant="outlined"
                          size="small"
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <TextValidator
                          className="w-100"
                          label={
                            <span className="font exper">
                              <span style={{ color: "red" }}> * </span>
                              Địa chỉ công ty
                            </span>
                          }
                          onChange={handleChangeExperience}
                          type="text"
                          name="companyAddress"
                          value={experience?.companyAddress || ""}
                          validators={["required", "maxStringLength:150"]}
                          errorMessages={[
                            "Không được bỏ trống!",
                            "Tối đa 150 ký tự",
                          ]}
                          variant="outlined"
                          size="small"
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <TextValidator
                          className="w-100"
                          label={
                            <span className="font exper">
                              <span style={{ color: "red" }}> * </span>
                              Lý do nghỉ việc
                            </span>
                          }
                          onChange={handleChangeExperience}
                          type="text"
                          name="leavingReason"
                          value={experience?.leavingReason || ""}
                          validators={["required", "maxStringLength:500"]}
                          errorMessages={[
                            "Không được bỏ trống!",
                            "Tối đa 500 ký tự!",
                          ]}
                          variant="outlined"
                          size="small"
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <TextValidator
                          className="w-100"
                          multiline
                          label={
                            <span className="font exper">
                              <span style={{ color: "red" }}> * </span>
                              Mô tả công việc
                            </span>
                          }
                          onChange={handleChangeExperience}
                          type="text"
                          name="jobDescription"
                          value={experience?.jobDescription || ""}
                          validators={["required", "maxStringLength:500"]}
                          errorMessages={[
                            "Không được bỏ trống!",
                            "Tối đa 500 ký tự!",
                          ]}
                          variant="outlined"
                          size="small"
                        />
                      </Grid>
                    </Grid>
                    <Grid item className="btn-center">
                      <Button variant="contained" color="primary" type="submit">
                        Lưu
                      </Button>

                      <Button
                        variant="contained"
                        color="secondary"
                        className="ml-5"
                        onClick={() => {
                          setExperience();
                          setIsEdit4(false);
                        }}
                      >
                        Hủy
                      </Button>
                    </Grid>
                  </>
                ) : (
                  <>
                    {experienceList.map((experience) => (
                      <div key={experience.id}>
                        <h6>
                          {formatMY(experience?.startDate)} -{" "}
                          {formatMY(experience?.endDate)} &ensp; &#8226; &ensp;
                          <span className="inputName">
                            {experience.companyName}
                          </span>{" "}
                          {!isView && (
                            <span>
                              {renderIconButton("edit", "secondary", () => {
                                setExperience(experience);
                                setExperienceId(experience.id);
                                setIsEdit4(true);
                              })}

                              {renderIconButton("delete", "primary", () => {
                                setExperienceId(experience?.id);
                                setShouldOpenConfirmationDialog(true);
                              })}
                            </span>
                          )}
                        </h6>
                        <h6 className="inputName">
                          {experience.companyAddress}
                        </h6>
                        <ul className="work">
                          {experience?.jobDescription
                            ? experience?.jobDescription
                                .split("\n")
                                .filter(
                                  (jobDescription) =>
                                    jobDescription.trim() !== ""
                                )
                                .map((jobDescription, index) => (
                                  <li key={index}>{jobDescription}</li>
                                ))
                            : "Không có"}
                        </ul>
                      </div>
                    ))}
                  </>
                )}
              </Grid>
            </ValidatorForm>
            <Grid
              className="cv_experience cv_right cc"
              container
              lg={12}
              md={12}
              sm={12}
              xs={12}
            >
              <h5>CHỨNG CHỈ</h5>
              <ul className="work">
                <li className="">
                  <span>2017: </span>
                  <span>Chứng chỉ tin học văn phòng Microsoft Office</span>
                </li>
                <li className="">
                  <span>2019: </span>
                  <span>Chứng chỉ khóa kĩ năng thuyết trình chuyên nghiệp</span>
                </li>
                <li className="">
                  <span>2021: </span> <span>TOIEC 800</span>
                </li>
              </ul>
            </Grid>
            <ValidatorForm
              onSubmit={(e) => {
                handleFormSubmit(e);
              }}
              className="w-100"
            >
              <Grid
                className="cv_experience cv_right cv_left wp_hv"
                container
                lg={12}
                md={12}
                sm={12}
                xs={12}
              >
                <h5>
                  HỌC VẤN{" "}
                  {isEdit3 !== isView
                    ? ""
                    : renderIconButton("edit", "secondary", () => {
                        setIsEdit3(true);
                      })}
                </h5>
                {isEdit3 ? (
                  <>
                    <TextValidator
                      onChange={handleChangeValue}
                      fullWidth
                      multiline
                      className=""
                      type="text"
                      name="knowledge"
                      value={employeeInfo?.knowledge || ""}
                      size="small"
                      variant="outlined"
                      placeholder="Nhập hoạt động của bạn"
                      validators={["maxStringLength:1000"]}
                      errorMessages={["Tối đa 1000 ký tự"]}
                    />
                    <Grid item className="btn-center">
                      <Button variant="contained" color="primary" type="submit">
                        Lưu
                      </Button>

                      <Button
                        variant="contained"
                        color="secondary"
                        className="ml-5"
                        onClick={() => {
                          setIsEdit3(false);
                        }}
                      >
                        Hủy
                      </Button>
                    </Grid>
                  </>
                ) : (
                  <ul className="work">
                    {employeeInfo?.knowledge
                      ? employeeInfo?.knowledge
                          .split("\n")
                          .filter((knowledge) => knowledge.trim() !== "")
                          .map((knowledge, index) => (
                            <li key={index}>{knowledge}</li>
                          ))
                      : "Không có"}
                  </ul>
                )}
              </Grid>
            </ValidatorForm>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default CVDialog;
