import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Grid,
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from "@material-ui/core";
import "../../../../styles/views/_cv.scss";
import { format } from "date-fns";
import { getCertificateById } from "../../../redux/actions/CertificateActions";

const CertificateDialog = ({ employeeInfo }) => {
  const { certificateById } = useSelector((state) => ({
    certificateById: state.certificate.certificateById,
  }));
  const dispatch = useDispatch();

  useEffect(() => {
    if (employeeInfo?.id) {
      dispatch(getCertificateById(employeeInfo?.id));
    }
  }, []);

  return (
    <>
      <Grid item container spacing={2}>
        <Grid item>
          <h4 className="font-roman">THÔNG TIN VĂN BẰNG</h4>
        </Grid>
        {certificateById.length > 0 ? (
          <TableContainer component={Paper} className="bd-solid m-bot">
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell
                    className="bd-solid font-bo font-roman text-center"
                    width={"5%"}
                  >
                    STT
                  </TableCell>
                  <TableCell
                    className="bd-solid font-bo font-roman text-center"
                    width={"25%"}
                  >
                    Tên văn bằng
                  </TableCell>
                  <TableCell
                    className="bd-solid font-bo font-roman text-center"
                    width={"25%"}
                  >
                    Nội dung văn bằng
                  </TableCell>
                  <TableCell
                    className="bd-solid font-bo font-roman text-center"
                    width={"20%"}
                  >
                    Ngày có hiệu lực
                  </TableCell>
                  <TableCell
                    className="bd-solid font-bo font-roman text-center"
                    width={"25%"}
                  >
                    Xếp loại
                  </TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {certificateById.map((row, index) => (
                  <TableRow key={row.id}>
                    <TableCell className="bd-solid font-roman text-center">
                      {index + 1}
                    </TableCell>
                    <TableCell className="bd-solid font-roman">
                      {row.certificateName}
                    </TableCell>
                    <TableCell className="bd-solid font-roman">
                      {row.content}
                    </TableCell>
                    <TableCell className="bd-solid font-roman text-center">
                      {row?.issueDate
                        ? format(new Date(row?.issueDate), "dd/MM/yyyy")
                        : null}
                    </TableCell>
                    <TableCell className="bd-solid font-roman text-center">
                      {row.field}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        ) : (
          <Table
            aria-label="simple table"
            className="ml-10 font-bo font-roman"
            width={"100%"}
          >
            Không có văn bằng
          </Table>
        )}
      </Grid>
    </>
  );
};

export default CertificateDialog;
