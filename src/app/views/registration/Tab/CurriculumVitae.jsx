import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Grid,
  Avatar,
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from "@material-ui/core";
import { GENDER, RELATIONSHIPS } from "../../component/constants";
import { getFamilyById } from "../../../redux/actions/FamilyActions";
import { formatDate } from "app/views/utilities/formatDate";
import "../../../../styles/views/_cv.scss";

const CurriculumVitae = ({ employeeInfo }) => {
  const { familyById } = useSelector((state) => ({
    familyById: state.family.familyById,
  }));
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getFamilyById(employeeInfo.id));
  }, []);

  return (
    <>
      <Grid container spacing={1} className="bg">
        <Grid container>
          <Grid container justifyContent="start" xs={4}>
            <Avatar src={employeeInfo?.image || ""} className="avatar" />
          </Grid>
          <Grid container xs={8}>
            <Grid container direction="column" alignItems="center">
              <Grid item>
                <h3 className="fontB">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</h3>
              </Grid>
              <Grid item>
                <h4 className="fontB">Độc lập - Tự do - Hạnh phúc</h4>
              </Grid>
              <Grid item>
                <h4 className="fontB">
                  ----------------------------------------
                </h4>
              </Grid>
            </Grid>
            <Grid container direction="column" alignItems="center">
              <Grid item>
                <h3 className="fontB">SƠ YẾU LÝ LỊCH</h3>
              </Grid>
              <Grid item>
                <h4 className="fontB">TỰ THUẬT</h4>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid container spacing={4} className="m-25px font-roman">
          <Grid item>
            <h4>I. THÔNG TIN BẢN THÂN</h4>
          </Grid>
          <Grid item container spacing={2}>
            <Grid item container xs={6}>
              <Grid item> 1. Họ và tên : </Grid>
              <Grid item xs className="inputName inputBottom">
                {employeeInfo?.name}
              </Grid>
            </Grid>
            <Grid item container xs>
              <Grid item> Giới tính:</Grid>
              <Grid item xs className="inputBottom">
                {GENDER.find((gender) => gender.CODE === employeeInfo?.gender)
                  ?.NAME || ""}
              </Grid>
            </Grid>
          </Grid>
          <Grid item container>
            <Grid item> 2. Sinh ngày: </Grid>
            <Grid item xs className="inputBottom">
              {employeeInfo?.dateOfBirth
                ? formatDate(employeeInfo.dateOfBirth)
                : null}
            </Grid>
          </Grid>
          <Grid item container>
            <Grid item> 3. Nơi ở hiện nay: &nbsp;</Grid>
            <Grid item xs className="inputBottom">
              {employeeInfo?.address}
            </Grid>
          </Grid>
          <Grid item container spacing={2}>
            <Grid item container xs={6}>
              <Grid item> 4. Điện thoại: &nbsp;</Grid>
              <Grid item xs className="inputBottom">
                {employeeInfo?.phone}
              </Grid>
            </Grid>
            <Grid item container xs={6}>
              <Grid item> Email: &nbsp;</Grid>
              <Grid item xs className="inputBottom">
                {employeeInfo?.email}
              </Grid>
            </Grid>
          </Grid>
          <Grid item container spacing={2}>
            <Grid item container xs={6}>
              <Grid item> 5. Dân tộc: &nbsp;</Grid>
              <Grid item xs className="inputBottom">
                {employeeInfo?.ethnic}
              </Grid>
            </Grid>
            <Grid item container xs={6}>
              <Grid item> Tôn giáo: &nbsp;</Grid>
              <Grid item xs className="inputBottom">
                {employeeInfo?.religion}
              </Grid>
            </Grid>
          </Grid>
          <Grid item container spacing={2}>
            <Grid item container xs={6}>
              <Grid item> 6. Số CCCD: &nbsp;</Grid>
              <Grid item xs className="inputBottom">
                {employeeInfo?.citizenIdentificationNumber}
              </Grid>
            </Grid>
            <Grid item container xs={6}>
              <Grid item> Ngày cấp: &nbsp;</Grid>
              <Grid item xs className="inputBottom">
                {employeeInfo?.dateOfIssuanceCard
                  ? formatDate(employeeInfo.dateOfIssuanceCard)
                  : null}
              </Grid>
            </Grid>
          </Grid>
          <Grid item container>
            <Grid item> 7. Nơi cấp: &nbsp;</Grid>
            <Grid item xs className="inputBottom">
              {employeeInfo?.placeOfIssueCard}
            </Grid>
          </Grid>
          <Grid item container spacing={2}>
            <Grid item>
              <h4>II. QUAN HỆ GIA ĐÌNH</h4>
            </Grid>
            <TableContainer component={Paper} className="bd-solid m-bot">
              <Table aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell
                      className="bd-solid font-bo font-roman text-center"
                      width={"7%"}
                    >
                      STT
                    </TableCell>
                    <TableCell
                      className="bd-solid font-bo font-roman text-center"
                      width={"20%"}
                    >
                      Họ và tên
                    </TableCell>
                    <TableCell
                      className="bd-solid font-bo font-roman text-center"
                      width={"15%"}
                    >
                      Năm sinh
                    </TableCell>
                    <TableCell
                      className="bd-solid font-bo font-roman text-center"
                      width={"20%"}
                    >
                      Căn cước công dân
                    </TableCell>
                    <TableCell
                      className="bd-solid font-bo font-roman text-center"
                      width={"13%"}
                    >
                      Quan hệ
                    </TableCell>
                    <TableCell
                      className="bd-solid font-bo font-roman text-center"
                      width={"25%"}
                    >
                      Địa chỉ
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {familyById && familyById.length > 0 ? (
                    familyById.map((row, index) => (
                      <TableRow key={row.id}>
                        <TableCell className="bd-solid font-roman text-center">
                          {index + 1}
                        </TableCell>
                        <TableCell className="bd-solid font-roman">
                          {row.name}
                        </TableCell>
                        <TableCell className="bd-solid font-roman text-center">
                          {row?.dateOfBirth
                            ? formatDate(row.dateOfBirth)
                            : null}
                        </TableCell>
                        <TableCell className="bd-solid font-roman text-center">
                          {row.citizenIdentificationNumber}
                        </TableCell>
                        <TableCell className="bd-solid font-roman text-center">
                          {
                            RELATIONSHIPS.find(
                              (relationShip) =>
                                relationShip.CODE === row.relationShip
                            )?.NAME
                          }
                        </TableCell>
                        <TableCell className="bd-solid font-roman">
                          {row.address}
                        </TableCell>
                      </TableRow>
                    ))
                  ) : (
                    <TableRow>
                      <TableCell
                        colSpan={6}
                        className="bd-solid font-roman"
                      ></TableCell>
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
          <Grid item container spacing={2}>
            <Grid item>
              <h4>III. LỜI CAM ĐOAN</h4>
            </Grid>
            <Grid item>
              Tôi xin cam đoan bản khai sơ yếu lý lịch trên đúng sự thật, nếu có
              điều gì không đúng tôi chịu trách nhiệm trước pháp luật về lời
              khai của mình.
            </Grid>
          </Grid>
          <Grid container justifyContent="flex-end" className="sig">
            <Grid item>
              <Grid item>
                Hà Nội, ngày {new Date().getDate().toString().padStart(2, "0")}{" "}
                tháng {(new Date().getMonth() + 1).toString().padStart(2, "0")}{" "}
                năm {new Date().getFullYear()}
              </Grid>
              <Grid item container direction="column" alignItems="center">
                <Grid item className="font-bo">
                  NGƯỜI LÀM ĐƠN
                </Grid>
                <Grid item className="sign_name">(Ký, ghi rõ họ tên)</Grid>
              </Grid>
              <Grid item className="pb-50"></Grid>
              <Grid item className="font-bo text-center pb-50">
                {employeeInfo.name}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default CurriculumVitae;
