import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  ValidatorForm,
  TextValidator,
  SelectValidator,
} from "react-material-ui-form-validator";
import {
  Dialog,
  Button,
  DialogActions,
  DialogTitle,
  DialogContent,
  Grid,
  MenuItem,
} from "@material-ui/core";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "../../../styles/views/_style.scss";
import { updateEmployees } from "../../redux/actions/EmployeeActions";
import { getAllLeader } from "../../redux/actions/LeaderActions";
import { parse, format } from "date-fns";
import { LEADER } from "../component/constants";

toast.configure({
  autoClose: 1000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const LeaderShipDialog = ({
  open,
  setShowLeaderShipDialog,
  handleShowRegistration,
  handleShowEditorDialog,
  employeeInfo,
  setEmployeeInfo,
}) => {
  const { leaderList } = useSelector((state) => ({
    leaderList: state.leader.leaderList,
  }));

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllLeader());
  }, []);

  const handleFormSubmit = (e) => {
    dispatch(updateEmployees(employeeInfo));
    handleShowEditorDialog(false, {});
    handleShowRegistration(false, employeeInfo, true);
    setShowLeaderShipDialog(false);
  };

  const handleChangeValue = (event) => {
    if (event.target.name === "leaderId") {
      setEmployeeInfo({
        ...employeeInfo,
        leaderId: event.target.value,
        leaderPosition: leaderList?.find(
          (leader) => leader.id === event.target.value
        ).leaderPosition,
        leaderName: leaderList?.find(
          (leader) => leader.id === event.target.value
        ).leaderName,
      });
    } else {
      setEmployeeInfo({
        ...employeeInfo,
        [event.target.name]: event.target.value,
      });
    }
  };

  const handleDateChange = (event) => {
    const timestamp = parse(
      event.target.value,
      "yyyy-MM-dd",
      new Date()
    ).getTime();
    setEmployeeInfo({
      ...employeeInfo,
      [event.target.name]: timestamp,
    });
  };

  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth={"sm"}
      fullWidth={true}
    >
      <ValidatorForm
        onSubmit={(e) => {
          handleFormSubmit(e);
        }}
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          Trình lãnh đạo
        </DialogTitle>

        <DialogContent>
          <Grid container spacing={1}>
            <Grid item lg={4} md={4} sm={4} xs={12}>
              <TextValidator
                className="w-100"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    {"Ngày gửi"}
                  </span>
                }
                onChange={(event) => handleDateChange(event)}
                type="date"
                name="submitDay"
                value={
                  employeeInfo?.submitDay
                    ? format(new Date(employeeInfo?.submitDay), "yyyy-MM-dd")
                    : format(new Date(), "yyyy-MM-dd")
                }
                validators={["required"]}
                errorMessages={["Vui lòng chọn ngày gửi."]}
                variant="outlined"
                size="small"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Grid>
            <Grid item lg={4} md={4} sm={4} xs={12}>
              <SelectValidator
                className="w-100"
                variant="outlined"
                size="small"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Tên lãnh đạo
                  </span>
                }
                onChange={(e) => handleChangeValue(e)}
                name="leaderId"
                value={employeeInfo?.leaderId || ""}
                validators={["required"]}
                errorMessages={["Vui lòng chọn lãnh đạo."]}
              >
                {leaderList && leaderList.length > 0 ? (
                  leaderList.map((item) => (
                    <MenuItem key={item.id} value={item.id}>
                      {item.leaderName}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem disabled>Không có</MenuItem>
                )}
              </SelectValidator>
            </Grid>
            <Grid item lg={4} md={4} sm={4} xs={12}>
              <TextValidator
                className="w-100"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Chức vụ
                  </span>
                }
                type="text"
                name="leaderPosition"
                value={
                  LEADER.find(
                    (team) => team.CODE === employeeInfo?.leaderPosition
                  )?.NAME || ""
                }
                variant="outlined"
                size="small"
                readOnly
                disabled
              />
            </Grid>
            <Grid item lg={12} md={12} sm={12} xs={12}>
              <TextValidator
                multiline
                className="w-100"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Nội dung
                  </span>
                }
                onChange={(event) => handleChangeValue(event)}
                type="text"
                name="submitContent"
                value={employeeInfo?.submitContent || ""}
                validators={["required"]}
                errorMessages={["Vui lòng điền nội dung."]}
                variant="outlined"
                size="small"
              />
            </Grid>
          </Grid>
        </DialogContent>

        <DialogActions className="justifyCenter">
          <Button
            variant="contained"
            color="primary"
            type="submit"
            onClick={() => {
              setEmployeeInfo({
                ...employeeInfo,
                submitProfileStatus: "2",
              });
            }}
          >
            Trình lãnh đạo
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => {
              setShowLeaderShipDialog(false);
            }}
          >
            Hủy
          </Button>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
};

export default LeaderShipDialog;
