import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Dialog,
  Button,
  DialogActions,
  DialogTitle,
  DialogContent,
  AppBar,
  Tabs,
  Tab,
  Box,
  Typography,
  Grid,
  Avatar,
  Icon,
} from "@material-ui/core";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { toast } from "react-toastify";
import PropTypes from "prop-types";
import { TEAMS, GENDER, APPROVED_APPBAR } from "../component/constants";
import { format } from "date-fns";
import Salary from "./Tabs/salary";
import Process from "./Tabs/process";
import Proposal from "./Tabs/proposal";
import { getAllLeader } from "../../redux/actions/LeaderActions";
import "react-toastify/dist/ReactToastify.css";
import "../../../styles/views/_loadding.scss";
import "../../../styles/views/_style.scss";
import "../../../styles/views/_approved.scss";

toast.configure({
  autoClose: 1000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box py={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const ApprovedDialog = ({
  open,
  handleShowRegistration,
  setShowApprovedDialog,
  handleShouldEndEmployee,
  employeeInfo,
  setEmployeeInfo,
  isView,
  isManage,
}) => {
  const dispatch = useDispatch();

  const { leaderList } = useSelector((state) => ({
    leaderList: state.leader.leaderList,
  }));

  const [value, setValue] = useState(0);
  const [salary, setSalary] = useState();
  const [process, setProcess] = useState();
  const [proposal, setProposal] = useState();

  useEffect(() => {
    dispatch(getAllLeader());
  }, [value]);

  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth={"lg"}
      fullWidth={true}
    >
      <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
        Cập nhật diễn biến
      </DialogTitle>

      <DialogContent>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={(event, newValue) => {
              setValue(newValue);
            }}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            aria-label="full width tabs example"
          >
            <Tab label={APPROVED_APPBAR.SALARY.name} />
            <Tab label={APPROVED_APPBAR.PROCESS.name} />
            <Tab label={APPROVED_APPBAR.PROPOSAL.name} />
          </Tabs>
        </AppBar>
        <Grid container>
          <Grid item lg={3} md={3} sm={3} xs={12}>
            <Box p={3}>
              <Grid
                item
                lg={12}
                md={12}
                sm={12}
                xs={12}
                className="approved_avatar"
              >
                <Avatar src={employeeInfo.image || ""} />
              </Grid>

              <Grid item lg={12} md={12} sm={12} xs={12} className="">
                <Grid
                  className="approved_name"
                  item
                  lg={12}
                  md={12}
                  sm={12}
                  xs={12}
                >
                  <div>{employeeInfo?.name || ""}</div>
                </Grid>
                <Grid
                  className="approved_team"
                  item
                  lg={12}
                  md={12}
                  sm={12}
                  xs={12}
                >
                  <div>
                    {TEAMS.find((team) => team.CODE === employeeInfo?.team)
                      ?.NAME || ""}
                  </div>
                </Grid>
              </Grid>
              <Grid container lg={12} md={12} sm={12} xs={12} spacing={1}>
                <Grid className="cv_icon" item lg={6} md={6} sm={6} xs={6}>
                  <Icon fontSize="small">wc</Icon>
                  <div>
                    {GENDER.find(
                      (gender) => gender.CODE === employeeInfo?.gender
                    )?.NAME || ""}
                  </div>
                </Grid>
                <Grid className="cv_icon" item lg={6} md={6} sm={6} xs={6}>
                  <Icon fontSize="small">email</Icon>
                  <div>{employeeInfo?.email || ""}</div>
                </Grid>
                <Grid className="cv_icon" item lg={6} md={6} sm={6} xs={6}>
                  <Icon fontSize="small">event</Icon>
                  <div>
                    {employeeInfo?.dateOfBirth
                      ? format(
                          new Date(employeeInfo?.dateOfBirth),
                          "dd/MM/yyyy"
                        )
                      : null}
                  </div>
                </Grid>
                <Grid className="cv_icon" item lg={6} md={6} sm={6} xs={6}>
                  <Icon fontSize="small">phone</Icon>
                  <div>{employeeInfo?.phone || ""}</div>
                </Grid>
                <Grid className="cv_icon" item lg={12} md={12} sm={12} xs={12}>
                  <Icon fontSize="small">business</Icon>
                  <div>{employeeInfo?.address || ""}</div>
                </Grid>
              </Grid>
            </Box>
          </Grid>
          <Grid item lg={9} md={9} sm={9} xs={12}>
            <TabPanel value={value} index={APPROVED_APPBAR.SALARY.value}>
              <Salary
                isManage={isManage}
                employeeInfo={employeeInfo}
                leaderList={leaderList}
                salary={salary}
                setSalary={setSalary}
              />
            </TabPanel>
            <TabPanel value={value} index={APPROVED_APPBAR.PROCESS.value}>
              <Process
                isManage={isManage}
                employeeInfo={employeeInfo}
                leaderList={leaderList}
                process={process}
                setProcess={setProcess}
              />
            </TabPanel>
            <TabPanel value={value} index={APPROVED_APPBAR.PROPOSAL.value}>
              <Proposal
                isManage={isManage}
                employeeInfo={employeeInfo}
                leaderList={leaderList}
                proposal={proposal}
                setProposal={setProposal}
              />
            </TabPanel>
          </Grid>
        </Grid>
      </DialogContent>

      <DialogActions className="justifyCenter">
        {isView && (
          <>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                handleShowRegistration();
              }}
            >
              Xem hồ sơ
            </Button>
            {isManage && (
              <Button
                variant="contained"
                color="primary"
                onClick={() => {
                  handleShouldEndEmployee(true, false);
                }}
              >
                Kết thúc
              </Button>
            )}
            {employeeInfo?.endDay && !isManage && (
              <Button
                variant="contained"
                color="primary"
                onClick={() => {
                  handleShouldEndEmployee(false, false);
                }}
              >
                Đơn nghỉ việc
              </Button>
            )}
          </>
        )}

        <Button
          variant="contained"
          color="secondary"
          onClick={() => {
            setEmployeeInfo();
            setShowApprovedDialog();
          }}
        >
          Hủy
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ApprovedDialog;
