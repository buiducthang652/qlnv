import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Grid, TextField } from "@material-ui/core";
import TablePage from "../component/Table/TablePage";
import { searchEmployees } from "../../redux/actions/EmployeeActions";
import { Breadcrumb } from "egret";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  GENDER,
  TEAMS,
  SUBMIT_PROFILE_STATUS,
  EMPLOYEE_STATUS,
} from "../component/constants";
import { formatDate } from "../utilities/formatDate";
import "../../../styles/views/_pending.scss";
import ApprovedDialog from "./ApprovedDialog";
import RegistrationDialog from "../registration/RegistrationDialog";
import { ConfirmationDialog } from "egret";
import EndEmployeeDialog from "../component/FormDialog/endEmployeeDialog";
import { renderIconButton } from "../utilities/renderButton";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const ApprovedManage = () => {
  const { employeeList, totalElements } = useSelector((state) => ({
    employeeList: state.employee.employeeList,
    totalElements: state.employee.totalElements,
  }));

  const dispatch = useDispatch();

  const [pageSize, setPageSize] = useState(10);
  const [page, setPage] = useState(0);
  const [keyword, setKeyword] = useState("");
  const [employeeInfo, setEmployeeInfo] = useState();
  const [showApprovedDialog, setShowApprovedDialog] = useState(false);
  const [shouldOpenRegistration, setShouldOpenRegistration] = useState(false);
  const [
    shouldNotificationConfirmationDialog,
    setShouldNotificationConfirmationDialog,
  ] = useState(false);

  const [showEndEmployeeDialog, setShowEndEmployeeDialog] = useState(false);
  const [isView, setIsView] = useState(false);
  const [isManage, setIsManage] = useState(false);

  const columns = [
    {
      title: "STT",
      align: "center",
      width: "5%",
      minWidth: "40px",
      render: (rowData) => {
        return <>{rowData.tableData.id + 1 + page * pageSize}</>;
      },
    },
    {
      title: "Thao tác",
      field: "custom",
      align: "center",
      minWidth: "95px",
      render: (rowData) => (
        <div>
          {EMPLOYEE_STATUS.APPROVED.includes(rowData.submitProfileStatus) &&
            renderIconButton("visibility", "secondary", () => {
              setEmployeeInfo({
                ...rowData,
              });
              setShowApprovedDialog(true);
              setIsView(true);
            })}

          {EMPLOYEE_STATUS.NOTIFICATION_APPROVED.includes(
            rowData.submitProfileStatus
          ) &&
            renderIconButton("notifications_active", "error", () => {
              setEmployeeInfo(rowData);
              setShouldNotificationConfirmationDialog(true);
            })}
        </div>
      ),
    },
    {
      title: "Họ và tên",
      field: "name",
      align: "center",
      minWidth: "140px",
      cellStyle: {
        textAlign: "left",
      },
    },
    {
      title: "Giới tính",
      field: "gender",
      align: "center",
      minWidth: "75px",
      render: (rowData) => {
        return (
          <span>
            {GENDER.find((gender) => gender.CODE === rowData.gender)?.NAME}
          </span>
        );
      },
    },
    {
      title: "Nhóm",
      field: "team",
      align: "center",
      minWidth: "75px",
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => {
        return (
          <span>{TEAMS.find((team) => team.CODE === rowData.team)?.NAME}</span>
        );
      },
    },
    {
      title: "Email",
      field: "email",
      align: "center",
      minWidth: "120px",
      cellStyle: {
        textAlign: "left",
      },
    },
    {
      title: "Số điện thoại",
      field: "phone",
      align: "center",
      minWidth: "120px",
    },
    {
      title: "Ngày sinh",
      field: "dateOfBirth",
      align: "center",
      minWidth: "120px",
      render: (rowData) => {
        return <span>{formatDate(rowData.dateOfBirth)}</span>;
      },
    },
    {
      title: "Trạng thái",
      field: "submitProfileStatus",
      align: "center",
      minWidth: "120px",
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => {
        return (
          <span>
            {
              SUBMIT_PROFILE_STATUS.find(
                (profileStatus) =>
                  profileStatus.CODE.toString() ===
                  rowData.submitProfileStatus.toString()
              )?.NAME
            }
          </span>
        );
      },
    },
  ];
  useEffect(() => {
    const searchObject = {
      keyword,
      pageIndex: page + 1,
      pageSize,
      listStatus: EMPLOYEE_STATUS.APPROVED,
    };
    dispatch(searchEmployees(searchObject));
  }, [pageSize, page, keyword]);

  const handleShowRegistration = () => {
    setShouldOpenRegistration(!shouldOpenRegistration);
  };

  const handleShouldEndEmployee = (status, manage) => {
    setShowEndEmployeeDialog(!showEndEmployeeDialog);
    setIsView(status);
    setIsManage(manage);
  };

  return (
    <div className="m-sm-30 employee">
      <Grid container className="">
        <Grid item xs={12} container justifyContent="space-between">
          <Grid item sx={6}>
            <Breadcrumb
              routeSegments={[
                {
                  name: "Quản lý",
                  path: "/dashboard/analytics",
                },
                { name: "Danh sách nhân viên" },
              ]}
            />
          </Grid>
          <>
            {/* Cập nhật diễn biến */}
            {showApprovedDialog && (
              <ApprovedDialog
                open={showApprovedDialog}
                handleShowRegistration={handleShowRegistration}
                handleShouldEndEmployee={handleShouldEndEmployee}
                setShowApprovedDialog={setShowApprovedDialog}
                employeeInfo={employeeInfo}
                setEmployeeInfo={setEmployeeInfo}
                isView={isView}
                isManage={isManage}
              />
            )}

            {/* Danh sách nhân viên */}
            {shouldOpenRegistration && (
              <RegistrationDialog
                open={shouldOpenRegistration}
                handleShowRegistration={handleShowRegistration}
                setShowApprovedDialog={setShowApprovedDialog}
                employeeInfo={employeeInfo}
                setEmployeeInfo={setEmployeeInfo}
                pending={false}
              />
            )}

            {/* Thông báo trạng thái */}
            {shouldNotificationConfirmationDialog && (
              <ConfirmationDialog
                title={
                  employeeInfo?.submitProfileStatus === "8"
                    ? "Bổ sung"
                    : "Từ chối"
                }
                open={shouldNotificationConfirmationDialog}
                onConfirmDialogClose={() => {
                  setEmployeeInfo();
                  setShouldNotificationConfirmationDialog(false);
                }}
                text={
                  employeeInfo?.submitProfileStatus === "8"
                    ? employeeInfo?.additionalRequestTermination || "Không có"
                    : employeeInfo?.reasonForRefuseEndProfile || "Không có"
                }
                No="Hủy"
              />
            )}

            {/* Đơn xin nghỉ việc */}
            {showEndEmployeeDialog && (
              <EndEmployeeDialog
                open={showEndEmployeeDialog}
                handleShouldEndEmployee={handleShouldEndEmployee}
                setShowApprovedDialog={setShowApprovedDialog}
                employeeInfo={employeeInfo}
                setEmployeeInfo={setEmployeeInfo}
                isView={isView}
                isManage={isManage}
              />
            )}
          </>
          <Grid item sx={12} className="pending_search">
            <TextField
              variant="standard"
              placeholder="Tìm kiếm"
              onChange={(event) => {
                setPage(0);
                setKeyword(event.target.value);
              }}
            />
          </Grid>
        </Grid>
        <TablePage
          data={employeeList}
          columns={columns}
          totalElements={totalElements}
          page={page}
          pageSize={pageSize}
          setPage={setPage}
          setPageSize={setPageSize}
        />
      </Grid>
    </div>
  );
};
export default ApprovedManage;
