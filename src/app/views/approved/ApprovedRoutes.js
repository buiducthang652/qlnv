import { EgretLoadable } from "egret";
import { authRoles } from "../../auth/authRoles";
import ConstantList from "../../appConfig";
import { withTranslation } from "react-i18next";

const Approved = EgretLoadable({
  loader: () => import("./Approved"),
});

const ApprovedManage = EgretLoadable({
  loader: () => import("./ApprovedManage"),
});

const ViewComponent = withTranslation()(Approved);
const ViewManageComponent = withTranslation()(ApprovedManage);

const approvedRoutes = [
  {
    path: ConstantList.ROOT_PATH + "employee_manager/approved",
    component: ViewComponent,
    auth: authRoles.admin,
  },
  {
    path: ConstantList.ROOT_PATH + "employee_manager/approvedManage",
    component: ViewManageComponent,
    auth: authRoles.admin,
  },
];

export default approvedRoutes;
