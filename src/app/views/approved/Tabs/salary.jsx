import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Grid, Button, MenuItem } from "@material-ui/core";
import TableDialog from "app/views/component/Table/TableDialog";
import {
  ValidatorForm,
  TextValidator,
  SelectValidator,
} from "react-material-ui-form-validator";
import { ConfirmationDialog } from "egret";
import { toast } from "react-toastify";
import {
  EMPLOYEE_STATUS,
  SUBMIT_PROFILE_STATUS,
} from "../../component/constants";
import { formatDate } from "../../utilities/formatDate";
import {
  saveSalary,
  deleteSalary,
  updateSalary,
  getByEmployeeIdSalary,
} from "../../../redux/actions/SalaryAction";
import { parse, format } from "date-fns";
import SalaryDialog from "../../component/FormDialog/salaryDialog";
import "react-toastify/dist/ReactToastify.css";
import "../../../../styles/views/_development.scss";
import {
  addValidationRules,
  removeValidationRules,
} from "app/views/utilities/validatorRule";
import { renderIconButton } from "app/views/utilities/renderButton";
import { formatVND } from "app/views/utilities/formatCurrency";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const checkStatus = (salaryList) => {
  return salaryList.some(
    (item) =>
      item.salaryIncreaseStatus === 1 ||
      item.salaryIncreaseStatus === 2 ||
      item.salaryIncreaseStatus === 4 ||
      item.salaryIncreaseStatus === 5
  );
};

const Salary = ({ isManage, employeeInfo, leaderList, salary, setSalary }) => {
  const dispatch = useDispatch();

  const { salaryList } = useSelector((state) => ({
    salaryList: state.salary.salaryList,
  }));

  const [showConfirmDialog, setShowConfirmDialog] = useState(false);
  const [item, setItem] = useState();
  const [salaryId, setSalaryId] = useState();
  const [send, setSend] = useState(true);
  const [showSalaryDialog, setShowSalaryDialog] = useState(false);
  const [isView, setIsView] = useState(true);
  const [
    shouldNotificationConfirmationDialog,
    setShouldNotificationConfirmationDialog,
  ] = useState(false);
  const [isDisabled, setIsDisabled] = useState(false);

  useEffect(() => {
    if (setSalary) {
      dispatch(getByEmployeeIdSalary(employeeInfo.id));
    }
    setSalary();

    addValidationRules();

    return () => {
      removeValidationRules();
    };
  }, []);

  useEffect(() => {
    setIsDisabled(checkStatus(salaryList));
  }, [salaryList]);

  const handleSubmit = (e) => {
    console.log(item);
    if (salary?.leaderId) {
      setShowSalaryDialog(true);
    } else {
      if (item?.id) {
        dispatch(updateSalary(item));
        setSalary();
      } else {
        dispatch(saveSalary(employeeInfo.id, item));
        setSalary();
      }
    }
  };

  const handleChangeValue = (e) => {
    if (e.target.name === "leaderId") {
      setSalary({
        ...salary,
        [e.target.name]: e.target.value,
      });
      setSend(false);
    } else {
      setSalary({
        ...salary,
        [e.target.name]: e.target.value,
      });
    }
  };

  const handleDateChange = (event) => {
    const timestamp = parse(
      event.target.value,
      "yyyy-MM-dd",
      new Date()
    ).getTime();
    setSalary({
      ...salary,
      [event.target.name]: timestamp,
    });
  };

  const columns = [
    {
      title: "STT",
      align: "center",
      width: "5%",
      minWidth: "40px",
      render: (rowData) => {
        return <>{rowData.tableData.id + 1}</>;
      },
    },
    {
      title: "Thao tác",
      field: "custom",
      align: "center",
      width: "5%",
      minWidth: "120px",
      render: (rowData) => (
        <>
          {isManage &&
            EMPLOYEE_STATUS.EDIT.includes(rowData.salaryIncreaseStatus) &&
            renderIconButton("edit", "primary", () => {
              setIsDisabled(false);
              setSalary(rowData);
            })}
          {isManage &&
            EMPLOYEE_STATUS.DELETE.includes(rowData.salaryIncreaseStatus) &&
            renderIconButton("delete", "error", () => {
              setSalaryId(rowData.id);
              setShowConfirmDialog(true);
            })}
          {EMPLOYEE_STATUS.VISIBILITY.includes(rowData.salaryIncreaseStatus) &&
            renderIconButton("visibility", "secondary", () => {
              setIsView(false);
              setSalary(rowData);
              setShowSalaryDialog(true);
            })}
          {EMPLOYEE_STATUS.NOTIFICATION.includes(
            rowData.salaryIncreaseStatus
          ) &&
            renderIconButton("notifications_active", "error", () => {
              setSalary(rowData);
              setShouldNotificationConfirmationDialog(true);
            })}
        </>
      ),
    },
    {
      title: "Ngày hiệu lực",
      field: "startDate",
      align: "center",
      minWidth: "120px",
      render: (rowData) => {
        return <span>{formatDate(rowData.startDate)}</span>;
      },
    },
    {
      title: "Lần thứ",
      field: "times",
      align: "center",
      minWidth: "40px",
    },
    {
      title: "Lương cũ",
      field: "oldSalary",
      align: "center",
      minWidth: "120px",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => {
        return <span>{formatVND(rowData.oldSalary)}</span>;
      },
    },
    {
      title: "Lương mới",
      field: "newSalary",
      align: "center",
      minWidth: "120px",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => {
        return <span>{formatVND(rowData.newSalary)}</span>;
      },
    },
    {
      title: "Trạng thái",
      field: "salaryIncreaseStatus",
      align: "center",
      minWidth: "120px",
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => {
        return (
          <span>
            {
              SUBMIT_PROFILE_STATUS.find(
                (salaryIncreaseStatus) =>
                  salaryIncreaseStatus.CODE.toString() ===
                  rowData.salaryIncreaseStatus.toString()
              )?.NAME
            }
          </span>
        );
      },
    },
  ];

  return (
    <>
      <ValidatorForm onSubmit={() => handleSubmit()}>
        {/* Xóa đề tăng lương */}
        {showConfirmDialog && (
          <ConfirmationDialog
            title="Xóa đề xuất tăng lương"
            open={showConfirmDialog}
            onConfirmDialogClose={() => {
              setSalaryId();
              setShowConfirmDialog(false);
            }}
            onYesClick={() => {
              dispatch(deleteSalary(salaryId));
              setShowConfirmDialog(false);
              setSalary();
            }}
            Yes={"Có"}
            No={"Không"}
          />
        )}

        {/* Đơn tăng lương */}
        {showSalaryDialog && (
          <SalaryDialog
            open={showSalaryDialog}
            salary={salary}
            setSalary={setSalary}
            isView={isView}
            setSend={setSend}
            isManage={false}
            employeeInfo={employeeInfo}
            setShowSalaryDialog={setShowSalaryDialog}
          />
        )}

        {/* Thông báo */}
        {shouldNotificationConfirmationDialog && (
          <ConfirmationDialog
            title={salary?.salaryIncreaseStatus === 4 ? "Bổ sung" : "Từ chối"}
            open={shouldNotificationConfirmationDialog}
            onConfirmDialogClose={() => {
              setSalary();
              setShouldNotificationConfirmationDialog(false);
            }}
            text={
              salary?.salaryIncreaseStatus === 4
                ? salary.additionalRequest
                : salary.reasonForRefusal
            }
            No="Hủy"
          />
        )}

        {isManage && (
          <Grid container spacing={2} className="mb-10">
            <Grid item md={2} xs={12}>
              <TextValidator
                className="w-100"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Ngày hiệu lực
                  </span>
                }
                onChange={(event) => handleDateChange(event)}
                type="date"
                name="startDate"
                value={
                  salary?.startDate
                    ? format(new Date(salary?.startDate), "yyyy-MM-dd")
                    : ""
                }
                validators={["required", "isBeforeCurrentDate"]}
                errorMessages={[
                  "Không được bỏ trống",
                  "Phải là ngày hiện tại trở đi",
                ]}
                variant="outlined"
                size="small"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Grid>

            <Grid item xs={12} md={3}>
              <TextValidator
                className="w-100"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Lương cũ
                  </span>
                }
                onChange={(e) => handleChangeValue(e)}
                type="text"
                name="oldSalary"
                value={salary?.oldSalary || ""}
                validators={["required", "isNumber"]}
                errorMessages={["Không được bỏ trống!", "Vui lòng nhập số"]}
                variant="outlined"
                size="small"
              />
            </Grid>

            <Grid item md={3} xs={12}>
              <TextValidator
                className="w-100"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Lương mới
                  </span>
                }
                onChange={(e) => handleChangeValue(e)}
                type="text"
                name="newSalary"
                value={salary?.newSalary || ""}
                validators={["required", "isNumber"]}
                errorMessages={["Không được bỏ trống!", "Vui lòng nhập số"]}
                variant="outlined"
                size="small"
              />
            </Grid>
            <Grid item lg={4} md={4} sm={4} xs={12}>
              <TextValidator
                className="w-100"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Lý do tăng lương
                  </span>
                }
                onChange={(event) => handleChangeValue(event)}
                type="text"
                name="reason"
                value={salary?.reason || ""}
                validators={["required", "maxStringLength:225"]}
                errorMessages={["Không được bỏ trống!", "Lý do quá dài"]}
                variant="outlined"
                size="small"
              />
            </Grid>

            <Grid item lg={5} md={5} sm={5} xs={12}>
              <TextValidator
                className="w-100"
                label="Ghi chú"
                onChange={(event) => handleChangeValue(event)}
                type="text"
                name="note"
                value={salary?.note || ""}
                validators={["maxStringLength:225"]}
                errorMessages={["Ghi chú quá dài"]}
                variant="outlined"
                size="small"
              />
            </Grid>

            <Grid item md={3} xs={12}>
              <SelectValidator
                className="w-100"
                variant="outlined"
                size="small"
                label="Tên lãnh đạo"
                onChange={(e) => handleChangeValue(e)}
                name="leaderId"
                value={salary?.leaderId || ""}
              >
                {leaderList && leaderList.length > 0 ? (
                  leaderList.map((item) => (
                    <MenuItem key={item.id} value={item.id}>
                      {item.leaderName}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem disabled>Không có</MenuItem>
                )}
              </SelectValidator>
            </Grid>

            <Grid item lg={4} md={4} sm={4} xs={12}>
              <div>
                {
                  <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    onClick={() => {
                      setItem({ ...salary });
                      setIsView(true);
                    }}
                    disabled={isDisabled}
                  >
                    {!send
                      ? "Trình lãnh đạo"
                      : salary?.id
                      ? "Cập nhật"
                      : "Thêm mới"}
                  </Button>
                }

                <Button
                  className="ml-10"
                  variant="contained"
                  color="secondary"
                  onClick={() => {
                    setSend(true);
                    setSalary();
                    setIsDisabled(checkStatus(salaryList));
                  }}
                >
                  Hủy
                </Button>
              </div>
            </Grid>
          </Grid>
        )}
      </ValidatorForm>

      <TableDialog data={salaryList} columns={columns} />
    </>
  );
};

export default Salary;
