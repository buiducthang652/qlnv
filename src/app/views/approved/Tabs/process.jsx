import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Grid, Button, MenuItem } from "@material-ui/core";
import TableDialog from "../../component/Table/TableDialog";
import {
  ValidatorForm,
  TextValidator,
  SelectValidator,
} from "react-material-ui-form-validator";
import { ConfirmationDialog } from "egret";
import { toast } from "react-toastify";
import {
  SUBMIT_PROFILE_STATUS,
  POSITION,
  EMPLOYEE_STATUS,
} from "../../component/constants";
import { formatDate } from "../../utilities/formatDate";
import {
  saveProcess,
  deleteProcess,
  updateProcess,
  getByEmployeeIdProcess,
} from "../../../redux/actions/ProcessAction";
import { parse, format } from "date-fns";
import ProcessDialog from "app/views/component/FormDialog/processDialog";
import "react-toastify/dist/ReactToastify.css";
import "../../../../styles/views/_development.scss";
import {
  addValidationRules,
  removeValidationRules,
} from "app/views/utilities/validatorRule";
import { renderIconButton } from "app/views/utilities/renderButton";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const checkStatus = (processList) => {
  return processList.some(
    (item) =>
      item.processStatus === "1" ||
      item.processStatus === "2" ||
      item.processStatus === "4" ||
      item.processStatus === "5"
  );
};

const Process = ({
  isManage,
  employeeInfo,
  leaderList,
  process,
  setProcess,
}) => {
  const dispatch = useDispatch();

  const { processList } = useSelector((state) => ({
    processList: state.process.processList,
  }));

  const [showConfirmDialog, setShowConfirmDialog] = useState(false);
  const [item, setItem] = useState();
  const [processId, setProcessId] = useState();
  const [send, setSend] = useState(true);
  const [showProcessDialog, setShowProcessDialog] = useState(false);
  const [isView, setIsView] = useState(true);
  const [
    shouldNotificationConfirmationDialog,
    setShouldNotificationConfirmationDialog,
  ] = useState(false);
  const [isDisabled, setIsDisabled] = useState(false);

  useEffect(() => {
    if (setProcess) {
      dispatch(getByEmployeeIdProcess(employeeInfo.id));
    }
    setProcess();

    addValidationRules();

    return () => {
      removeValidationRules();
    };
  }, []);

  useEffect(() => {
    setIsDisabled(checkStatus(processList));
  }, [processList]);

  const handleSubmit = (e) => {
    if (process?.leaderId) {
      setShowProcessDialog(true);
    } else {
      if (item?.id) {
        dispatch(updateProcess(item));
        setProcess();
      } else {
        dispatch(saveProcess(employeeInfo?.id, item));
        setProcess();
      }
    }
  };

  const handleChangeValue = (e) => {
    if (e.target.name === "leaderId") {
      setProcess({
        ...process,
        [e.target.name]: e.target.value,
      });
      setSend(false);
    } else {
      setProcess({
        ...process,
        [e.target.name]: e.target.value,
      });
    }
  };

  const handleDateChange = (event) => {
    const timestamp = parse(
      event.target.value,
      "yyyy-MM-dd",
      new Date()
    ).getTime();
    setProcess({
      ...process,
      [event.target.name]: timestamp,
    });
  };

  const columns = [
    {
      title: "STT",
      align: "center",
      width: "5%",
      minWidth: "40px",
      render: (rowData) => {
        return <>{rowData.tableData.id + 1}</>;
      },
    },
    {
      title: "Thao tác",
      field: "custom",
      align: "center",
      width: "5%",
      minWidth: "120px",
      render: (rowData) => (
        <>
          {isManage &&
            EMPLOYEE_STATUS.EDIT.includes(rowData.processStatus) &&
            renderIconButton("edit", "primary", () => {
              setIsDisabled(false);
              setProcess(rowData);
            })}
          {isManage &&
            EMPLOYEE_STATUS.DELETE.includes(rowData.processStatus) &&
            renderIconButton("delete", "error", () => {
              setProcessId(rowData.id);
              setShowConfirmDialog(true);
            })}
          {EMPLOYEE_STATUS.VISIBILITY.includes(rowData.processStatus) &&
            renderIconButton("visibility", "secondary", () => {
              setProcess(rowData);
              setIsView(false);
              setShowProcessDialog(true);
            })}
          {EMPLOYEE_STATUS.NOTIFICATION.includes(rowData.processStatus) &&
            renderIconButton("notifications_active", "error", () => {
              setProcess(rowData);
              setShouldNotificationConfirmationDialog(true);
            })}
        </>
      ),
    },
    {
      title: "Ngày tăng chức",
      field: "promotionDay",
      align: "center",
      minWidth: "120px",
      render: (rowData) => {
        return <span>{formatDate(rowData.promotionDay)}</span>;
      },
    },
    {
      title: "Vị trí cũ",
      field: "currentPosition",
      align: "center",
      minWidth: "120px",
      render: (rowData) => {
        return (
          <span>
            {
              POSITION.find(
                (position) => position.CODE === rowData?.currentPosition
              )?.NAME
            }
          </span>
        );
      },
    },
    {
      title: "Vị trí mới",
      field: "newPosition",
      align: "center",
      minWidth: "120px",
      render: (rowData) => {
        return (
          <span>
            {
              POSITION.find(
                (position) => position.CODE === rowData?.newPosition
              )?.NAME
            }
          </span>
        );
      },
    },
    {
      title: "Trạng thái",
      field: "processStatus",
      align: "center",
      minWidth: "120px",
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => {
        return (
          <span>
            {
              SUBMIT_PROFILE_STATUS.find(
                (processStatus) =>
                  processStatus.CODE.toString() ===
                  rowData.processStatus.toString()
              )?.NAME
            }
          </span>
        );
      },
    },
  ];

  return (
    <>
      <ValidatorForm onSubmit={() => handleSubmit()}>
        {showConfirmDialog && (
          <ConfirmationDialog
            title="Xóa đề xuất thăng chức"
            open={showConfirmDialog}
            onConfirmDialogClose={() => {
              setProcessId();
              setShowConfirmDialog(false);
            }}
            onYesClick={() => {
              dispatch(deleteProcess(processId));
              setShowConfirmDialog(false);
              setProcess();
            }}
            Yes={"Có"}
            No={"Không"}
          />
        )}
        {showProcessDialog && (
          <ProcessDialog
            open={showProcessDialog}
            process={process}
            setProcess={setProcess}
            isView={isView}
            setSend={setSend}
            isManage={false}
            employeeInfo={employeeInfo}
            setShowProcessDialog={setShowProcessDialog}
          />
        )}
        {shouldNotificationConfirmationDialog && (
          <ConfirmationDialog
            title={process?.processStatus === "4" ? "Bổ sung" : "Từ chối"}
            open={shouldNotificationConfirmationDialog}
            onConfirmDialogClose={() => {
              setProcess();
              setShouldNotificationConfirmationDialog(false);
            }}
            text={
              process?.processStatus === "4"
                ? process.additionalRequest
                : process.reasonForRefusal
            }
            No="Hủy"
          />
        )}

        {isManage && (
          <Grid container spacing={2} className="mb-10">
            <Grid item md={3} xs={12}>
              <TextValidator
                className="w-100"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    {"Ngày hiệu lực"}
                  </span>
                }
                onChange={(event) => handleDateChange(event)}
                type="date"
                name="promotionDay"
                value={
                  process?.promotionDay
                    ? format(new Date(process?.promotionDay), "yyyy-MM-dd")
                    : ""
                }
                validators={["required", "isBeforeCurrentDate"]}
                errorMessages={[
                  "Không được bỏ trống",
                  "Phải từ ngày hiện tại trở đi.",
                ]}
                variant="outlined"
                size="small"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Grid>

            <Grid item xs={12} md={3}>
              <TextValidator
                className="w-100"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Vị trí cũ
                  </span>
                }
                onChange={(e) => handleChangeValue(e)}
                type="text"
                name="currentPosition"
                value={
                  POSITION.find(
                    (position) =>
                      position.CODE === employeeInfo?.currentPosition
                  )?.NAME || ""
                }
                validators={["required"]}
                errorMessages={["Không được bỏ trống!"]}
                variant="outlined"
                size="small"
                disabled
              />
            </Grid>

            <Grid item xs={12} md={3}>
              <SelectValidator
                className="w-100"
                variant="outlined"
                size="small"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Vị trí mới
                  </span>
                }
                onChange={(e) => handleChangeValue(e)}
                name="newPosition"
                value={process?.newPosition || ""}
                validators={["required"]}
                errorMessages={["Không được bỏ trống"]}
              >
                {POSITION && POSITION.length > 0 ? (
                  POSITION.map((position) => (
                    <MenuItem key={position.CODE} value={position.CODE}>
                      {position.NAME}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem disabled>Không có</MenuItem>
                )}
              </SelectValidator>
            </Grid>

            <Grid item md={3} xs={12}>
              <SelectValidator
                className="w-100"
                variant="outlined"
                size="small"
                label="Tên lãnh đạo"
                onChange={(e) => handleChangeValue(e)}
                name="leaderId"
                value={process?.leaderId || ""}
              >
                {leaderList && leaderList.length > 0 ? (
                  leaderList.map((item) => (
                    <MenuItem key={item.id} value={item.id}>
                      {item.leaderName}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem disabled>Không có</MenuItem>
                )}
              </SelectValidator>
            </Grid>

            <Grid item xs={12} md={8}>
              <TextValidator
                className="w-100"
                label="Lý do"
                onChange={(event) => handleChangeValue(event)}
                type="text"
                name="note"
                value={process?.note || ""}
                validators={["maxStringLength:225"]}
                errorMessages={["Lý do quá dài"]}
                variant="outlined"
                size="small"
              />
            </Grid>

            <Grid item md={4} xs={12}>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                onClick={() => {
                  setItem({
                    ...process,
                    currentPosition: employeeInfo?.currentPosition,
                  });
                  setIsView(true);
                }}
                disabled={isDisabled}
              >
                {!send
                  ? "Trình lãnh đạo"
                  : process?.id
                  ? "Cập nhật"
                  : "Thêm mới"}
              </Button>

              <Button
                className="ml-10"
                variant="contained"
                color="secondary"
                onClick={() => {
                  setSend(true);
                  setProcess();
                  setIsDisabled(checkStatus(processList));
                }}
              >
                Hủy
              </Button>
            </Grid>
          </Grid>
        )}
      </ValidatorForm>

      <TableDialog data={processList} columns={columns} />
    </>
  );
};

export default Process;
