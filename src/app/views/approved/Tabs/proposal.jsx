import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Grid, Button, MenuItem } from "@material-ui/core";
import TableDialog from "app/views/component/Table/TableDialog";
import {
  ValidatorForm,
  TextValidator,
  SelectValidator,
} from "react-material-ui-form-validator";
import { ConfirmationDialog } from "egret";
import { toast } from "react-toastify";
import {
  EMPLOYEE_STATUS,
  SUBMIT_PROFILE_STATUS,
  TYPE,
} from "../../component/constants";
import { formatDate } from "../../utilities/formatDate";
import {
  saveProposal,
  deleteProposal,
  updateProposal,
  getByEmployeeIdProposal,
} from "../../../redux/actions/ProposalAction";
import { parse, format } from "date-fns";
import ProposalDialog from "app/views/component/FormDialog/proposalDialog";
import "react-toastify/dist/ReactToastify.css";
import "../../../../styles/views/_development.scss";
import {
  addValidationRules,
  removeValidationRules,
} from "app/views/utilities/validatorRule";
import { renderIconButton } from "app/views/utilities/renderButton";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const checkStatus = (proposalList) => {
  return proposalList.some(
    (item) =>
      item.proposalStatus === 1 ||
      item.proposalStatus === 2 ||
      item.proposalStatus === 4 ||
      item.proposalStatus === 5
  );
};

const Proposal = ({
  isManage,
  employeeInfo,
  leaderList,
  proposal,
  setProposal,
}) => {
  const dispatch = useDispatch();

  const { proposalList } = useSelector((state) => ({
    proposalList: state.proposal.proposalList,
  }));

  const [showConfirmDialog, setShowConfirmDialog] = useState(false);
  const [item, setItem] = useState();
  const [proposalId, setProposalId] = useState();
  const [send, setSend] = useState(true);
  const [showProposalDialog, setShowProposalDialog] = useState(false);
  const [isView, setIsView] = useState(true);
  const [
    shouldNotificationConfirmationDialog,
    setShouldNotificationConfirmationDialog,
  ] = useState(false);
  const [isDisabled, setIsDisabled] = useState(false);

  useEffect(() => {
    if (setProposal) {
      dispatch(getByEmployeeIdProposal(employeeInfo.id));
    }
    setProposal();

    addValidationRules();

    return () => {
      removeValidationRules();
    };
  }, []);

  useEffect(() => {
    setIsDisabled(checkStatus(proposalList));
  }, [proposalList]);

  const handleSubmit = (e) => {
    if (proposal?.leaderId) {
      setShowProposalDialog(true);
    } else {
      if (item?.id) {
        dispatch(updateProposal(item));
        setProposal();
      } else {
        dispatch(saveProposal(employeeInfo?.id, item));
        setProposal();
      }
    }
  };

  const handleChangeValue = (e) => {
    if (e.target.name === "leaderId") {
      setProposal({
        ...proposal,
        [e.target.name]: e.target.value,
      });
      setSend(false);
    } else {
      setProposal({
        ...proposal,
        [e.target.name]: e.target.value,
      });
    }
  };

  const handleDateChange = (event) => {
    const timestamp = parse(
      event.target.value,
      "yyyy-MM-dd",
      new Date()
    ).getTime();
    setProposal({
      ...proposal,
      [event.target.name]: timestamp,
    });
  };

  const columns = [
    {
      title: "STT",
      align: "center",
      width: "5%",
      minWidth: "40px",
      render: (rowData) => {
        return <>{rowData.tableData.id + 1}</>;
      },
    },
    {
      title: "Thao tác",
      field: "custom",
      align: "center",
      width: "5%",
      minWidth: "120px",
      render: (rowData) => (
        <>
          {isManage &&
            EMPLOYEE_STATUS.EDIT.includes(rowData.proposalStatus) &&
            renderIconButton("edit", "primary", () => {
              setIsDisabled(false);
              setProposal(rowData);
            })}
          {isManage &&
            EMPLOYEE_STATUS.DELETE.includes(rowData.proposalStatus) &&
            renderIconButton("delete", "error", () => {
              setProposalId(rowData.id);
              setShowConfirmDialog(true);
            })}
          {EMPLOYEE_STATUS.VISIBILITY.includes(rowData.proposalStatus) &&
            renderIconButton("visibility", "secondary", () => {
              setItem(rowData);
              setIsView(false);
              setShowProposalDialog(true);
            })}
          {EMPLOYEE_STATUS.NOTIFICATION.includes(rowData.proposalStatus) &&
            renderIconButton("notifications_active", "error", () => {
              setProposal(rowData);
              setShouldNotificationConfirmationDialog(true);
            })}
        </>
      ),
    },
    {
      title: "Ngày đề xuất",
      field: "proposalDate",
      align: "center",
      minWidth: "120px",
      render: (rowData) => {
        return <span>{formatDate(rowData.proposalDate)}</span>;
      },
    },
    {
      title: "Loại",
      field: "type",
      align: "center",
      minWidth: "120px",
      render: (rowData) => {
        return (
          <span className="ww">
            {TYPE.find((type) => type.CODE === rowData.type)?.NAME}
          </span>
        );
      },
    },
    {
      title: "Nội dung",
      field: "content",
      align: "center",
      minWidth: "200px",
      render: (rowData) => {
        return <span className="ww">{rowData.content}</span>;
      },
    },
    {
      title: "Trạng thái",
      field: "proposalStatus",
      align: "center",
      minWidth: "120px",
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => {
        return (
          <span>
            {
              SUBMIT_PROFILE_STATUS.find(
                (proposalStatus) =>
                  proposalStatus.CODE.toString() ===
                  rowData.proposalStatus.toString()
              )?.NAME
            }
          </span>
        );
      },
    },
  ];

  return (
    <>
      <ValidatorForm onSubmit={() => handleSubmit()}>
        {showConfirmDialog && (
          <ConfirmationDialog
            title="Xóa đề xuất"
            open={showConfirmDialog}
            onConfirmDialogClose={() => {
              setProposalId();
              setShowConfirmDialog(false);
            }}
            onYesClick={() => {
              dispatch(deleteProposal(proposalId));
              setShowConfirmDialog(false);
              setProposal();
            }}
            Yes={"Có"}
            No={"Không"}
          />
        )}
        {showProposalDialog && (
          <ProposalDialog
            open={showProposalDialog}
            proposal={item}
            setProposal={setItem}
            isView={isView}
            setSend={setSend}
            isManage={false}
            employeeInfo={employeeInfo}
            setShowProposalDialog={setShowProposalDialog}
          />
        )}
        {shouldNotificationConfirmationDialog && (
          <ConfirmationDialog
            title={proposal?.proposalStatus === 4 ? "Bổ sung" : "Từ chối"}
            open={shouldNotificationConfirmationDialog}
            onConfirmDialogClose={() => {
              setProposal();
              setShouldNotificationConfirmationDialog(false);
            }}
            text={
              proposal?.proposalStatus === 4
                ? proposal.additionalRequest
                : proposal.reasonForRefusal
            }
            No="Hủy"
          />
        )}

        {isManage && (
          <Grid container spacing={2} className="mb-10">
            <Grid item md={3} xs={12}>
              <TextValidator
                className="w-100"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Ngày đề xuất
                  </span>
                }
                onChange={(event) => handleDateChange(event)}
                type="date"
                name="proposalDate"
                value={
                  proposal?.proposalDate
                    ? format(new Date(proposal?.proposalDate), "yyyy-MM-dd")
                    : ""
                }
                validators={["required", "isBeforeCurrentDate"]}
                errorMessages={[
                  "Không được bỏ trống",
                  "Phải từ ngày hiện tại trở đi.",
                ]}
                variant="outlined"
                size="small"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Grid>

            <Grid item xs={12} md={3}>
              <SelectValidator
                className="w-100"
                variant="outlined"
                size="small"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Loại đề xuất
                  </span>
                }
                onChange={(e) => handleChangeValue(e)}
                name="type"
                value={proposal?.type || ""}
                validators={["required"]}
                errorMessages={["Không được bỏ trống"]}
              >
                {TYPE && TYPE.length > 0 ? (
                  TYPE.map((type) => (
                    <MenuItem key={type.CODE} value={type.CODE}>
                      {type.NAME}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem disabled>Không có</MenuItem>
                )}
              </SelectValidator>
            </Grid>

            <Grid item xs={12} md={6}>
              <TextValidator
                className="w-100"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Nội dung
                  </span>
                }
                onChange={(e) => handleChangeValue(e)}
                type="text"
                name="content"
                value={proposal?.content || ""}
                validators={["required", "maxStringLength: 225"]}
                errorMessages={["Không được bỏ trống!", "Nội dung quá dài"]}
                variant="outlined"
                size="small"
              />
            </Grid>
            <Grid item xs={12} md={4}>
              <TextValidator
                className="w-100"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Ghi chú
                  </span>
                }
                onChange={(event) => handleChangeValue(event)}
                type="text"
                name="note"
                value={proposal?.note || ""}
                validators={["required", "maxStringLength:255"]}
                errorMessages={["Không được bỏ trống!", "Lý do quá dài"]}
                variant="outlined"
                size="small"
              />
            </Grid>
            <Grid item md={2} xs={12}>
              <SelectValidator
                className="w-100"
                variant="outlined"
                size="small"
                label="Tên lãnh đạo"
                onChange={(e) => handleChangeValue(e)}
                name="leaderId"
                value={proposal?.leaderId || ""}
              >
                {leaderList && leaderList.length > 0 ? (
                  leaderList.map((item) => (
                    <MenuItem key={item.id} value={item.id}>
                      {item.leaderName}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem disabled>Không có</MenuItem>
                )}
              </SelectValidator>
            </Grid>

            <Grid item md={6} xs={12}>
              <div>
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  onClick={() => {
                    setItem({ ...proposal });
                    setIsView(true);
                  }}
                  disabled={isDisabled}
                >
                  {!send
                    ? "Trình lãnh đạo"
                    : proposal?.id
                    ? "Cập nhật"
                    : "Thêm mới"}
                </Button>

                <Button
                  className="ml-10"
                  variant="contained"
                  color="secondary"
                  onClick={() => {
                    setSend(true);
                    setProposal();
                    setIsDisabled(checkStatus(proposalList));
                  }}
                >
                  Hủy
                </Button>
              </div>
            </Grid>
          </Grid>
        )}
      </ValidatorForm>

      <TableDialog data={proposalList} columns={columns} />
    </>
  );
};

export default Proposal;
