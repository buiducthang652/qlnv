import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Grid, TextField } from "@material-ui/core";
import TablePage from "../component/Table/TablePage";
import { searchEmployees } from "../../redux/actions/EmployeeActions";
import RegistrationDialog from "../registration/RegistrationDialog";
import { Breadcrumb } from "egret";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  GENDER,
  TEAMS,
  SUBMIT_PROFILE_STATUS,
  EMPLOYEE_STATUS,
} from "../component/constants";
import { formatDate } from "../utilities/formatDate";
import "../../../styles/views/_emp.scss";
import PendingEndDialog from "./pendingEndDialog";
import { renderIconButton } from "../utilities/renderButton";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const EndEmployee = () => {
  const { employeeList, totalElements } = useSelector((state) => ({
    employeeList: state.employee.employeeList,
    totalElements: state.employee.totalElements,
  }));

  const dispatch = useDispatch();

  const [pageSize, setPageSize] = useState(10);
  const [page, setPage] = useState(0);
  const [keyword, setKeyword] = useState("");
  const [shouldOpenRegistration, setShouldOpenRegistration] = useState(false);
  const [pendingEndDialog, setPendingEndDialog] = useState(false);
  const [item, setItem] = useState();

  const columns = [
    {
      title: "STT",
      align: "center",
      width: "5%",
      minWidth: "40px",
      render: (rowData) => {
        return <>{rowData.tableData.id + 1 + page * pageSize}</>;
      },
    },
    {
      title: "Thao tác",
      field: "custom",
      align: "center",
      minWidth: "95px",
      render: (rowData) => (
        <div>
          {EMPLOYEE_STATUS.END_EMPLOYEE.includes(rowData.submitProfileStatus) &&
            renderIconButton("visibility", "secondary", () => {
              setItem(rowData);
              setShouldOpenRegistration(true);
            })}
          {EMPLOYEE_STATUS.SEND.includes(rowData.submitProfileStatus) &&
            renderIconButton("send", "error", () => {
              setItem(rowData);
              setPendingEndDialog(true);
            })}
        </div>
      ),
    },
    {
      title: "Họ và tên",
      field: "name",
      align: "center",
      minWidth: "140px",
      cellStyle: {
        textAlign: "left",
      },
    },
    {
      title: "Giới tính",
      field: "gender",
      align: "center",
      minWidth: "75px",
      render: (rowData) => {
        return (
          <span>
            {GENDER.find((gender) => gender.CODE === rowData.gender)?.NAME}
          </span>
        );
      },
    },
    {
      title: "Nhóm",
      field: "team",
      align: "center",
      minWidth: "75px",
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => {
        return (
          <span>{TEAMS.find((team) => team.CODE === rowData.team)?.NAME}</span>
        );
      },
    },
    {
      title: "Email",
      field: "email",
      align: "center",
      minWidth: "120px",
      cellStyle: {
        textAlign: "left",
      },
    },
    {
      title: "Số điện thoại",
      field: "phone",
      align: "center",
      minWidth: "120px",
    },
    {
      title: "Ngày sinh",
      field: "dateOfBirth",
      align: "center",
      minWidth: "120px",
      render: (rowData) => {
        return <span>{formatDate(rowData.dateOfBirth)}</span>;
      },
    },
    {
      title: "Trạng thái",
      field: "submitProfileStatus",
      align: "center",
      minWidth: "120px",
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => {
        return (
          <span>
            {
              SUBMIT_PROFILE_STATUS.find(
                (submitProfileStatus) =>
                  submitProfileStatus.CODE.toString() ===
                  rowData.submitProfileStatus.toString()
              )?.NAME
            }
          </span>
        );
      },
    },
  ];

  useEffect(() => {
    const searchObject = {
      keyword,
      pageIndex: page + 1,
      pageSize,
      listStatus: EMPLOYEE_STATUS.END_EMPLOYEE,
    };

    dispatch(searchEmployees(searchObject));
  }, [pageSize, page, keyword]);

  const handleShowRegistration = () => {
    setShouldOpenRegistration(!shouldOpenRegistration);
  };

  return (
    <div className="m-sm-30 employee">
      <Grid xs={12} container justifyContent="space-between">
        <Grid item sx={6}>
          <Breadcrumb
            routeSegments={[
              {
                name: "Quản lý",
                path: "/dashboard/analytics",
              },
              { name: "Kết thúc nhân viên" },
            ]}
          />
        </Grid>
        <Grid item sx={12} className="pending_search">
          <TextField
            variant="standard"
            placeholder="Tìm kiếm"
            onChange={(event) => {
              setPage(0);
              setKeyword(event.target.value);
            }}
          />
        </Grid>
      </Grid>

      <Grid container className="">
        <Grid item xs={12}>
          <div>
            {shouldOpenRegistration && (
              <RegistrationDialog
                open={shouldOpenRegistration}
                handleShowRegistration={handleShowRegistration}
                employeeInfo={item}
                setEmployeeInfo={setItem}
                pending={false}
              />
            )}

            {pendingEndDialog && (
              <PendingEndDialog
                open={pendingEndDialog}
                employeeInfo={item}
                setEmployeeInfo={setItem}
                setPendingEndDialog={setPendingEndDialog}
              />
            )}
          </div>
        </Grid>

        <TablePage
          data={employeeList}
          columns={columns}
          totalElements={totalElements}
          page={page}
          pageSize={pageSize}
          setPage={setPage}
          setPageSize={setPageSize}
        />
      </Grid>
    </div>
  );
};

export default EndEmployee;
