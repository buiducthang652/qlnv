import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from "react-i18next";

const EndEmployee = EgretLoadable({
  loader: () => import("./EndEmployee"),
});

const EndEmployeeManage = EgretLoadable({
  loader: () => import("./EndEmployeeManage"),
});

const ViewComponent1 = withTranslation()(EndEmployee);

const ViewComponent2 = withTranslation()(EndEmployeeManage);

const EndEmployeeRoutes = [
  {
    path: ConstantList.ROOT_PATH + "employee_manager/endEmployee",
    exact: true,
    component: ViewComponent1,
  },
  {
    path: ConstantList.ROOT_PATH + "employee_manager/endEmployeeManage",
    exact: true,
    component: ViewComponent2,
  },
];

export default EndEmployeeRoutes;
