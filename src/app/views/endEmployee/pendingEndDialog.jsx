import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import {
  Dialog,
  Button,
  DialogActions,
  DialogTitle,
  DialogContent,
  Grid,
} from "@material-ui/core";
import { parse, format } from "date-fns";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "../../../styles/views/_style.scss";
import "../../../styles/views/_pending.scss";
import { updateEmployees } from "app/redux/actions/EmployeeActions";

toast.configure({
  autoClose: 1000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const EndCodeRegex = `NL${String(new Date().getMonth() + 1).padStart(2, "0")}${
  new Date().getFullYear() % 100
}/\\d{3}`;
const EndCodeRegexLog = `NL${String(new Date().getMonth() + 1).padStart(
  2,
  "0"
)}${new Date().getFullYear() % 100}/354`;

const PendingEndDialog = ({
  open,
  employeeInfo,
  setEmployeeInfo,
  setPendingEndDialog,
}) => {
  const dispatch = useDispatch();

  const handleFormSubmit = (e) => {
    dispatch(updateEmployees(employeeInfo));
    setPendingEndDialog(false);
  };

  const handleDateChange = (event) => {
    const timestamp = parse(
      event.target.value,
      "yyyy-MM-dd",
      new Date()
    ).getTime();
    setEmployeeInfo({
      ...employeeInfo,
      [event.target.name]: timestamp,
    });
  };

  const handleInputChange = (event) => {
    setEmployeeInfo({
      ...employeeInfo,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth={"sm"}
      fullWidth={true}
    >
      <ValidatorForm
        onSubmit={(e) => {
          handleFormSubmit(e);
        }}
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          Nộp lưu hồ sơ
        </DialogTitle>

        <DialogContent className="pd-10">
          <Grid container lg={12} md={12} sm={12} xs={12} spacing={1}>
            <Grid item lg={4} md={4} sm={4} xs={12}>
              <TextValidator
                className="w-100"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Ngày nộp lưu
                  </span>
                }
                onChange={(event) => handleDateChange(event)}
                type="date"
                name="decisionDay"
                value={
                  employeeInfo?.decisionDay
                    ? format(new Date(employeeInfo?.decisionDay), "yyyy-MM-dd")
                    : ""
                }
                validators={["required"]}
                errorMessages={["Vui lòng chọn ngày sinh."]}
                variant="outlined"
                size="small"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Grid>
            <Grid item lg={8} md={8} sm={8} xs={12}>
              <TextValidator
                className="w-100"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Mã nộp lưu
                  </span>
                }
                onChange={(event) => handleInputChange(event)}
                type="text"
                name="numberSaved"
                value={employeeInfo ? employeeInfo.numberSaved : ""}
                validators={["required", `matchRegexp:${EndCodeRegex}`]}
                errorMessages={[
                  "Vui lòng điền dữ liệu.",
                  `Mã nộp lưu có dạng NLmmyyxxx (ví dụ: ${EndCodeRegexLog}).`,
                ]}
                variant="outlined"
                size="small"
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions className="justifyCenter">
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              setEmployeeInfo({ ...employeeInfo, submitProfileStatus: "0" });
            }}
            type="submit"
          >
            Lưu
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => {
              setPendingEndDialog(false);
            }}
          >
            Hủy
          </Button>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
};

export default PendingEndDialog;
