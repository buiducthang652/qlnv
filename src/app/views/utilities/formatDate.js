import Moment from "moment";

export const formatDate = (date) => {
  return Moment(date).format("DD/MM/YYYY");
};

export const formatMY = (date) => {
  return Moment(date).format("MM/YYYY");
};

export const formatNTN = (date) => {
  return Moment(date).format("[ngày] DD [tháng] MM [năm] YYYY");
};
