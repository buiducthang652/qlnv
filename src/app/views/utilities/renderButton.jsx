import React from "react";
import { IconButton, Icon, Button } from "@material-ui/core";

export const renderIconButton = (icon, color, onClick) => (
  <IconButton size="small" onClick={onClick}>
    <Icon fontSize="small" color={color}>
      {icon}
    </Icon>
  </IconButton>
);
