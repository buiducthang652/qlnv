import { ValidatorForm } from "react-material-ui-form-validator";
import { parse, format, differenceInYears } from "date-fns";
import { isAfter } from "date-fns";

const addValidationRules = () => {
  ValidatorForm.addValidationRule("isNotFutureDate", (value) => {
    if (!value) {
      return true;
    }

    const currentDate = new Date().setHours(23, 59, 59, 59);
    const selectedDate = new Date(value);
    return isAfter(currentDate, selectedDate);
  });

  ValidatorForm.addValidationRule("isBeforeCurrentDate", (value) => {
    if (!value) {
      return true;
    }
    const currentDate = new Date().setHours(0, 0, 0, 0);
    const selectedDate = new Date(value);
    return isAfter(selectedDate, currentDate);
  });

  ValidatorForm.addValidationRule("is18OrOlder", (value) => {
    if (value) {
      const birthDate = new Date(value);
      const currentDate = new Date();
      return differenceInYears(currentDate, birthDate) >= 18;
    }
    return false;
  });
};

const removeValidationRules = () => {
  ValidatorForm.removeValidationRule("isNotFutureDate");
  ValidatorForm.removeValidationRule("is18OrOlder");
};

export { addValidationRules, removeValidationRules };
