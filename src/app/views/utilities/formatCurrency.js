export const formatVND = (value) => {
  if (typeof value !== "number" || isNaN(value)) {
    return "";
  }

  return value.toLocaleString("it-IT", {
    style: "currency",
    currency: "VND",
  });
};
