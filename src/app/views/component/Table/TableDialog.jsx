import React from "react";
import MaterialTable from "material-table";

const TableDialog = ({ data, columns }) => {
  return (
    <>
      <MaterialTable
        data={data}
        columns={columns}
        options={{
          selection: false,
          actionsColumnIndex: -1,
          paging: false,
          search: false,
          rowStyle: (rowData, index) => ({
            backgroundColor: index % 2 === 1 ? "#EEE" : "#FFF",
          }),
          maxBodyHeight: "300px",
          headerStyle: {
            backgroundColor: "#7467ef",
            color: "#fff",
          },
          padding: "dense",
          toolbar: false,
          sorting: false,
          draggable: false,
        }}
        localization={{
          body: {
            emptyDataSourceMessage: "Không có bản ghi nào.",
          },
        }}
      />
    </>
  );
};

export default TableDialog;
