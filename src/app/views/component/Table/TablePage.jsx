import React from "react";
import MaterialTable, { MTableToolbar } from "material-table";
import { TablePagination } from "@material-ui/core";

const TablePage = ({
  data,
  columns,
  totalElements,
  pageSize,
  page,
  setPage,
  setPageSize,
}) => {
  return (
    <>
      <MaterialTable
        data={data}
        columns={columns}
        options={{
          selection: false,
          actionsColumnIndex: -1,
          paging: false,
          search: false,
          rowStyle: (rowData, index) => ({
            backgroundColor: index % 2 === 1 ? "#EEE" : "#FFF",
          }),
          maxBodyHeight: "530px",
          minBodyHeight: "365px",
          headerStyle: {
            backgroundColor: "#7467ef",
            color: "#fff",
          },
          padding: "dense",
          toolbar: false,
          sorting: false,
          draggable: false,
        }}
        localization={{
          body: {
            emptyDataSourceMessage: "Không có bản ghi nào.",
          },
        }}
      />

      <TablePagination
        align="left"
        className="px-16 w-100"
        rowsPerPageOptions={[1, 2, 3, 5, 10, 25, 50, 100]}
        component="div"
        labelRowsPerPage="Số hàng mỗi trang:"
        labelDisplayedRows={({ from, to, count }) =>
          `${from}-${to} hoặc ${count !== -1 ? count : `more than ${to}`}`
        }
        count={totalElements}
        rowsPerPage={pageSize}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={(event, newPage) => {
          setPage(newPage);
        }}
        onChangeRowsPerPage={(event) => {
          setPageSize(event.target.value);
          setPage(0);
        }}
      />
    </>
  );
};

export default TablePage;
