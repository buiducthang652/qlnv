import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Grid,
  Button,
  DialogActions,
  DialogTitle,
  DialogContent,
  Dialog,
} from "@material-ui/core";
import { toast } from "react-toastify";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import EndDialog from "../StatusDialog/EndDialog";
import ApproveDialog from "../StatusDialog/ApproveDialog";
import AdditionalRequestDialog from "../StatusDialog/AdditionalRequestDialog";
import RefuseDialog from "../StatusDialog/RefuseDialog";
import "react-toastify/dist/ReactToastify.css";
import "../../../../styles/views/_development.scss";
import "../../../../styles/views/_formEmployee.scss";
import { getAllLeader } from "app/redux/actions/LeaderActions";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import moment from "moment";
import { POSITION } from "../constants";
import { parse, format } from "date-fns";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const EndEmployeeDialog = ({
  open,
  employeeInfo,
  setEmployeeInfo,
  setShowEndEmployeeDialog,
  isView,
  isManage,
  handleShouldEndEmployee,
  handleShowRegistration,
  setShowApprovedDialog,
}) => {
  const [endDialog, setEndDialog] = useState(false);
  const [showApproveDialog, setShowApproveDialog] = useState(false);
  const [showRefuseDialog, setShowRefuseDialog] = useState(false);
  const [showAdditionalRequestDialog, setShowAdditionalRequestDialog] =
    useState(false);
  const dispatch = useDispatch();

  const { leaderList } = useSelector((state) => ({
    leaderList: state.leader.leaderList,
  }));
  useEffect(() => {
    dispatch(getAllLeader());

    if (!employeeInfo?.endDay) {
      setEmployeeInfo({
        ...employeeInfo,
        endDay: format(new Date(), "yyyy-MM-dd"),
      });
    }
  }, []);

  const handleFormSubmit = (e) => {};

  const handleInputChange = (event) => {
    setEmployeeInfo({
      ...employeeInfo,
      [event.target.name]: event.target.value,
    });
  };

  const handleDateChange = (event) => {
    const timestamp = parse(
      event.target.value,
      "yyyy-MM-dd",
      new Date()
    ).getTime();
    setEmployeeInfo({
      ...employeeInfo,
      [event.target.name]: timestamp,
    });
  };

  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth={"md"}
      fullWidth={true}
    >
      {endDialog && (
        <EndDialog
          open={endDialog}
          setEndDialog={setEndDialog}
          employeeInfo={employeeInfo}
          setEmployeeInfo={setEmployeeInfo}
          handleShouldEndEmployee={handleShouldEndEmployee}
          setShowApprovedDialog={setShowApprovedDialog}
          setShowEndEmployeeDialog={setShowEndEmployeeDialog}
          isPending={true}
        />
      )}

      {showApproveDialog && (
        <ApproveDialog
          open={showApproveDialog}
          setShowApproveDialog={setShowApproveDialog}
          setShowEndEmployeeDialog={setShowEndEmployeeDialog}
          item={employeeInfo}
          setItem={setEmployeeInfo}
          isPending={true}
        />
      )}

      {showAdditionalRequestDialog && (
        <AdditionalRequestDialog
          open={showAdditionalRequestDialog}
          setShowAdditionalRequestDialog={setShowAdditionalRequestDialog}
          setShowEndEmployeeDialog={setShowEndEmployeeDialog}
          item={employeeInfo}
          setItem={setEmployeeInfo}
          isPending={true}
        />
      )}

      {showRefuseDialog && (
        <RefuseDialog
          open={showRefuseDialog}
          setShowRefuseDialog={setShowRefuseDialog}
          setShowEndEmployeeDialog={setShowEndEmployeeDialog}
          item={employeeInfo}
          setItem={setEmployeeInfo}
          isPending={true}
        />
      )}

      <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
        Đơn xin nghỉ việc
      </DialogTitle>

      <DialogContent>
        <ValidatorForm
          onSubmit={(e) => {
            handleFormSubmit(e);
          }}
        >
          <Grid className="employeeForm">
            <Grid container spacing={1} className="subEmployeeForm">
              <Grid container className="titleEmployeeForm">
                <Grid item lg={12} md={12} sm={12} xs={12}>
                  <h3 className="fontB">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</h3>
                </Grid>
                <Grid item lg={12} md={12} sm={12} xs={12}>
                  <h4 className="fontB">Độc lập - Tự do - Hạnh phúc</h4>
                </Grid>
                <Grid item lg={12} md={12} sm={12} xs={12}>
                  <h4>----------------------------------------</h4>
                </Grid>
                <Grid item lg={12} md={12} sm={12} xs={12}>
                  <h3 className="fontB">ĐƠN XIN NGHỈ VIỆC</h3>
                </Grid>
              </Grid>

              <Grid container spacing={4} className="">
                <Grid
                  item
                  lg={12}
                  md={12}
                  sm={12}
                  xs={12}
                  className="widthEmployeeForm"
                >
                  <span className="widthEmployeeForm_name">Kính gửi: </span>{" "}
                  <span className="border_bot">Ban giám đốc công ty </span>
                </Grid>
                <Grid
                  item
                  lg={12}
                  md={12}
                  sm={12}
                  xs={12}
                  className="widthEmployeeForm"
                >
                  <span className="widthEmployeeForm_name">Tôi tên là: </span>{" "}
                  <span className="border_bot">{employeeInfo?.name}</span>
                </Grid>
                <Grid
                  item
                  lg={12}
                  md={12}
                  sm={12}
                  xs={12}
                  className="widthEmployeeForm"
                >
                  <span className="widthEmployeeForm_position">
                    Hiện tại đang giữ chức vụ:
                  </span>{" "}
                  <span className="border_bot">
                    {POSITION.find(
                      (position) =>
                        position.CODE === employeeInfo?.currentPosition
                    )?.NAME || ""}
                  </span>
                </Grid>
                <Grid
                  item
                  lg={12}
                  md={12}
                  sm={12}
                  xs={12}
                  className="widthEmployeeForm"
                >
                  <span className="">
                    Tôi viết đơn này, đề nghị Ban Giám đốc cho tôi xin nghỉ việc
                    từ ngày{" "}
                    {
                      moment(new Date(employeeInfo?.endDay))
                        .format("DD/MM/YYYY")
                        .split("/")[0]
                    }{" "}
                    tháng{" "}
                    {
                      moment(new Date(employeeInfo?.endDay))
                        .format("DD/MM/YYYY")
                        .split("/")[1]
                    }{" "}
                    năm{" "}
                    {
                      moment(new Date(employeeInfo?.endDay))
                        .format("DD/MM/YYYY")
                        .split("/")[2]
                    }{" "}
                    {isView && (
                      <span className="inlineBlock">
                        <TextValidator
                          className="w-15"
                          onChange={(e) => handleDateChange(e)}
                          type="date"
                          name="endDay"
                          value={moment(employeeInfo?.endDay).format(
                            "YYYY-MM-DD"
                          )}
                          validators={["required"]}
                          errorMessages={["Không được để trống"]}
                          InputLabelProps={{
                            shrink: true,
                          }}
                          size="small"
                        />
                      </span>
                    )}
                    vì lý do:{" "}
                  </span>
                </Grid>
                <Grid
                  item
                  lg={12}
                  md={12}
                  sm={12}
                  xs={12}
                  className="widthEmployeeForm fl-colum"
                >
                  {isView ? (
                    <span className="border_bot bot_13">
                      <TextValidator
                        multiline
                        className="customUnderline"
                        onChange={(event) => handleInputChange(event)}
                        type="text"
                        name="reasonForEnding"
                        value={employeeInfo?.reasonForEnding || ""}
                        validators={["required"]}
                        errorMessages={["Vui lòng điền dữ liệu."]}
                        size="small"
                      />
                    </span>
                  ) : employeeInfo?.reasonForEnding ? (
                    employeeInfo?.reasonForEnding
                      .split("\n")
                      .filter(
                        (reasonForEnding) => reasonForEnding.trim() !== ""
                      )
                      .map((reasonForEnding, index) => (
                        <span className="border_bot break_line">
                          {reasonForEnding}
                        </span>
                      ))
                  ) : (
                    "Không có"
                  )}
                </Grid>
                <Grid
                  item
                  lg={12}
                  md={12}
                  sm={12}
                  xs={12}
                  className="widthEmployeeForm"
                >
                  <span className="">
                    Trong thời gian chờ đợi sự chấp thuận của Ban Giám đốc Công
                    ty, tôi sẽ tiếp tục làm việc nghiêm túc và tiến hành bàn
                    giao công việc cũng như tài sản cho người quản lý trực tiếp
                    của tôi là{" "}
                    {
                      leaderList.find(
                        (item) => item.id === employeeInfo?.leaderId
                      )?.leaderName
                    }
                  </span>
                </Grid>
                <Grid
                  item
                  lg={12}
                  md={12}
                  sm={12}
                  xs={12}
                  className="widthEmployeeForm"
                >
                  <span className="">Tôi xin chân thành cảm ơn!</span>
                </Grid>
              </Grid>
              <Grid item lg={12} md={12} sm={12} xs={12} className="sigLeft">
                <Grid
                  container
                  justifyContent="flex-end"
                  className="sigEmployeeForm"
                >
                  <Grid item>
                    Hà Nội, ngày{" "}
                    {new Date().getDate().toString().padStart(2, "0")} tháng{" "}
                    {(new Date().getMonth() + 1).toString().padStart(2, "0")}{" "}
                    năm {new Date().getFullYear()}
                  </Grid>
                  <Grid
                    item
                    container
                    direction="column"
                    alignItems="center"
                    className="pb-50"
                  >
                    <Grid item className="font-bo">
                      NGƯỜI LÀM ĐƠN
                    </Grid>
                    <Grid item className="sign_name">
                      (Ký, ghi rõ họ tên)
                    </Grid>
                  </Grid>
                  <Grid item className="font-bo">
                    {employeeInfo.name}
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </ValidatorForm>
      </DialogContent>

      <DialogActions className="justifyCenter">
        {isView && (
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              setEndDialog(true);
            }}
          >
            Trình lãnh đạo
          </Button>
        )}

        {isManage && (
          <>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                handleShowRegistration(employeeInfo?.id, false);
              }}
            >
              Xem hồ sơ
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                setShowApproveDialog(true);
              }}
            >
              Phê duyệt
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                setShowAdditionalRequestDialog(true);
              }}
            >
              Yêu cầu bổ sung
            </Button>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => {
                setShowRefuseDialog(true);
              }}
            >
              Từ chối
            </Button>
          </>
        )}

        <Button
          variant="contained"
          color="secondary"
          onClick={() => {
            if (handleShouldEndEmployee) {
              handleShouldEndEmployee(true, false);
            } else {
              setShowEndEmployeeDialog(false);
            }
          }}
        >
          Hủy
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default EndEmployeeDialog;
