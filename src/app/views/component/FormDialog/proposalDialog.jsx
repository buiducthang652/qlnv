import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
  Grid,
  Button,
  DialogActions,
  DialogTitle,
  DialogContent,
  Dialog,
} from "@material-ui/core";
import { toast } from "react-toastify";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import "react-toastify/dist/ReactToastify.css";
import "../../../../styles/views/_development.scss";
import AdditionalRequestDialog from "../StatusDialog/AdditionalRequestDialog";
import ApproveDialog from "../StatusDialog/ApproveDialog";
import RefuseDialog from "../StatusDialog/RefuseDialog";
import "../../../../styles/views/_formEmployee.scss";
import {
  saveProposal,
  updateProposal,
} from "../../../redux/actions/ProposalAction";
import { POSITION } from "../constants";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const ProposalDialog = ({
  open,
  proposal,
  setProposal,
  employeeInfo,
  isView,
  isManage,
  setShowProposalDialog,
  handleShowRegistration,
}) => {
  const dispatch = useDispatch();
  const [showApproveDialog, setShowApproveDialog] = useState(false);
  const [showAdditionalRequestDialog, setShowAdditionalRequestDialog] =
    useState(false);
  const [showRefuseDialog, setShowRefuseDialog] = useState(false);

  useEffect(() => {
    setProposal({
      ...proposal,
      proposalStatus: 2,
    });
  }, []);

  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth={"md"}
      fullWidth={true}
    >
      {showApproveDialog && (
        <ApproveDialog
          open={showApproveDialog}
          setShowApproveDialog={setShowApproveDialog}
          setShowProposalDialog={setShowProposalDialog}
          item={proposal}
          setItem={setProposal}
          isPending={false}
        />
      )}

      {showAdditionalRequestDialog && (
        <AdditionalRequestDialog
          open={showAdditionalRequestDialog}
          setShowAdditionalRequestDialog={setShowAdditionalRequestDialog}
          setShowProposalDialog={setShowProposalDialog}
          item={proposal}
          setItem={setProposal}
          isPending={false}
        />
      )}

      {showRefuseDialog && (
        <RefuseDialog
          open={showRefuseDialog}
          setShowRefuseDialog={setShowRefuseDialog}
          setShowProposalDialog={setShowProposalDialog}
          item={proposal}
          setItem={setProposal}
          isPending={false}
        />
      )}

      <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
        Đơn đề xuất
      </DialogTitle>

      <DialogContent>
        <Grid className="employeeForm">
          <Grid container spacing={1} className="subEmployeeForm">
            <Grid container className="titleEmployeeForm">
              <Grid item lg={12} md={12} sm={12} xs={12}>
                <h3 className="fontB">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</h3>
              </Grid>
              <Grid item lg={12} md={12} sm={12} xs={12}>
                <h4 className="fontB">Độc lập - Tự do - Hạnh phúc</h4>
              </Grid>
              <Grid item lg={12} md={12} sm={12} xs={12}>
                <h4>----------------------------------------</h4>
              </Grid>
              <Grid item lg={12} md={12} sm={12} xs={12}>
                <h3 className="fontB">ĐƠN ĐỀ XUẤT</h3>
              </Grid>
            </Grid>

            <Grid container spacing={4} className="">
              <Grid
                item
                lg={12}
                md={12}
                sm={12}
                xs={12}
                className="widthEmployeeForm"
              >
                <span className="widthEmployeeForm_name">Kính gửi: </span>{" "}
                <span className="border_bot">Ban giám đốc công ty </span>
              </Grid>
              <Grid
                item
                lg={12}
                md={12}
                sm={12}
                xs={12}
                className="widthEmployeeForm"
              >
                <span className="widthEmployeeForm_name">Tên tôi là: </span>{" "}
                <span className="border_bot">{employeeInfo?.name}</span>
              </Grid>
              <Grid
                item
                lg={12}
                md={12}
                sm={12}
                xs={12}
                className="widthEmployeeForm"
              >
                <span className="widthEmployeeForm_position">
                  Hiện tại đang giữ chức vụ:
                </span>{" "}
                <span className="border_bot">
                  {
                    POSITION.find(
                      (position) =>
                        position.CODE === employeeInfo?.currentPosition
                    )?.NAME
                  }
                </span>
              </Grid>
              <Grid
                item
                lg={12}
                md={12}
                sm={12}
                xs={12}
                className="widthEmployeeForm"
              >
                <span className="widthEmployeeForm_content">
                  Tôi viết đơn này để đề xuất:{" "}
                </span>{" "}
                <span className="border_bot">{proposal?.content}</span>
              </Grid>
              <Grid
                item
                lg={12}
                md={12}
                sm={12}
                xs={12}
                className="widthEmployeeForm"
              >
                <span className="widthEmployeeForm_note">Lý do: </span>{" "}
                <span className="border_bot">{proposal?.note}</span>
              </Grid>
            </Grid>
            <Grid item lg={12} md={12} sm={12} xs={12} className="sigLeft">
              <Grid
                container
                justifyContent="flex-end"
                className="sigEmployeeForm"
              >
                <Grid item>
                  Hà Nội, ngày{" "}
                  {new Date().getDate().toString().padStart(2, "0")} tháng{" "}
                  {(new Date().getMonth() + 1).toString().padStart(2, "0")} năm{" "}
                  {new Date().getFullYear()}
                </Grid>
                <Grid
                  item
                  container
                  direction="column"
                  alignItems="center"
                  className="pb-50"
                >
                  <Grid item className="font-bo">
                    NGƯỜI LÀM ĐƠN
                  </Grid>
                  <Grid item className="sign_name">(Ký, ghi rõ họ tên)</Grid>
                </Grid>
                <Grid item className="font-bo">
                  {employeeInfo.name}
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </DialogContent>

      <DialogActions className="justifyCenter">
        {isManage && (
          <>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                handleShowRegistration(employeeInfo?.id, false);
              }}
            >
              Xem hồ sơ
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                setShowApproveDialog(true);
              }}
            >
              Phê duyệt
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                setShowAdditionalRequestDialog(true);
              }}
            >
              Yêu cầu bổ sung
            </Button>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => {
                setShowRefuseDialog(true);
              }}
            >
              Từ chối
            </Button>
          </>
        )}
        {isView && !isManage && (
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              if (proposal?.id) {
                dispatch(updateProposal(proposal));
              } else {
                dispatch(saveProposal(employeeInfo.id, proposal));
              }

              setShowProposalDialog(false);
              setProposal();
            }}
          >
            Trình lãnh đạo
          </Button>
        )}
        <Button
          variant="contained"
          color="secondary"
          onClick={() => {
            setProposal();
            setShowProposalDialog(false);
          }}
        >
          Hủy
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ProposalDialog;
