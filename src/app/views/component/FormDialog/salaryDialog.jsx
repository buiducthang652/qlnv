import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
  Grid,
  Button,
  DialogActions,
  DialogTitle,
  DialogContent,
  Dialog,
} from "@material-ui/core";
import { toast } from "react-toastify";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import "react-toastify/dist/ReactToastify.css";
import { formatNTN } from "../../utilities/formatDate";
import ApproveDialog from "../StatusDialog/ApproveDialog";
import AdditionalRequestDialog from "../StatusDialog/AdditionalRequestDialog";
import RefuseDialog from "../StatusDialog/RefuseDialog";
import { saveSalary, updateSalary } from "../../../redux/actions/SalaryAction";
import "../../../../styles/views/_development.scss";
import "../../../../styles/views/_formEmployee.scss";
import { formatVND } from "app/views/utilities/formatCurrency";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const SalaryDialog = ({
  open,
  salary,
  setSalary,
  employeeInfo,
  isView,
  isManage,
  setShowSalaryDialog,
  handleShowRegistration,
}) => {
  const dispatch = useDispatch();

  const [showApproveDialog, setShowApproveDialog] = useState(false);
  const [showAdditionalRequestDialog, setShowAdditionalRequestDialog] =
    useState(false);
  const [showRefuseDialog, setShowRefuseDialog] = useState(false);
  const [dataSalary, setDataSalary] = useState();

  useEffect(() => {
    setDataSalary({ ...salary, salaryIncreaseStatus: 2 });
  }, []);

  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth={"md"}
      fullWidth={true}
    >
      {showApproveDialog && (
        <ApproveDialog
          open={showApproveDialog}
          setShowApproveDialog={setShowApproveDialog}
          setShowSalaryDialog={setShowSalaryDialog}
          item={salary}
          setItem={setSalary}
          isPending={false}
        />
      )}

      {showAdditionalRequestDialog && (
        <AdditionalRequestDialog
          open={showAdditionalRequestDialog}
          setShowAdditionalRequestDialog={setShowAdditionalRequestDialog}
          setShowSalaryDialog={setShowSalaryDialog}
          item={salary}
          setItem={setSalary}
          isPending={false}
        />
      )}

      {showRefuseDialog && (
        <RefuseDialog
          open={showRefuseDialog}
          setShowRefuseDialog={setShowRefuseDialog}
          setShowSalaryDialog={setShowSalaryDialog}
          item={salary}
          setItem={setSalary}
          isPending={false}
        />
      )}

      <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
        Đơn tăng lương
      </DialogTitle>

      <DialogContent>
        <Grid className="employeeForm">
          <Grid container spacing={1} className="subEmployeeForm">
            <Grid container className="titleEmployeeForm">
              <Grid item lg={12} md={12} sm={12} xs={12}>
                <h3 className="fontB">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</h3>
              </Grid>
              <Grid item lg={12} md={12} sm={12} xs={12}>
                <h4 className="fontB">Độc lập - Tự do - Hạnh phúc</h4>
              </Grid>
              <Grid item lg={12} md={12} sm={12} xs={12}>
                <h4>----------------------------------------</h4>
              </Grid>
              <Grid item lg={12} md={12} sm={12} xs={12}>
                <h3 className="fontB">QUYẾT ĐỊNH</h3>
              </Grid>
              <Grid item lg={12} md={12} sm={12} xs={12}>
                <h5 className="fontB">Về việc tăng lương cho nhân viên</h5>
              </Grid>
            </Grid>
            <Grid container spacing={4} className="contentEmployeeForm">
              <Grid item lg={12} md={12} sm={12} xs={12}>
                - Căn cứ vào quy chế lương của công ty OCEANTECH
              </Grid>
              <Grid item lg={12} md={12} sm={12} xs={12}>
                - Căn cứ vào hợp đồng lao động Điều 3 Khoản 1 BLLĐ 2012
              </Grid>
              <Grid item lg={12} md={12} sm={12} xs={12}>
                - Căn cứ vào những đóng góp thực tế của Ông (Bà)
                <span className="bor_bot"> {employeeInfo.name} </span> đối với
                sự phát triển của Công ty.
              </Grid>
            </Grid>
            <Grid container className="titleEmployeeForm">
              <Grid item lg={12} md={12} sm={12} xs={12}>
                <h3 className="fontB">QUYẾT ĐỊNH</h3>
              </Grid>
            </Grid>
            <Grid container spacing={4} className="">
              <Grid item lg={12} md={12} sm={12} xs={12}>
                - Điều 1: Kể từ ngày {formatNTN(dataSalary?.startDate)}, mức
                lương chính thức của Ông (Bà) là:{" "}
                <span className="bor_bot">
                  {formatVND(dataSalary?.newSalary)}
                </span>
              </Grid>
              <Grid item lg={12} md={12} sm={12} xs={12}>
                - Điều 2: Các đơn vị và cá nhân có liên quan và ông/bà{" "}
                <span className="bor_bot">{employeeInfo?.name}</span> chịu
                trách nhiệm thi hành quyết định này.
              </Grid>
            </Grid>
            <Grid item lg={12} md={12} sm={12} xs={12} className="sigLeft">
              <Grid
                container
                justifyContent="flex-end"
                className="sigEmployeeForm"
              >
                <Grid item>
                  Hà Nội, ngày{" "}
                  {new Date().getDate().toString().padStart(2, "0")} tháng{" "}
                  {(new Date().getMonth() + 1).toString().padStart(2, "0")} năm{" "}
                  {new Date().getFullYear()}
                </Grid>
                <Grid
                  item
                  container
                  direction="column"
                  alignItems="center"
                  className="pb-50"
                >
                  <Grid item className="font-bo">
                    NGƯỜI LÀM ĐƠN
                  </Grid>
                  <Grid item className="sign_name">
                    (Ký, ghi rõ họ tên)
                  </Grid>
                </Grid>
                <Grid item className="font-bo">
                  {employeeInfo.name}
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </DialogContent>

      <DialogActions className="justifyCenter">
        {isView && !isManage && (
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              if (salary?.id) {
                dispatch(updateSalary(dataSalary));
              } else {
                dispatch(saveSalary(employeeInfo.id, dataSalary));
              }

              setShowSalaryDialog(false);
              setSalary();
            }}
          >
            Trình lãnh đạo
          </Button>
        )}
        {isManage && (
          <>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                handleShowRegistration(employeeInfo?.id, false);
              }}
            >
              Xem hồ sơ
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                setShowApproveDialog(true);
              }}
            >
              Phê duyệt
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                setShowAdditionalRequestDialog(true);
              }}
            >
              Yêu cầu bổ sung
            </Button>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => {
                setShowRefuseDialog(true);
              }}
            >
              Từ chối
            </Button>
          </>
        )}
        <Button
          variant="contained"
          color="secondary"
          onClick={() => {
            setSalary();
            setShowSalaryDialog(false);
          }}
        >
          Hủy
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default SalaryDialog;
