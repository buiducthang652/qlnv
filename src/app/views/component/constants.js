export const SUBMIT_PROFILE_STATUS = [
  {
    CODE: 1,
    NAME: "Tạo mới",
  },
  {
    CODE: 2,
    NAME: "Chờ duyệt",
  },
  {
    CODE: 3,
    NAME: "Đã duyệt",
  },
  {
    CODE: 4,
    NAME: "Yêu cầu bổ sung",
  },
  {
    CODE: 5,
    NAME: "Từ chối",
  },
  {
    CODE: 6,
    NAME: "Chờ duyệt kết thúc",
  },
  {
    CODE: 7,
    NAME: "Kết thúc hồ sơ",
  },
  {
    CODE: 8,
    NAME: "Bổ sung kết thúc hồ sơ",
  },
  {
    CODE: 9,
    NAME: "Từ chối kết thúc hồ sơ",
  },
  {
    CODE: 0,
    NAME: "Nộp lưu hồ sơ",
  },
];

export const GENDER = [
  { CODE: 1, NAME: "Nam" },
  { CODE: 2, NAME: "Nữ" },
  { CODE: 0, NAME: "Khác" },
];

export const EMPLOYEE_STATUS = {
  EMPLOYEE: "5,4,1,2",
  EMPLOYEE_PENDING: "2,6",
  APPROVED: "3,6,8,9",
  END_EMPLOYEE: "0,7",
  VISIBILITY_APPROVED: "6,8,9",
  EDIT_APPROVED: "3,8,9",
  NOTIFICATION_APPROVED: "8,9",
  VISIBILITY: "2,3",
  EDIT: "1,4,5",
  NOTIFICATION: "4,5",
  DELETE: "1",
  VISIBILITY_PENDING_END: "2",
  VISIBILITY_PENDING_REG: "6",
  NOTIFICATION_PENDING: "2",
  VISIBILITY_EMPLOYEE: "2",
  EDIT_EMPLOYEE: "1,4,5",
  NOTIFICATION_EMPLOYEE: "4,5",
  SEND: "7",
};

export const TEAMS = [
  { CODE: 0, NAME: "IOS" },
  { CODE: 1, NAME: "ReactJS" },
  { CODE: 2, NAME: "Java" },
  { CODE: 3, NAME: "C#" },
  { CODE: 4, NAME: "Android" },
  { CODE: 5, NAME: "PHP" },
];

export const RELATIONSHIPS = [
  { CODE: 0, NAME: "Ông" },
  { CODE: 1, NAME: "Bà" },
  { CODE: 2, NAME: "Bố" },
  { CODE: 3, NAME: "Mẹ" },
  { CODE: 4, NAME: "Vợ/Chồng" },
  { CODE: 5, NAME: "Anh trai" },
  { CODE: 6, NAME: "Em trai" },
  { CODE: 7, NAME: "Chị gái" },
  { CODE: 8, NAME: "Em gái" },
];

export const LEADER = [
  { CODE: 2, NAME: "Phó giám đốc" },
  { CODE: 3, NAME: "Trưởng phòng" },
  { CODE: 4, NAME: "Giám đốc" },
];

export const POSITION = [
  { CODE: 1, NAME: "Nhân viên" },
  { CODE: 2, NAME: "Phó giám đốc" },
  { CODE: 3, NAME: "Trưởng phòng" },
  { CODE: 4, NAME: "Giám đốc" },
  { CODE: 5, NAME: "Giám đốc 1" },
  { CODE: 6, NAME: "Giám đốc 2" },
  { CODE: 7, NAME: "Giám đốc 3" },
];

export const TYPE = [
  { CODE: 1, NAME: "Đào tạo" },
  { CODE: 2, NAME: "Quyền lợi" },
  { CODE: 3, NAME: "Thời gian" },
  { CODE: 4, NAME: "Quy trình" },
];

export const EMPLOYEE_APPBAR = {
  INFOMATION: {
    value: 0,
    name: "Thông tin nhân viên",
  },

  CERTIFICATE: {
    value: 1,
    name: "Thông tin văn bằng",
  },

  FAMILYRELATIONSHIP: {
    value: 2,
    name: "Quan hệ gia đình",
  },
};

export const DEVELOPMENT_APPBAR = {
  PENDING: {
    value: 0,
    name: "Chờ duyệt đăng ký/kết thúc",
  },

  SALARY: {
    value: 1,
    name: "Chờ duyệt tăng lương",
  },

  PROCESS: {
    value: 2,
    name: "Chờ duyệt thăng chức",
  },

  PROPOSAL: {
    value: 3,
    name: "Chờ duyệt tham mưu",
  },
};

export const APPROVED_APPBAR = {
  SALARY: {
    value: 0,
    name: "Tăng lương",
  },

  PROCESS: {
    value: 1,
    name: "Thăng chức",
  },

  PROPOSAL: {
    value: 2,
    name: "Tham mưu",
  },
};
