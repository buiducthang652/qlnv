import React from "react";
import { useDispatch } from "react-redux";
import {
  Dialog,
  Button,
  DialogActions,
  DialogTitle,
  DialogContent,
  Grid,
} from "@material-ui/core";
import { parse, format } from "date-fns";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { toast } from "react-toastify";
import { updateSalary } from "../../../redux/actions/SalaryAction";
import { updateProcess } from "../../../redux/actions/ProcessAction";
import { updateProposal } from "../../../redux/actions/ProposalAction";
import { updateEmployeeDelete } from "../../../redux/actions/EmployeeActions";
import "../../../../styles/views/_loadding.scss";
import "react-toastify/dist/ReactToastify.css";
import "../../../../styles/views/_style.scss";

toast.configure({
  autoClose: 1000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const ApproveDialog = ({
  open,
  setShowApproveDialog,
  handleShowRegistration,
  item,
  setItem,
  isPending,
  setShowSalaryDialog,
  setShowProcessDialog,
  setShowProposalDialog,
  setShowEndEmployeeDialog,
}) => {
  const dispatch = useDispatch();

  const handleFormSubmit = (e) => {
    if (setShowSalaryDialog) {
      dispatch(updateSalary(item));
      setShowSalaryDialog(false);
      setShowApproveDialog(false);
    } else if (setShowProcessDialog) {
      dispatch(updateProcess(item));
      setShowProcessDialog(false);
      setShowApproveDialog(false);
    } else if (setShowProposalDialog) {
      dispatch(updateProposal(item));
      setShowProposalDialog(false);
      setShowApproveDialog(false);
    } else {
      dispatch(updateEmployeeDelete(item));
      if (setShowEndEmployeeDialog) {
        setShowApproveDialog(false);
        setShowEndEmployeeDialog(false);
      } else {
        handleCloseApprove();
      }
    }
  };

  const handleCloseApprove = () => {
    if (!isPending) {
      handleShowRegistration(false, item, false);
    } else {
      setShowEndEmployeeDialog(false);
    }

    setShowApproveDialog(false);
  };

  const handleOnclickPending = () => {
    setItem({
      ...item,
      submitProfileStatus: "7",
    });
  };

  const handleOnclickDevelopment = () => {
    if (setShowSalaryDialog) {
      setItem({
        ...item,
        salaryIncreaseStatus: 3,
      });
    } else if (setShowProcessDialog) {
      setItem({
        ...item,
        processStatus: "3",
      });
    } else if (setShowProposalDialog) {
      setItem({
        ...item,
        proposalStatus: 3,
      });
    } else {
      setItem({
        ...item,
        submitProfileStatus: "3",
      });
    }
  };

  const handleDateChange = (event) => {
    const timestamp = parse(
      event.target.value,
      "yyyy-MM-dd",
      new Date()
    ).getTime();
    setItem({
      ...item,
      [event.target.name]: timestamp,
    });
  };

  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth={"sm"}
      fullWidth={true}
    >
      <ValidatorForm
        onSubmit={(e) => {
          handleFormSubmit(e);
        }}
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          Phê duyệt
        </DialogTitle>
        <DialogContent>
          <Grid container lg={12} md={12} sm={12} xs={12}>
            <Grid item lg={12} md={12} sm={12} xs={12}>
              <TextValidator
                className="w-100 mb-6"
                label={
                  <span className="font exper">
                    <span style={{ color: "red" }}> * </span>
                    Ngày duyệt
                  </span>
                }
                onChange={handleDateChange}
                type="date"
                name="appointmentDate"
                value={
                  item?.appointmentDate
                    ? format(new Date(item?.appointmentDate), "yyyy-MM-dd")
                    : format(new Date(), "yyyy-MM-dd")
                }
                InputLabelProps={{ shrink: true }}
                validators={["required"]}
                errorMessages={["Không được bỏ trống!"]}
                variant="outlined"
                size="small"
              />
            </Grid>
          </Grid>
        </DialogContent>

        <DialogActions className="justifyCenter">
          <Button
            variant="contained"
            color="primary"
            type="submit"
            onClick={() => {
              isPending ? handleOnclickPending() : handleOnclickDevelopment();
            }}
          >
            Phê duyệt
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => {
              setShowApproveDialog(false);
            }}
          >
            Hủy
          </Button>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
};

export default ApproveDialog;
