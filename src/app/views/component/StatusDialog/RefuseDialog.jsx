import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import {
  Dialog,
  Button,
  DialogActions,
  DialogTitle,
  DialogContent,
  Grid,
} from "@material-ui/core";
import { parse, format } from "date-fns";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "../../../../styles/views/_loadding.scss";
import "../../../../styles/views/_style.scss";
import "../../../../styles/views/_pending.scss";
import { updateSalary } from "../../../redux/actions/SalaryAction";
import { updateProcess } from "../../../redux/actions/ProcessAction";
import { updateProposal } from "../../../redux/actions/ProposalAction";
import { updateEmployeeDelete } from "../../../redux/actions/EmployeeActions";

toast.configure({
  autoClose: 1000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const RefuseDialog = ({
  open,
  setShowRefuseDialog,
  handleShowRegistration,
  item,
  setItem,
  isPending,
  setShowSalaryDialog,
  setShowProcessDialog,
  setShowProposalDialog,
  setShowEndEmployeeDialog,
}) => {
  const dispatch = useDispatch();

  const handleFormSubmit = (e) => {
    if (setShowSalaryDialog) {
      dispatch(updateSalary(item));
      setShowSalaryDialog(false);
      setShowRefuseDialog(false);
    } else if (setShowProcessDialog) {
      dispatch(updateProcess(item));
      setShowProcessDialog(false);
      setShowRefuseDialog(false);
    } else if (setShowProposalDialog) {
      dispatch(updateProposal(item));
      setShowProposalDialog(false);
      setShowRefuseDialog(false);
    } else {
      dispatch(updateEmployeeDelete(item));
      if (setShowEndEmployeeDialog) {
        setShowRefuseDialog(false);
        setShowEndEmployeeDialog(false);
      } else {
        handleCloseRefuse();
      }
    }
  };

  const handleCloseRefuse = () => {
    if (!isPending) {
      handleShowRegistration(false, item, false);
    } else {
      setShowEndEmployeeDialog(false);
    }
    setShowRefuseDialog(false);
  };

  const handleOnclickPending = () => {
    setItem({
      ...item,
      submitProfileStatus: "9",
    });
  };

  const handleOnclickDevelopment = () => {
    if (setShowSalaryDialog) {
      setItem({
        ...item,
        salaryIncreaseStatus: 5,
      });
    } else if (setShowProcessDialog) {
      setItem({
        ...item,
        processStatus: "5",
      });
    } else if (setShowProposalDialog) {
      setItem({
        ...item,
        proposalStatus: 5,
      });
    } else {
      setItem({
        ...item,
        submitProfileStatus: "5",
      });
    }
  };

  const handleInputChange = (event) => {
    setItem({
      ...item,
      [event.target.name]: event.target.value,
    });
  };

  const handleDateChange = (event) => {
    const timestamp = parse(
      event.target.value,
      "yyyy-MM-dd",
      new Date()
    ).getTime();
    setItem({
      ...item,
      [event.target.name]: timestamp,
    });
  };

  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth={"sm"}
      fullWidth={true}
    >
      <ValidatorForm
        onSubmit={(e) => {
          handleFormSubmit(e);
        }}
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          Từ chối
        </DialogTitle>

        <DialogContent className="pd-10">
          <Grid container lg={12} md={12} sm={12} xs={12} spacing={1}>
            <Grid item lg={12} md={12} sm={12} xs={12}>
              <TextValidator
                className="w-100 mb-6"
                label={
                  <span className="font exper">
                    <span style={{ color: "red" }}> * </span>
                    Ngày từ chối
                  </span>
                }
                onChange={handleDateChange}
                type="date"
                name="rejectionDate"
                value={
                  item?.rejectionDate
                    ? format(new Date(item?.rejectionDate), "yyyy-MM-dd")
                    : format(new Date(), "yyyy-MM-dd")
                }
                InputLabelProps={{ shrink: true }}
                validators={["required"]}
                errorMessages={["Không được bỏ trống!"]}
                variant="outlined"
                size="small"
              />
            </Grid>
            <Grid item lg={12} md={12} sm={12} xs={12}>
              <TextValidator
                multiline
                className="w-100"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Lý do từ chối
                  </span>
                }
                onChange={(event) => handleInputChange(event)}
                type="text"
                name={
                  isPending ? "reasonForRefuseEndProfile" : "reasonForRefusal"
                }
                value={
                  isPending
                    ? item?.reasonForRefuseEndProfile
                    : item?.reasonForRefusal
                }
                validators={["required"]}
                errorMessages={["Vui lòng điền dữ liệu."]}
                variant="outlined"
                size="small"
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions className="justifyCenter">
          <Button
            variant="contained"
            color="primary"
            type="submit"
            onClick={() => {
              isPending ? handleOnclickPending() : handleOnclickDevelopment();
            }}
          >
            Phê duyệt
          </Button>

          <Button
            variant="contained"
            color="secondary"
            onClick={() => {
              setShowRefuseDialog(false);
            }}
          >
            Hủy
          </Button>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
};

export default RefuseDialog;
