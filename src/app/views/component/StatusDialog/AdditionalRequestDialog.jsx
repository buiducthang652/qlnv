import React from "react";
import { useDispatch } from "react-redux";
import {
  Dialog,
  Button,
  DialogActions,
  DialogTitle,
  DialogContent,
  Grid,
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { toast } from "react-toastify";
import { updateEmployeeDelete } from "../../../redux/actions/EmployeeActions";
import { updateSalary } from "../../../redux/actions/SalaryAction";
import { updateProcess } from "../../../redux/actions/ProcessAction";
import { updateProposal } from "../../../redux/actions/ProposalAction";
import "react-toastify/dist/ReactToastify.css";
import "../../../../styles/views/_pending.scss";
import "../../../../styles/views/_loadding.scss";
import "../../../../styles/views/_style.scss";

toast.configure({
  autoClose: 1000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const AdditionalRequestDialog = ({
  open,
  setShowAdditionalRequestDialog,
  handleShowRegistration,
  item,
  setItem,
  isPending,
  setShowSalaryDialog,
  setShowProcessDialog,
  setShowProposalDialog,
  setShowEndEmployeeDialog,
}) => {
  const dispatch = useDispatch();

  const handleFormSubmit = (e) => {
    if (setShowSalaryDialog) {
      dispatch(updateSalary(item));
      setShowSalaryDialog(false);
      setShowAdditionalRequestDialog(false);
    } else if (setShowProcessDialog) {
      dispatch(updateProcess(item));
      setShowProcessDialog(false);
      setShowAdditionalRequestDialog(false);
    } else if (setShowProposalDialog) {
      dispatch(updateProposal(item));
      setShowProposalDialog(false);
      setShowAdditionalRequestDialog(false);
    } else {
      dispatch(updateEmployeeDelete(item));
      if (setShowEndEmployeeDialog) {
        setShowAdditionalRequestDialog(false);
        setShowEndEmployeeDialog(false);
      } else {
        handleCloseAdditional();
      }
    }
  };

  const handleCloseAdditional = () => {
    if (!isPending) {
      handleShowRegistration(false, item, false);
    } else {
      setShowEndEmployeeDialog(false);
    }
    setShowAdditionalRequestDialog(false);
  };

  const handleOnclickPending = () => {
    setItem({
      ...item,
      submitProfileStatus: "8",
    });
  };

  const handleOnclickDevelopment = () => {
    if (setShowSalaryDialog) {
      setItem({
        ...item,
        salaryIncreaseStatus: 4,
      });
    } else if (setShowProcessDialog) {
      setItem({
        ...item,
        processStatus: "4",
      });
    } else if (setShowProposalDialog) {
      setItem({
        ...item,
        proposalStatus: 4,
      });
    } else {
      setItem({
        ...item,
        submitProfileStatus: "4",
      });
    }
  };

  const handleInputChange = (event) => {
    setItem({
      ...item,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth={"sm"}
      fullWidth={true}
    >
      <ValidatorForm
        onSubmit={(e) => {
          handleFormSubmit(e);
        }}
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          Yêu cầu bổ sung
        </DialogTitle>

        <DialogContent className="pd-10">
          <Grid container lg={12} md={12} sm={12} xs={12}>
            <Grid item lg={12} md={12} sm={12} xs={12}>
              <TextValidator
                className="w-100"
                multiline
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Yêu cầu bổ sung
                  </span>
                }
                onChange={(event) => handleInputChange(event)}
                type="text"
                name={
                  isPending
                    ? "additionalRequestTermination"
                    : "additionalRequest"
                }
                value={
                  isPending
                    ? item?.additionalRequestTermination
                    : item?.additionalRequest
                }
                validators={["required"]}
                errorMessages={["Vui lòng điền dữ liệu."]}
                variant="outlined"
                size="small"
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions className="justifyCenter">
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              if (isPending) {
                handleOnclickPending();
              } else {
                handleOnclickDevelopment();
              }
            }}
            type="submit"
          >
            Phê duyệt
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => {
              setShowAdditionalRequestDialog(false);
            }}
          >
            Hủy
          </Button>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
};

export default AdditionalRequestDialog;
