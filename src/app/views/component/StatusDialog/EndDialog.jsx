import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Dialog,
  Button,
  DialogActions,
  DialogTitle,
  DialogContent,
  MenuItem,
  Grid,
} from "@material-ui/core";
import {
  ValidatorForm,
  SelectValidator,
} from "react-material-ui-form-validator";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import { parse } from "date-fns";
import { toast } from "react-toastify";
import { getAllLeader } from "app/redux/actions/LeaderActions";
import {
  updateEmployeeDelete,
  updateEmployees,
} from "app/redux/actions/EmployeeActions";
import "react-toastify/dist/ReactToastify.css";
import "../../../../styles/views/_loadding.scss";
import "../../../../styles/views/_style.scss";
import "../../../../styles/views/_pending.scss";

toast.configure({
  autoClose: 1000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const EndDialog = ({
  open,
  setEndDialog,
  employeeInfo,
  setEmployeeInfo,
  handleShouldEndEmployee,
  setShowApprovedDialog,
}) => {
  const dispatch = useDispatch();

  const { leaderList } = useSelector((state) => ({
    leaderList: state.leader.leaderList,
  }));

  const handleFormSubmit = (e) => {
    dispatch(updateEmployees(employeeInfo));
    setEndDialog(false);
    handleShouldEndEmployee();
    setShowApprovedDialog(false);
  };

  const handleInputChange = (event) => {
    setEmployeeInfo({
      ...employeeInfo,
      [event.target.name]: event.target.value,
    });
  };

  useEffect(() => {
    dispatch(getAllLeader());

    setEmployeeInfo({ ...employeeInfo, submitProfileStatus: "6" });
  }, []);

  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth={"sm"}
      fullWidth={true}
    >
      <ValidatorForm
        onSubmit={(e) => {
          handleFormSubmit(e);
        }}
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          Kết thúc
        </DialogTitle>

        <DialogContent className="pd-10">
          <Grid container lg={12} md={12} sm={12} xs={12}>
            <Grid item lg={12} md={12} sm={12} xs={12} className="">
              <SelectValidator
                className="w-100"
                variant="outlined"
                size="small"
                label={
                  <span className="font">
                    <span style={{ color: "red" }}> * </span>
                    Tên lãnh đạo
                  </span>
                }
                onChange={(e) => handleInputChange(e)}
                name="leaderId"
                value={employeeInfo?.leaderId || ""}
                validators={["required"]}
                errorMessages={["Vui lòng điền dữ liệu."]}
              >
                {leaderList && leaderList.length > 0 ? (
                  leaderList.map((employeeInfo) => (
                    <MenuItem key={employeeInfo.id} value={employeeInfo.id}>
                      {employeeInfo.leaderName}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem disabled>Không có</MenuItem>
                )}
              </SelectValidator>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions className="justifyCenter">
          <Button variant="contained" color="primary" type="submit">
            Trình lãnh đạo
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => {
              setEndDialog(false);
            }}
          >
            Hủy
          </Button>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
};

export default EndDialog;
