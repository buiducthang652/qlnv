import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getByCurrentLeaderProcess } from "../../../redux/actions/ProcessAction";
import { getEmployeeById } from "app/redux/actions/EmployeeActions";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { POSITION } from "../../component/constants";
import { formatDate } from "../../utilities/formatDate";
import "../../../../styles/views/_development.scss";
import ProcessDialog from "../../component/FormDialog/processDialog";
import TableDialog from "app/views/component/Table/TableDialog";
import { renderIconButton } from "app/views/utilities/renderButton";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const Process = ({ handleShowRegistration }) => {
  const dispatch = useDispatch();

  const [showProcessDialog, setShowProcessDialog] = useState(false);
  const [process, setProcess] = useState();

  const { processList, employee } = useSelector((state) => ({
    processList: state.process.processList,
    employee: state.employee.employee,
  }));

  useEffect(() => {
    dispatch(getByCurrentLeaderProcess());
  }, []);

  const columns = [
    {
      title: "STT",
      align: "center",
      width: "5%",
      minWidth: "40px",
      render: (rowData) => {
        return <>{rowData.tableData.id + 1}</>;
      },
    },
    {
      title: "Thao tác",
      field: "custom",
      align: "center",
      minWidth: "85px",
      render: (rowData) => (
        <>
          {renderIconButton("visibility", "secondary", () => {
            dispatch(getEmployeeById(rowData.employeeId));
            setProcess({ ...rowData });
            setShowProcessDialog(true);
          })}
        </>
      ),
    },
    {
      title: "Ngày hiệu lực",
      field: "startDate",
      align: "center",
      minWidth: "95px",
      render: (rowData) => {
        return <span>{formatDate(rowData.startDate)}</span>;
      },
    },
    {
      title: "Lần thứ",
      field: "times",
      align: "center",
      minWidth: "60px",
    },
    {
      title: "Ghi chú",
      field: "note",
      align: "center",
      minWidth: "200px",
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => {
        return <div className="ww ">{rowData.note || "Không có"}</div>;
      },
    },
    {
      title: "Chức vụ cũ",
      field: "currentPosition",
      align: "center",
      minWidth: "90px",
      render: (rowData) => {
        return (
          <div className="ww">
            {
              POSITION.find(
                (position) => position.CODE === rowData?.currentPosition
              )?.NAME
            }
          </div>
        );
      },
    },
    {
      title: "Chức vụ mới",
      field: "newPosition",
      align: "center",
      minWidth: "90px",
      render: (rowData) => {
        return (
          <div className="ww">
            {" "}
            {
              POSITION.find(
                (position) => position.CODE === rowData?.newPosition
              )?.NAME
            }
          </div>
        );
      },
    },
  ];

  return (
    <>
      <div>
        {showProcessDialog && (
          <ProcessDialog
            open={showProcessDialog}
            process={process}
            setProcess={setProcess}
            employeeInfo={employee}
            isManage={true}
            setShowProcessDialog={setShowProcessDialog}
            handleShowRegistration={handleShowRegistration}
          />
        )}
      </div>
      <TableDialog data={processList} columns={columns} />
    </>
  );
};

export default Process;
