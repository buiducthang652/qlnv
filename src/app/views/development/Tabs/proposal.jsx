import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getByCurrentLeaderProposal } from "../../../redux/actions/ProposalAction";
import { getEmployeeById } from "app/redux/actions/EmployeeActions";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { TYPE } from "../../component/constants";
import { formatDate } from "../../utilities/formatDate";
import "../../../../styles/views/_development.scss";
import ProposalDialog from "../../component/FormDialog/proposalDialog";
import TableDialog from "app/views/component/Table/TableDialog";
import { renderIconButton } from "app/views/utilities/renderButton";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const Proposal = ({ handleShowRegistration }) => {
  const dispatch = useDispatch();

  const [proposal, setProposal] = useState();
  const [showProposalDialog, setShowProposalDialog] = useState();

  const { proposalList, employee } = useSelector((state) => ({
    proposalList: state.proposal.proposalList,
    employee: state.employee.employee,
  }));

  useEffect(() => {
    dispatch(getByCurrentLeaderProposal());
  }, []);

  const columns = [
    {
      title: "STT",
      align: "center",
      width: "5%",
      minWidth: "40px",
      render: (rowData) => {
        return <>{rowData.tableData.id + 1}</>;
      },
    },
    {
      title: "Thao tác",
      field: "custom",
      align: "center",
      minWidth: "85px",
      render: (rowData) => (
        <>
          {renderIconButton("visibility", "secondary", () => {
            dispatch(getEmployeeById(rowData.employeeId));
            setProposal({ ...rowData });
            setShowProposalDialog(true);
          })}
        </>
      ),
    },
    {
      title: "Ngày đề xuất",
      field: "proposalDate",
      align: "center",
      minWidth: "95px",
      render: (rowData) => {
        return <span>{formatDate(rowData.startDate)}</span>;
      },
    },
    {
      title: "Nội dung đề xuất",
      field: "content",
      align: "center",
      minWidth: "200px",
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => {
        return <div className="ww">{rowData.content}</div>;
      },
    },
    {
      title: "Ghi chú",
      field: "note",
      align: "center",
      minWidth: "250px",
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => {
        return <div className="ww">{rowData.note || "Không có"}</div>;
      },
    },
    {
      title: "Loại",
      field: "type",
      align: "center",
      minWidth: "95px",
      render: (rowData) => {
        return (
          <div className="ww">
            {TYPE.find((type) => type.CODE === rowData.type)?.NAME}
          </div>
        );
      },
    },
  ];

  return (
    <>
      <div>
        {showProposalDialog && (
          <ProposalDialog
            open={showProposalDialog}
            proposal={proposal}
            setProposal={setProposal}
            employeeInfo={employee}
            isManage={true}
            setShowProposalDialog={setShowProposalDialog}
            handleShowRegistration={handleShowRegistration}
          />
        )}
      </div>
      <TableDialog data={proposalList} columns={columns} />
    </>
  );
};

export default Proposal;
