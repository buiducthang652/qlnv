import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Grid, TextField } from "@material-ui/core";
import TablePage from "../../component/Table/TablePage";
import { searchEmployees } from "../../../redux/actions/EmployeeActions";
import { ConfirmationDialog } from "egret";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  GENDER,
  TEAMS,
  SUBMIT_PROFILE_STATUS,
  EMPLOYEE_STATUS,
} from "../../component/constants";
import { formatDate } from "../../utilities/formatDate";
import "../../../../styles/views/_pending.scss";
import EndEmployeeDialog from "../../component/FormDialog/endEmployeeDialog";
import { renderIconButton } from "../../utilities/renderButton";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const EmployeePending = ({ handleShowRegistration }) => {
  const { employeeList, totalElements } = useSelector((state) => ({
    employeeList: state.employee.employeeList,
    totalElements: state.employee.totalElements,
  }));

  const dispatch = useDispatch();

  const [pageSize, setPageSize] = useState(10);
  const [page, setPage] = useState(0);
  const [keyword, setKeyword] = useState("");
  const [
    shouldNotificationConfirmationDialog,
    setShouldNotificationConfirmationDialog,
  ] = useState(false);
  const [item, setItem] = useState();
  const [showEndEmployeeDialog, setShowEndEmployeeDialog] = useState(false);

  const columns = [
    {
      title: "STT",
      align: "center",
      width: "5%",
      minWidth: "40px",
      render: (rowData) => {
        return <>{rowData.tableData.id + 1 + page * pageSize}</>;
      },
    },
    {
      title: "Thao tác",
      field: "custom",
      align: "center",
      minWidth: "95px",
      render: (rowData) => (
        <>
          {/* Duyệt nhân viên */}
          {EMPLOYEE_STATUS.VISIBILITY_PENDING_END.includes(
            rowData.submitProfileStatus
          ) &&
            renderIconButton("visibility", "secondary", () => {
              handleShowRegistration(rowData?.id, true);
            })}
          {/* Duyệt kết thúc */}
          {EMPLOYEE_STATUS.VISIBILITY_PENDING_REG.includes(
            rowData.submitProfileStatus
          ) &&
            renderIconButton("visibility", "secondary", () => {
              setItem(rowData);
              setShowEndEmployeeDialog(true);
            })}
          {EMPLOYEE_STATUS.NOTIFICATION_PENDING.includes(
            rowData.submitProfileStatus
          ) &&
            renderIconButton("notifications_active", "error", () => {
              handleShowNotification(rowData);
            })}
        </>
      ),
    },
    {
      title: "Họ và tên",
      field: "name",
      minWidth: "140px",
      cellStyle: {
        textAlign: "left",
      },
    },
    {
      title: "Giới tính",
      field: "gender",
      align: "center",
      minWidth: "75px",
      render: (rowData) => {
        return (
          <span>
            {GENDER.find((gender) => gender.CODE === rowData.gender)?.NAME}
          </span>
        );
      },
    },
    {
      title: "Nhóm",
      field: "team",
      align: "center",
      minWidth: "75px",
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => {
        return (
          <span>{TEAMS.find((team) => team.CODE === rowData.team)?.NAME}</span>
        );
      },
    },
    {
      title: "Email",
      field: "email",
      align: "center",
      minWidth: "217px",
      cellStyle: {
        textAlign: "left",
      },
    },
    {
      title: "Số điện thoại",
      field: "phone",
      align: "center",
      minWidth: "108px",
    },
    {
      title: "Ngày sinh",
      field: "dateOfBirth",
      align: "center",
      minWidth: "95px",
      render: (rowData) => {
        return <span>{formatDate(rowData.dateOfBirth)}</span>;
      },
    },
    {
      title: "Trạng thái",
      field: "submitProfileStatus",
      align: "center",
      minWidth: "145px",
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => {
        return (
          <span>
            {
              SUBMIT_PROFILE_STATUS.find(
                (submitProfileStatus) =>
                  submitProfileStatus.CODE.toString() ===
                  rowData.submitProfileStatus.toString()
              )?.NAME
            }
          </span>
        );
      },
    },
  ];

  useEffect(() => {
    const searchObject = {
      keyword,
      pageIndex: page + 1,
      pageSize,
      listStatus: EMPLOYEE_STATUS.EMPLOYEE_PENDING,
    };

    dispatch(searchEmployees(searchObject));
  }, [pageSize, page, keyword]);

  // Thông báo
  const handleShowNotification = (rowData) => {
    setItem(rowData);
    setShouldNotificationConfirmationDialog(
      !shouldNotificationConfirmationDialog
    );
  };

  return (
    <div className=" employee">
      <Grid container className="">
        <Grid container justifyContent="flex-end">
          <Grid item sx={12} className="pending_search">
            <TextField
              variant="standard"
              placeholder="Tìm kiếm"
              onChange={(event) => {
                setKeyword(event.target.value);
              }}
            />
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <div>
            {/* Thông báo trả về */}
            {shouldNotificationConfirmationDialog && (
              <ConfirmationDialog
                title={"Nội dung"}
                open={shouldNotificationConfirmationDialog}
                onConfirmDialogClose={() => {
                  handleShowNotification();
                }}
                text={item?.submitContent}
                No="Hủy"
              />
            )}

            {/* Đơn xin nghỉ việc */}
            {showEndEmployeeDialog && (
              <EndEmployeeDialog
                open={showEndEmployeeDialog}
                employeeInfo={item}
                setEmployeeInfo={setItem}
                setShowEndEmployeeDialog={setShowEndEmployeeDialog}
                handleShowRegistration={handleShowRegistration}
                isManage={true}
              />
            )}
          </div>
        </Grid>

        <TablePage
          data={employeeList}
          columns={columns}
          totalElements={totalElements}
          page={page}
          pageSize={pageSize}
          setPage={setPage}
          setPageSize={setPageSize}
        />
      </Grid>
    </div>
  );
};

export default EmployeePending;
