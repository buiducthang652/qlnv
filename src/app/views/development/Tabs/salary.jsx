import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getByCurrentLeaderSalary } from "../../../redux/actions/SalaryAction";
import { getEmployeeById } from "app/redux/actions/EmployeeActions";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { formatDate } from "../../utilities/formatDate";
import "../../../../styles/views/_development.scss";
import SalaryDialog from "app/views/component/FormDialog/salaryDialog";
import TableDialog from "app/views/component/Table/TableDialog";
import { renderIconButton } from "app/views/utilities/renderButton";
import { formatVND } from "app/views/utilities/formatCurrency";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const Salary = ({ handleShowRegistration }) => {
  const dispatch = useDispatch();

  const [showSalaryDialog, setShowSalaryDialog] = useState(false);
  const [salary, setSalary] = useState();

  const { salaryList, employee } = useSelector((state) => ({
    salaryList: state.salary.salaryList,
    employee: state.employee.employee,
  }));

  useEffect(() => {
    dispatch(getByCurrentLeaderSalary());
  }, []);

  const columns = [
    {
      title: "STT",
      align: "center",
      width: "5%",
      minWidth: "40px",
      render: (rowData) => {
        return <>{rowData.tableData.id + 1}</>;
      },
    },
    {
      title: "Thao tác",
      field: "custom",
      align: "center",
      minWidth: "85px",
      render: (rowData) => (
        <>
          {renderIconButton("visibility", "secondary", () => {
            dispatch(getEmployeeById(rowData.employeeId));
            setSalary({ ...rowData });
            setShowSalaryDialog(true);
          })}
        </>
      ),
    },
    {
      title: "Ngày hiệu lực",
      field: "startDate",
      align: "center",
      minWidth: "95px",
      render: (rowData) => {
        return <span>{formatDate(rowData.startDate)}</span>;
      },
    },
    {
      title: "Lần thứ",
      field: "times",
      align: "center",
      minWidth: "60px",
    },
    {
      title: "Ghi chú",
      field: "note",
      align: "center",
      minWidth: "200px",
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => {
        return <div className="ww">{rowData.note || "Không có"}</div>;
      },
    },
    {
      title: "Lý do",
      field: "reason",
      align: "center",
      minWidth: "200px",
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => {
        return <div className="ww">{rowData.reason || "Không có"}</div>;
      },
    },
    {
      title: "Lương cũ",
      field: "oldSalary",
      align: "center",
      minWidth: "90px",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => {
        return <div className="ww">{formatVND(rowData.oldSalary)}</div>;
      },
    },
    {
      title: "Lương mới",
      field: "newSalary",
      align: "center",
      minWidth: "90px",
      cellStyle: {
        textAlign: "right",
      },
      render: (rowData) => {
        return <div className="ww">{formatVND(rowData.newSalary)}</div>;
      },
    },
  ];

  return (
    <>
      <div>
        {showSalaryDialog && (
          <SalaryDialog
            open={showSalaryDialog}
            salary={salary}
            setSalary={setSalary}
            employeeInfo={employee}
            isManage={true}
            setShowSalaryDialog={setShowSalaryDialog}
            handleShowRegistration={handleShowRegistration}
          />
        )}
      </div>

      <TableDialog data={salaryList} columns={columns} />
    </>
  );
};

export default Salary;
