import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Breadcrumb } from "egret";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "../../../styles/views/_emp.scss";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import AppBar from "@material-ui/core/AppBar";
import Salary from "./Tabs/salary";
import Process from "./Tabs/process";
import Proposal from "./Tabs/proposal";
import RegistrationDialog from "../registration/RegistrationDialog";
import { getEmployeeById } from "app/redux/actions/EmployeeActions";
import EmployeePending from "./Tabs/EmployeePending";
import { DEVELOPMENT_APPBAR } from "../component/constants";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box py={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    "aria-controls": `nav-tabpanel-${index}`,
  };
}

const Development = () => {
  const { employee } = useSelector((state) => ({
    employee: state.employee.employee,
  }));

  const dispatch = useDispatch();

  const [value, setValue] = useState(0);
  const [pending, setPending] = useState(false);
  const [employeeInfo, setEmployeeInfo] = useState();
  const [shouldOpenRegistration, setShouldOpenRegistration] = useState(false);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleShowRegistration = (id, status) => {
    if (!shouldOpenRegistration) {
      dispatch(getEmployeeById(id));
    }

    setPending(status);
    setShouldOpenRegistration(!shouldOpenRegistration);
  };

  useEffect(() => {
    setEmployeeInfo({ ...employee });
  }, [employee]);

  return (
    <div className="m-sm-30 Development">
      <>
        {shouldOpenRegistration && (
          <RegistrationDialog
            open={shouldOpenRegistration}
            handleShowRegistration={handleShowRegistration}
            employeeInfo={employeeInfo}
            setEmployeeInfo={setEmployeeInfo}
            pending={pending}
            isView={false}
          />
        )}
      </>
      <div className="mb-sm-30">
        <Breadcrumb
          routeSegments={[
            {
              name: "Quản lý",
              path: "/dashboard/analytics",
            },
            { name: "Cập nhật diễn biến" },
          ]}
        />
      </div>
      <AppBar position="static" color="default">
        <Tabs
          variant="fullWidth"
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          aria-label="full width tabs example"
        >
          <Tab
            label={DEVELOPMENT_APPBAR.PENDING.name}
            {...a11yProps(DEVELOPMENT_APPBAR.PENDING.value)}
          />
          <Tab
            label={DEVELOPMENT_APPBAR.SALARY.name}
            {...a11yProps(DEVELOPMENT_APPBAR.SALARY.value)}
          />
          <Tab
            label={DEVELOPMENT_APPBAR.PROCESS.name}
            {...a11yProps(DEVELOPMENT_APPBAR.PROCESS.value)}
          />
          <Tab
            label={DEVELOPMENT_APPBAR.PROPOSAL.name}
            {...a11yProps(DEVELOPMENT_APPBAR.PROPOSAL.value)}
          />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={DEVELOPMENT_APPBAR.PENDING.value}>
        <EmployeePending handleShowRegistration={handleShowRegistration} />
      </TabPanel>
      <TabPanel value={value} index={DEVELOPMENT_APPBAR.SALARY.value}>
        <Salary handleShowRegistration={handleShowRegistration} />
      </TabPanel>
      <TabPanel value={value} index={DEVELOPMENT_APPBAR.PROCESS.value}>
        <Process handleShowRegistration={handleShowRegistration} />
      </TabPanel>
      <TabPanel value={value} index={DEVELOPMENT_APPBAR.PROPOSAL.value}>
        <Proposal handleShowRegistration={handleShowRegistration} />
      </TabPanel>
    </div>
  );
};

export default Development;
