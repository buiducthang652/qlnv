import { EgretLoadable } from "egret";
import ConstantList from "../../appConfig";
import { useTranslation, withTranslation, Trans } from "react-i18next";

const Development = EgretLoadable({
  loader: () => import("./Development"),
});

const ViewComponent = withTranslation()(Development);

const DevelopmentRoutes = [
  {
    path: ConstantList.ROOT_PATH + "employee_manager/development",
    exact: true,
    component: ViewComponent,
  },
];

export default DevelopmentRoutes;
