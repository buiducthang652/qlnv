import { EgretLoadable } from "egret";
import { authRoles } from "../../auth/authRoles";
import ConstantList from "../../appConfig";
import { withTranslation } from 'react-i18next';

const Analytics = EgretLoadable({
  loader: () => import("./Analytics")
});

const ViewAnalytics = withTranslation()(Analytics);
const dashboardRoutes = [
  {
    path: ConstantList.ROOT_PATH + "dashboard/analytics",
    component: ViewAnalytics,
    auth: authRoles.admin
  },

];

export default dashboardRoutes;
