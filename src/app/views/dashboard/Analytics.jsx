import React, { Component } from "react";
import {
  Grid,
} from "@material-ui/core";
import { Breadcrumb } from "egret";
import { Helmet } from "react-helmet";

class Dashboard1 extends Component {

  checkIsAdmin = () => {
    this.setState({ isAdmin: false })
    if (this.state.user != null && this.state.user.roles != null && this.state.user.roles.length > 0) {
      this.state.user.roles.forEach(element => {
        if (element.name == 'ROLE_ADMIN') {

          this.setState({ isAdmin: true })
          return true;
        }
      });
    }
  }

  render() {

    let { t } = this.props;

    return (
      <div className="analytics m-sm-30">
        <div className="mb-sm-30">
          <Helmet>
            <title>
              {t("Dashboard.dashboard")} | {t("web_site")}
            </title>
          </Helmet>

          <div className="mb-sm-30">
            <Breadcrumb routeSegments={[{ name: t("Dashboard.dashboard") }]} />
          </div>
        </div>
        <Grid container spacing={3}>
          <Grid item lg={12} md={12} sm={12} xs={12}>
            
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default Dashboard1;