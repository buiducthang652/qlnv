import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Dialog,
  Button,
  DialogActions,
  DialogTitle,
  DialogContent,
  AppBar,
  Tabs,
  Tab,
  Box,
  Typography,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import Draggable from "react-draggable";
import Paper from "@material-ui/core/Paper";
import "../../../styles/views/_loadding.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "../../../styles/views/_style.scss";
import "../../../styles/views/_customTable.scss";
import PropTypes from "prop-types";
import Infomation from "./Tabs/Infomation";
import Certificate from "./Tabs/Certificate";
import {
  saveEmployees,
  updateEmployees,
} from "../../redux/actions/EmployeeActions";
import FamilyRelationship from "./Tabs/FamilyRelationship";
import { EMPLOYEE_APPBAR } from "../component/constants";

toast.configure({
  autoClose: 1000,
  draggable: false,
  limit: 3,
});

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box py={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const EmployeeEditorDialog = ({
  open,
  item,
  setItem,
  handleShowRegistration,
  handleShowEditorDialog,
}) => {
  const dispatch = useDispatch();
  const [value, setValue] = useState(0);

  const { employee } = useSelector((state) => ({
    employee: state.employee.employee,
  }));

  const [employeeInfo, setEmployeeInfo] = useState(
    item?.id
      ? item
      : {
          image: "",
          employeeFamilyDtos: [],
          certificatesDto: [],
        }
  );

  const handleFormSubmit = (e) => {
    if (item?.id) {
      dispatch(updateEmployees(employeeInfo));
      handleShowEditorDialog(false, {});
    } else {
      dispatch(saveEmployees(employeeInfo));
    }
  };

  useEffect(() => {
    if (employee?.id) {
      setItem({ ...employee });
      setEmployeeInfo({ ...employee });
    }
  }, [employee]);

  return (
    <Dialog
      open={open}
      PaperComponent={PaperComponent}
      maxWidth={"lg"}
      fullWidth={true}
    >
      <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
        {item?.id ? "Cập nhật nhân viên" : "Tạo mới nhân viên"}
      </DialogTitle>

      <DialogContent>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={(event, newValue) => {
              if (item?.id) {
                setValue(newValue);
              } else {
                toast.warning("Phải thêm thông tin nhân viên.");
              }
            }}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            aria-label="full width tabs example"
          >
            <Tab
              label={EMPLOYEE_APPBAR.INFOMATION.name}
              {...a11yProps(EMPLOYEE_APPBAR.INFOMATION.value)}
            />
            <Tab
              label={EMPLOYEE_APPBAR.CERTIFICATE.name}
              {...a11yProps(EMPLOYEE_APPBAR.CERTIFICATE.value)}
            />
            <Tab
              label={EMPLOYEE_APPBAR.FAMILYRELATIONSHIP.name}
              {...a11yProps(EMPLOYEE_APPBAR.FAMILYRELATIONSHIP.value)}
            />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={EMPLOYEE_APPBAR.CERTIFICATE.value}>
          <Certificate id={employeeInfo?.id} />
        </TabPanel>
        <TabPanel
          value={value}
          index={EMPLOYEE_APPBAR.FAMILYRELATIONSHIP.value}
        >
          <FamilyRelationship id={employeeInfo?.id} />
        </TabPanel>
      </DialogContent>

      <ValidatorForm
        onSubmit={(e) => {
          handleFormSubmit(e);
        }}
      >
        <DialogContent>
          <TabPanel value={value} index={EMPLOYEE_APPBAR.INFOMATION.value}>
            <Infomation
              employeeInfo={employeeInfo}
              setEmployeeInfo={setEmployeeInfo}
            />
          </TabPanel>
        </DialogContent>

        <DialogActions className="justifyCenter">
          {item?.id && (
            <Button
              variant="contained"
              color=""
              onClick={() => {
                if (JSON.stringify(item) !== JSON.stringify(employeeInfo)) {
                  dispatch(updateEmployees(employeeInfo));
                }
                handleShowRegistration(true, employeeInfo, true);
              }}
            >
              Đăng ký
            </Button>
          )}

          {value === 0 && (
            <Button variant="contained" color="primary" type="submit">
              Lưu
            </Button>
          )}

          <Button
            variant="contained"
            color="secondary"
            onClick={() => {
              handleShowEditorDialog(false, {});
            }}
          >
            Hủy
          </Button>
        </DialogActions>
      </ValidatorForm>
    </Dialog>
  );
};

export default EmployeeEditorDialog;
