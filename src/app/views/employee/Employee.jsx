import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Grid, Button, TextField } from "@material-ui/core";
import {
  searchEmployees,
  deleteEmployees,
} from "../../redux/actions/EmployeeActions";
import EmployeeEditorDialog from "./EmployeeEditorDialog";
import RegistrationDialog from "../registration/RegistrationDialog";
import TablePage from "../component/Table/TablePage";
import { renderIconButton } from "../utilities/renderButton";
import { Breadcrumb, ConfirmationDialog } from "egret";
import { toast } from "react-toastify";
import {
  GENDER,
  TEAMS,
  SUBMIT_PROFILE_STATUS,
  EMPLOYEE_STATUS,
} from "../component/constants";
import { formatDate } from "../utilities/formatDate";
import "react-toastify/dist/ReactToastify.css";
import "../../../styles/views/_emp.scss";

toast.configure({
  autoClose: 2000,
  draggable: false,
  limit: 3,
});

const Employee = () => {
  const { employeeList, totalElements } = useSelector((state) => ({
    employeeList: state.employee.employeeList,
    totalElements: state.employee.totalElements,
  }));

  const dispatch = useDispatch();

  const [pageSize, setPageSize] = useState(10);
  const [page, setPage] = useState(0);
  const [keyword, setKeyword] = useState("");
  const [shouldOpenEditorDialog, setShouldOpenEditorDialog] = useState(false);
  const [shouldOpenConfirmationDialog, setShouldOpenConfirmationDialog] =
    useState(false);
  const [shouldOpenRegistration, setShouldOpenRegistration] = useState(false);
  const [
    shouldNotificationConfirmationDialog,
    setShouldNotificationConfirmationDialog,
  ] = useState(false);
  const [item, setItem] = useState();
  const [id, setId] = useState(0);
  const [isView, setIsView] = useState(false);
  const [pending, setPending] = useState(false);

  const columns = [
    {
      title: "STT",
      align: "center",
      width: "5%",
      minWidth: "40px",
      render: (rowData) => {
        return <>{rowData.tableData.id + 1 + page * pageSize}</>;
      },
    },
    {
      title: "Thao tác",
      field: "custom",
      align: "center",
      minWidth: "85px",
      render: (rowData) => (
        <div>
          {EMPLOYEE_STATUS.VISIBILITY_EMPLOYEE.includes(
            rowData.submitProfileStatus
          ) &&
            renderIconButton("visibility", "secondary", () => {
              handleShowRegistration(true, rowData, false);
            })}
          {EMPLOYEE_STATUS.EDIT_EMPLOYEE.includes(
            rowData.submitProfileStatus
          ) &&
            renderIconButton("edit", "primary", () => {
              handleShowEditorDialog(true, rowData);
            })}
          {EMPLOYEE_STATUS.DELETE.includes(rowData.submitProfileStatus) &&
            renderIconButton("delete", "error", () => {
              setId(rowData?.id);
              setShouldOpenConfirmationDialog(true);
            })}
          {EMPLOYEE_STATUS.NOTIFICATION_EMPLOYEE.includes(
            rowData.submitProfileStatus
          ) &&
            renderIconButton("notifications_active", "error", () => {
              handleShowNotification(rowData);
            })}
        </div>
      ),
    },
    {
      title: "Họ và tên",
      field: "name",
      align: "center",
      minWidth: "140px",
      cellStyle: {
        textAlign: "left",
      },
    },
    {
      title: "Giới tính",
      field: "gender",
      align: "center",
      minWidth: "75px",
      render: (rowData) => {
        return (
          <span>
            {GENDER.find((gender) => gender.CODE === rowData.gender)?.NAME}
          </span>
        );
      },
    },
    {
      title: "Nhóm",
      field: "team",
      align: "center",
      minWidth: "75px",
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => {
        return (
          <span>{TEAMS.find((team) => team.CODE === rowData.team)?.NAME}</span>
        );
      },
    },
    {
      title: "Email",
      field: "email",
      align: "center",
      minWidth: "217px",
      cellStyle: {
        textAlign: "left",
      },
    },
    {
      title: "Số điện thoại",
      field: "phone",
      align: "center",
      minWidth: "108px",
    },
    {
      title: "Ngày sinh",
      field: "dateOfBirth",
      align: "center",
      minWidth: "95px",
      render: (rowData) => {
        return <span>{formatDate(rowData.dateOfBirth)}</span>;
      },
    },
    {
      title: "Trạng thái",
      field: "submitProfileStatus",
      align: "center",
      minWidth: "145px",
      cellStyle: {
        textAlign: "left",
      },
      render: (rowData) => {
        return (
          <span>
            {
              SUBMIT_PROFILE_STATUS.find(
                (submitProfileStatus) =>
                  submitProfileStatus.CODE.toString() ===
                  rowData.submitProfileStatus.toString()
              )?.NAME
            }
          </span>
        );
      },
    },
  ];

  useEffect(() => {
    const searchObject = {
      keyword,
      pageIndex: page + 1,
      pageSize,
      listStatus: EMPLOYEE_STATUS.EMPLOYEE,
    };

    dispatch(searchEmployees(searchObject));
  }, [pageSize, page, keyword, dispatch]);

  const handleShowRegistration = (open, rowData, status) => {
    setIsView(status);
    setPending(false);
    setItem(rowData);
    setShouldOpenRegistration(open);
  };

  const handleShowEditorDialog = (status, rowData) => {
    setItem(rowData);
    setShouldOpenEditorDialog(status);
  };

  const handleShowNotification = (rowData) => {
    setItem(rowData);
    setShouldNotificationConfirmationDialog(
      !shouldNotificationConfirmationDialog
    );
  };

  const handleShowConfirmationDialog = () => {
    setItem();
    setShouldOpenConfirmationDialog(false);
  };

  return (
    <div className="m-sm-30 employee">
      <div className="mb-sm-30">
        <Breadcrumb
          routeSegments={[
            {
              name: "Quản lý",
              path: "/dashboard/analytics",
            },
            { name: "Tạo mới nhân viên" },
          ]}
        />
      </div>

      <Grid container className="">
        <Grid item xs={12} container justifyContent="space-between">
          <Grid item sx={6}>
            <Button
              className="mb-16 mr-16 align-bottom"
              variant="contained"
              color="primary"
              onClick={() => {
                handleShowEditorDialog(true, {});
              }}
            >
              Thêm mới
            </Button>
          </Grid>
          <Grid item sx={12} className="emp_search">
            <TextField
              variant="standard"
              placeholder="Tìm kiếm"
              onChange={(event) => {
                setPage(0);
                setKeyword(event?.target?.value);
              }}
            />
          </Grid>
        </Grid>
        <div>
          {/* Hồ sơ nhân viên */}
          {shouldOpenRegistration && (
            <RegistrationDialog
              open={shouldOpenRegistration}
              handleShowRegistration={handleShowRegistration}
              handleShowEditorDialog={handleShowEditorDialog}
              employeeInfo={item}
              setEmployeeInfo={setItem}
              pending={pending}
              isView={isView}
            />
          )}

          {/* Thêm sửa nhân viên */}
          {shouldOpenEditorDialog && (
            <EmployeeEditorDialog
              open={shouldOpenEditorDialog}
              item={item}
              setItem={setItem}
              handleShowRegistration={handleShowRegistration}
              handleShowEditorDialog={handleShowEditorDialog}
            />
          )}

          {/* Xóa nhân viên */}
          {shouldOpenConfirmationDialog && (
            <ConfirmationDialog
              title="Xác nhận"
              open={shouldOpenConfirmationDialog}
              onConfirmDialogClose={() => {
                handleShowConfirmationDialog();
              }}
              onYesClick={() => {
                dispatch(deleteEmployees(id));
                handleShowConfirmationDialog();
              }}
              text="Bạn có chắc chắn xóa"
              No="Hủy"
              Yes="Xóa"
            />
          )}

          {/* Thông báo trả về */}
          {shouldNotificationConfirmationDialog && (
            <ConfirmationDialog
              title={item?.submitProfileStatus === "4" ? "Bổ sung" : "Từ chối"}
              open={shouldNotificationConfirmationDialog}
              onConfirmDialogClose={() => {
                handleShowNotification();
              }}
              text={
                item?.submitProfileStatus === "4"
                  ? item?.additionalRequest || "Không có"
                  : item?.reasonForRejection || "Không có"
              }
              No="Hủy"
            />
          )}
        </div>

        <TablePage
          data={employeeList}
          columns={columns}
          totalElements={totalElements}
          page={page}
          pageSize={pageSize}
          setPage={setPage}
          setPageSize={setPageSize}
        />
      </Grid>
    </div>
  );
};

export default Employee;
