import React, { useEffect } from "react";
import { Button, Grid, MenuItem, Avatar } from "@material-ui/core";
import {
  TextValidator,
  SelectValidator,
} from "react-material-ui-form-validator";
import { GENDER, TEAMS } from "../../component/constants";
import { parse, format } from "date-fns";
import {
  addValidationRules,
  removeValidationRules,
} from "app/views/utilities/validatorRule";

const employeeCodeRegex = `NV${new Date().getFullYear() % 100}\\d{3}`;
const employeeCodeRegexLog = `NV${new Date().getFullYear() % 100}123`;

const Infomation = ({ employeeInfo, setEmployeeInfo }) => {
  useEffect(() => {
    addValidationRules();

    return () => {
      removeValidationRules();
    };
  }, []);

  const handleInputChange = (event) => {
    if (event?.target?.name === "image") {
      setEmployeeInfo({
        ...employeeInfo,
        image: URL.createObjectURL(event.target.files[0]),
        file: event.target.files[0],
      });
    } else {
      setEmployeeInfo({
        ...employeeInfo,
        [event.target.name]: event.target.value,
      });
    }
  };

  const handleDateChange = (event) => {
    const timestamp = parse(
      event.target.value,
      "yyyy-MM-dd",
      new Date()
    ).getTime();
    setEmployeeInfo({
      ...employeeInfo,
      [event.target.name]: timestamp,
    });
  };

  return (
    <Grid className="" container spacing={1}>
      <Grid container lg={4} md={4} sm={4} xs={12} className="employeeAvatar">
        <Avatar src={employeeInfo?.image || ""} />
        <Button variant="contained" color="primary" component="label">
          Tải lên
          <input
            accept="image/*"
            type="file"
            style={{ display: "none" }}
            name="image"
            onChange={(event) => {
              handleInputChange(event);
            }}
          />
        </Button>
      </Grid>
      <Grid container lg={8} md={8} sm={8} xs={12} spacing={1}>
        <Grid item lg={6} md={6} sm={6} xs={12}>
          <TextValidator
            className="w-100"
            label={
              <span className="font">
                <span style={{ color: "red" }}> * </span>
                Mã nhân viên
              </span>
            }
            onChange={(event) => handleInputChange(event)}
            type="text"
            name="code"
            value={employeeInfo?.code || ""}
            validators={["required", `matchRegexp:${employeeCodeRegex}`]}
            errorMessages={[
              "Vui lòng điền mã nhân viên.",
              `Mã nhân viên có dạng NVyyxxx (ví dụ: ${employeeCodeRegexLog}).`,
            ]}
            variant="outlined"
            size="small"
          />
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={12}>
          <TextValidator
            className="w-100"
            label={
              <span className="font">
                <span style={{ color: "red" }}> * </span>
                Tên nhân viên
              </span>
            }
            onChange={(event) => handleInputChange(event)}
            type="text"
            name="name"
            value={employeeInfo?.name || ""}
            validators={[
              "required",
              "matchRegexp:^[^\\d!@#$%^&*()_+{}[\\]:;<>,.?~\\\\/-]+$",
              "maxStringLength: 40",
            ]}
            errorMessages={[
              "Vui lòng điền dữ liệu.",
              "Tên nhân viên phải đúng định dạng.",
              "Tên không quá 40 ký tự.",
            ]}
            variant="outlined"
            size="small"
          />
        </Grid>
        <Grid item lg={4} md={4} sm={4} xs={12}>
          <SelectValidator
            className="w-100"
            variant="outlined"
            size="small"
            label={
              <span className="font">
                <span style={{ color: "red" }}> * </span>
                Giới tính
              </span>
            }
            onChange={(e) => handleInputChange(e)}
            name="gender"
            value={employeeInfo?.gender || null}
            validators={["required"]}
            errorMessages={["Vui lòng chọn giới tính."]}
          >
            {GENDER && GENDER.length > 0 ? (
              GENDER.map((gender) => (
                <MenuItem key={gender.CODE} value={gender.CODE}>
                  {gender.NAME}
                </MenuItem>
              ))
            ) : (
              <MenuItem disabled>Không có</MenuItem>
            )}
          </SelectValidator>
        </Grid>
        <Grid item lg={4} md={4} sm={4} xs={12}>
          <TextValidator
            className="w-100"
            label={
              <span className="font">
                <span style={{ color: "red" }}> * </span>
                Ngày sinh
              </span>
            }
            onChange={(event) => handleDateChange(event)}
            type="date"
            name="dateOfBirth"
            value={
              employeeInfo?.dateOfBirth
                ? format(new Date(employeeInfo?.dateOfBirth), "yyyy-MM-dd")
                : null
            }
            validators={["required", "is18OrOlder"]}
            errorMessages={[
              "Vui lòng chọn ngày sinh.",
              "Nhân viên phải trên 18 tuổi.",
            ]}
            variant="outlined"
            size="small"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>
        <Grid item lg={4} md={4} sm={4} xs={12}>
          <SelectValidator
            className="w-100"
            variant="outlined"
            size="small"
            label={
              <span className="font">
                <span style={{ color: "red" }}> * </span>
                Nhóm
              </span>
            }
            onChange={(event) => handleInputChange(event)}
            name="team"
            value={employeeInfo?.team || ""}
            validators={["required"]}
            errorMessages={["Vui lòng chọn dữ liệu."]}
          >
            {TEAMS && TEAMS.length > 0 ? (
              TEAMS.map((team) => (
                <MenuItem key={team.CODE} value={team.CODE}>
                  {team.NAME}
                </MenuItem>
              ))
            ) : (
              <MenuItem disabled>Không có</MenuItem>
            )}
          </SelectValidator>
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={12}>
          <TextValidator
            className="w-100"
            label={
              <span className="font">
                <span style={{ color: "red" }}> * </span>
                Email
              </span>
            }
            onChange={(event) => handleInputChange(event)}
            type="text"
            name="email"
            value={employeeInfo?.email || ""}
            validators={["required", "isEmail"]}
            errorMessages={[
              "Vui lòng điền dữ liệu.",
              "Vui lòng nhập đúng định dạng email.",
            ]}
            variant="outlined"
            size="small"
          />
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={12}>
          <TextValidator
            className="w-100"
            label={
              <span className="font">
                <span style={{ color: "red" }}> * </span>
                Số điện thoại
              </span>
            }
            onChange={(event) => handleInputChange(event)}
            type="text"
            name="phone"
            value={employeeInfo?.phone || ""}
            validators={["required", "matchRegexp:^\\+?[0-9]{10}$"]}
            errorMessages={[
              "Vui lòng điền dữ liệu.",
              "Số điện thoại phải đúng định dạng.",
            ]}
            variant="outlined"
            size="small"
          />
        </Grid>
        <Grid item lg={12} md={12} sm={12} xs={12}>
          <TextValidator
            className="w-100"
            label={
              <span className="font">
                <span style={{ color: "red" }}> * </span>
                Địa chỉ
              </span>
            }
            onChange={(event) => handleInputChange(event)}
            type="text"
            name="address"
            value={employeeInfo?.address || ""}
            validators={["required", "maxStringLength:225"]}
            errorMessages={["Vui lòng điền dữ liệu.", "Địa chỉ quá dài."]}
            variant="outlined"
            size="small"
          />
        </Grid>

        <Grid item lg={6} md={6} sm={6} xs={12}>
          <TextValidator
            className="w-100"
            label={
              <span className="font">
                <span style={{ color: "red" }}> * </span>
                Số CCCD/CMND
              </span>
            }
            onChange={(event) => handleInputChange(event)}
            type="text"
            name="citizenIdentificationNumber"
            value={employeeInfo?.citizenIdentificationNumber}
            validators={["required", "matchRegexp:^(\\d{9}|\\d{12})$"]}
            errorMessages={[
              "Vui lòng điền dữ liệu.",
              "Vui lòng điền đúng định dạng.",
            ]}
            variant="outlined"
            size="small"
          />
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={12}>
          <TextValidator
            className="w-100"
            label={
              <span className="font">
                <span style={{ color: "red" }}> * </span>
                Ngày cấp
              </span>
            }
            onChange={(event) => handleDateChange(event)}
            type="date"
            name="dateOfIssuanceCard"
            value={
              employeeInfo?.dateOfIssuanceCard
                ? format(
                    new Date(employeeInfo?.dateOfIssuanceCard),
                    "yyyy-MM-dd"
                  )
                : null
            }
            validators={["required", "isNotFutureDate"]}
            errorMessages={[
              "Vui lòng chọn ngày cấp.",
              "Ngày cấp không quá ngày hiện tại.",
            ]}
            variant="outlined"
            size="small"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>
        <Grid item lg={4} md={4} sm={4} xs={12}>
          <TextValidator
            className="w-100"
            label={
              <span className="font">
                <span style={{ color: "red" }}> * </span>
                Nơi cấp
              </span>
            }
            onChange={(event) => handleInputChange(event)}
            type="text"
            name="placeOfIssueCard"
            value={employeeInfo?.placeOfIssueCard || ""}
            validators={["required", "maxStringLength:225"]}
            errorMessages={["Vui lòng điền dữ liệu.", "Nơi cấp quá dài."]}
            variant="outlined"
            size="small"
          />
        </Grid>
        <Grid item lg={4} md={4} sm={4} xs={12}>
          <TextValidator
            className="w-100"
            label={
              <span className="font">
                <span style={{ color: "red" }}> * </span>
                Dân tộc
              </span>
            }
            onChange={(event) => handleInputChange(event)}
            type="text"
            name="ethnic"
            value={employeeInfo?.ethnic || ""}
            validators={["required", "maxStringLength:225"]}
            errorMessages={["Vui lòng điền dữ liệu.", "Tên dân tộc quá dài."]}
            variant="outlined"
            size="small"
          />
        </Grid>
        <Grid item lg={4} md={4} sm={4} xs={12}>
          <TextValidator
            className="w-100"
            label={
              <span className="font">
                <span style={{ color: "red" }}> * </span>
                Tôn giáo
              </span>
            }
            onChange={(event) => handleInputChange(event)}
            type="text"
            name="religion"
            value={employeeInfo?.religion || ""}
            validators={["required", "maxStringLength:225"]}
            errorMessages={["Vui lòng điền dữ liệu.", "Tôn giáo quá dài."]}
            variant="outlined"
            size="small"
          />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Infomation;
