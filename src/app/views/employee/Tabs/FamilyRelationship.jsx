import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Grid, MenuItem, Button, IconButton, Icon } from "@material-ui/core";
import TableDialog from "app/views/component/Table/TableDialog";
import { parse, format } from "date-fns";
import {
  ValidatorForm,
  TextValidator,
  SelectValidator,
} from "react-material-ui-form-validator";
import {
  getFamily,
  saveFamily,
  deleteFamily,
  updateFamily,
} from "../../../redux/actions/FamilyActions";
import { formatDate } from "../../utilities/formatDate";
import { GENDER, RELATIONSHIPS } from "../../component/constants";
import { ConfirmationDialog } from "egret";
import {
  addValidationRules,
  removeValidationRules,
} from "app/views/utilities/validatorRule";
import { renderIconButton } from "app/views/utilities/renderButton";

const initState = {
  name: "",
  gender: "",
  dateOfBirth: "",
  relationShip: "",
  citizenIdentificationNumber: "",
  address: "",
  email: "",
  phoneNumber: "",
};

const FamilyRelationship = ({ id }) => {
  const { familyList } = useSelector((state) => ({
    familyList: state.family.familyList,
  }));

  const dispatch = useDispatch();

  const [family, setFamily] = useState({
    ...initState,
  });
  const [item, setItem] = useState({});
  const [shouldOpenConfirmationDialog, setShouldOpenConfirmationDialog] =
    useState(false);

  useEffect(() => {
    dispatch(getFamily(id));

    addValidationRules();

    return () => {
      removeValidationRules();
    };
  }, []);

  const handleInputChange = (event) => {
    setFamily({
      ...family,
      [event.target.name]: event.target.value,
    });
  };

  const handleDateChange = (event) => {
    const timestamp = parse(
      event.target.value,
      "yyyy-MM-dd",
      new Date()
    ).getTime();
    setFamily({
      ...family,
      [event.target.name]: timestamp,
    });
  };

  const handleFormSubmit = (e) => {
    if (family?.id) {
      dispatch(updateFamily(item));
      setFamily({ ...initState });
    } else {
      dispatch(saveFamily(id, [item]));
      setFamily({ ...initState });
    }
  };

  const columns = [
    {
      title: "STT",
      align: "center",
      width: "5%",
      minWidth: "40px",
      render: (rowData) => {
        return <>{rowData.tableData.id + 1}</>;
      },
    },
    {
      title: "Thao tác",
      field: "custom",
      align: "center",
      width: "5%",
      minWidth: "75px",
      render: (rowData) => (
        <>
          {renderIconButton("edit", "primary", () => {
            setFamily(rowData);
          })}
          {renderIconButton("delete", "error", () => {
            setItem(rowData);
            setShouldOpenConfirmationDialog(true);
          })}
        </>
      ),
    },
    {
      title: "Họ và tên",
      field: "name",
      align: "center",
      minWidth: "140px",
      cellStyle: {
        textAlign: "left",
      },
    },
    {
      title: "Giới tính",
      field: "gender",
      align: "center",
      minWidth: "75px",
      render: (rowData) => {
        return (
          <span>
            {GENDER.find((gender) => gender.CODE === rowData.gender)?.NAME}
          </span>
        );
      },
    },
    {
      title: "Ngày sinh",
      field: "dateOfBirth",
      align: "center",
      minWidth: "100px",
      render: (rowData) => {
        return <span>{formatDate(rowData.dateOfBirth)}</span>;
      },
    },
    {
      title: "Mối quan hệ",
      field: "relationShip",
      align: "center",
      minWidth: "80px",
      render: (rowData) => {
        return (
          <span>
            {
              RELATIONSHIPS.find(
                (relationShip) => relationShip.CODE === rowData.relationShip
              )?.NAME
            }
          </span>
        );
      },
    },
    {
      title: "Địa chỉ",
      field: "address",
      align: "center",
      minWidth: "200px",
      maxWidth: "350px",
      cellStyle: {
        textAlign: "left",
      },
    },
    {
      title: "Email",
      field: "email",
      align: "center",
      minWidth: "140px",
      cellStyle: {
        textAlign: "left",
      },
    },
    {
      title: "Số điện thoại",
      field: "phoneNumber",
      align: "center",
      minWidth: "100px",
    },
  ];

  return (
    <>
      <ValidatorForm
        onSubmit={(e) => {
          handleFormSubmit(e);
        }}
      >
        {shouldOpenConfirmationDialog && (
          <ConfirmationDialog
            title={"Xác nhận"}
            open={shouldOpenConfirmationDialog}
            onConfirmDialogClose={() => {
              setItem({});
              setShouldOpenConfirmationDialog(false);
            }}
            onYesClick={() => {
              dispatch(deleteFamily(item));
              setShouldOpenConfirmationDialog(false);
              setFamily({ ...initState });
            }}
            text={"Bạn có chắc chắn xóa?"}
            Yes={"Xóa"}
            No={"Hủy"}
          />
        )}
        <Grid className="" container spacing={1}>
          <Grid item lg={4} md={4} sm={4} xs={12}>
            <TextValidator
              className="w-100"
              label={
                <span className="font">
                  <span style={{ color: "red" }}> * </span>
                  Họ tên
                </span>
              }
              onChange={(event) => handleInputChange(event)}
              type="text"
              name="name"
              value={family ? family?.name : ""}
              validators={[
                "required",
                "matchRegexp:^[^\\d!@#$%^&*()_+{}[\\]:;<>,.?~\\\\/-]+$",
                "maxStringLength: 40",
              ]}
              errorMessages={[
                "Vui lòng điền họ và tên.",
                "Họ và tên phải đúng định dạng.",
                "Tên không quá 40 ký tự.",
              ]}
              variant="outlined"
              size="small"
            />
          </Grid>
          <Grid item lg={2} md={2} sm={2} xs={12}>
            <SelectValidator
              className="w-100"
              variant="outlined"
              size="small"
              label={
                <span className="font">
                  <span style={{ color: "red" }}> * </span>
                  Giới tính
                </span>
              }
              onChange={(e) => handleInputChange(e)}
              name="gender"
              value={family?.gender}
              validators={["required"]}
              errorMessages={["Vui lòng chọn giới tính."]}
            >
              {GENDER && GENDER.length > 0 ? (
                GENDER.map((gender) => (
                  <MenuItem key={gender.CODE} value={gender.CODE}>
                    {gender.NAME}
                  </MenuItem>
                ))
              ) : (
                <MenuItem disabled>Không có</MenuItem>
              )}
            </SelectValidator>
          </Grid>
          <Grid item lg={2} md={2} sm={2} xs={12}>
            <TextValidator
              className="w-100"
              label={
                <span className="font">
                  <span style={{ color: "red" }}> * </span>
                  Ngày sinh
                </span>
              }
              onChange={(event) => handleDateChange(event)}
              type="date"
              name="dateOfBirth"
              value={
                family?.dateOfBirth
                  ? format(new Date(family?.dateOfBirth), "yyyy-MM-dd")
                  : ""
              }
              validators={["required", "isNotFutureDate"]}
              errorMessages={[
                "Vui lòng chọn ngày sinh.",
                "Ngày sinh không vượt quá ngày hiện tại.",
              ]}
              variant="outlined"
              size="small"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>
          <Grid item lg={2} md={2} sm={2} xs={12}>
            <TextValidator
              className="w-100"
              label={
                <span className="font">
                  <span style={{ color: "red" }}> * </span>
                  CMND/CCCD
                </span>
              }
              onChange={(event) => handleInputChange(event)}
              type="text"
              name="citizenIdentificationNumber"
              value={family ? family?.citizenIdentificationNumber : ""}
              validators={["required", "matchRegexp:^(\\d{9}|\\d{12})$"]}
              errorMessages={[
                "Vui lòng điền số căn cước.",
                "Vui lòng điền đúng định dạng.",
              ]}
              variant="outlined"
              size="small"
            />
          </Grid>
          <Grid item lg={2} md={2} sm={2} xs={12}>
            <SelectValidator
              className="w-100"
              variant="outlined"
              size="small"
              label={
                <span className="font">
                  <span style={{ color: "red" }}> * </span>
                  Mối quan hệ
                </span>
              }
              onChange={(e) => handleInputChange(e)}
              name="relationShip"
              value={family?.relationShip}
              validators={["required"]}
              errorMessages={["Vui lòng chọn mối quan hệ."]}
            >
              {RELATIONSHIPS && RELATIONSHIPS.length > 0 ? (
                RELATIONSHIPS.map((relationship) => (
                  <MenuItem key={relationship.CODE} value={relationship.CODE}>
                    {relationship.NAME}
                  </MenuItem>
                ))
              ) : (
                <MenuItem disabled>Không có</MenuItem>
              )}
            </SelectValidator>
          </Grid>
          <Grid item lg={3} md={3} sm={3} xs={12}>
            <TextValidator
              className="w-100"
              label={
                <span className="font">
                  <span style={{ color: "red" }}> * </span>
                  Email
                </span>
              }
              onChange={(event) => handleInputChange(event)}
              type="text"
              name="email"
              value={family ? family?.email : ""}
              validators={["required", "isEmail"]}
              errorMessages={[
                "Vui lòng điền email.",
                "Vui lòng nhập đúng định dạng email.",
              ]}
              variant="outlined"
              size="small"
            />
          </Grid>
          <Grid item lg={2} md={2} sm={2} xs={12}>
            <TextValidator
              className="w-100"
              label={
                <span className="font">
                  <span style={{ color: "red" }}> * </span>
                  Số điện thoại
                </span>
              }
              onChange={(event) => handleInputChange(event)}
              type="text"
              name="phoneNumber"
              value={family ? family?.phoneNumber : ""}
              validators={["required", "matchRegexp:^\\+?[0-9]{10}$"]}
              errorMessages={[
                "Vui lòng điền số điện thoại.",
                "Số điện thoại phải đúng định dạng.",
              ]}
              variant="outlined"
              size="small"
            />
          </Grid>
          <Grid item lg={5} md={5} sm={5} xs={12}>
            <TextValidator
              className="w-100"
              label={
                <span className="font">
                  <span style={{ color: "red" }}> * </span>
                  Địa chỉ
                </span>
              }
              onChange={(event) => handleInputChange(event)}
              type="text"
              name="address"
              value={family ? family?.address : ""}
              validators={["required", "maxStringLength:225"]}
              errorMessages={["Vui lòng điền địa chỉ.", "Địa chỉ quá dài."]}
              variant="outlined"
              size="small"
            />
          </Grid>

          <Grid item lg={2} md={2} sm={2} xs={12}>
            <Button
              className="my"
              variant="contained"
              color="primary"
              type="submit"
              onClick={() => {
                setItem({
                  ...family,
                });
              }}
            >
              Lưu
            </Button>
            <Button
              className="my"
              variant="contained"
              color="secondary"
              onClick={() => {
                setFamily({ ...initState });
              }}
            >
              Hủy
            </Button>
          </Grid>
        </Grid>
      </ValidatorForm>
      <TableDialog data={familyList} columns={columns} />
    </>
  );
};

export default FamilyRelationship;
