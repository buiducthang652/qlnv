import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Grid, Button, IconButton, Icon } from "@material-ui/core";
import TableDialog from "app/views/component/Table/TableDialog";
import { ConfirmationDialog } from "egret";
import { parse, format } from "date-fns";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import {
  getCertificate,
  saveCertificate,
  deleteCertificate,
  updateCertificate,
} from "../../../redux/actions/CertificateActions";
import { formatDate } from "../../utilities/formatDate";
import {
  addValidationRules,
  removeValidationRules,
} from "app/views/utilities/validatorRule";
import { renderIconButton } from "app/views/utilities/renderButton";

const initState = {
  certificateName: "",
  issueDate: "",
  content: "",
  field: "",
};

const Certificate = ({ id }) => {
  const { certificateList } = useSelector((state) => ({
    certificateList: state.certificate.certificateList,
  }));

  const dispatch = useDispatch();

  const [certificate, setCertificate] = useState({ ...initState });
  const [item, setItem] = useState({});
  const [shouldOpenConfirmationDialog, setShouldOpenConfirmationDialog] =
    useState(false);

  useEffect(() => {
    dispatch(getCertificate(id));

    addValidationRules();

    return () => {
      removeValidationRules();
    };
  }, []);

  const handleInputChange = (event) => {
    setCertificate({
      ...certificate,
      [event.target.name]: event.target.value,
    });
  };

  const handleDateChange = (event) => {
    const timestamp = parse(
      event.target.value,
      "yyyy-MM-dd",
      new Date()
    ).getTime();
    setCertificate({
      ...certificate,
      [event.target.name]: timestamp,
    });
  };

  const handleFormSubmit = (e) => {
    if (certificate?.id) {
      dispatch(updateCertificate(item));
      setCertificate({ ...initState });
    } else {
      dispatch(saveCertificate(id, [item]));
      setCertificate({ ...initState });
    }
  };

  const columns = [
    {
      title: "STT",
      align: "center",
      width: "5%",
      minWidth: "40px",
      render: (rowData) => {
        return <>{rowData.tableData.id + 1}</>;
      },
    },
    {
      title: "Thao tác",
      field: "custom",
      align: "center",
      minWidth: "75px",
      width: "5%",
      render: (rowData) => (
        <>
          {renderIconButton("edit", "primary", () => {
            setCertificate(rowData);
          })}
          {renderIconButton("delete", "error", () => {
            setItem(rowData);
            setShouldOpenConfirmationDialog(true);
          })}
        </>
      ),
    },
    {
      title: "Tên văn bằng",
      field: "certificateName",
      align: "center",
      minWidth: "140px",
      cellStyle: {
        textAlign: "left",
      },
    },
    {
      title: "Ngày cấp",
      field: "issueDate",
      align: "center",
      minWidth: "115px",
      render: (rowData) => {
        return <span>{formatDate(rowData.issueDate)}</span>;
      },
    },
    {
      title: "Nội dung",
      field: "content",
      align: "center",
      minWidth: "200px",
      cellStyle: {
        textAlign: "left",
      },
    },
    {
      title: "Lĩnh vực",
      field: "field",
      align: "center",
      minWidth: "140px",
      cellStyle: {
        textAlign: "left",
      },
    },
  ];

  return (
    <>
      <ValidatorForm
        onSubmit={(e) => {
          handleFormSubmit(e);
        }}
      >
        {shouldOpenConfirmationDialog && (
          <ConfirmationDialog
            title={"Xác nhận"}
            open={shouldOpenConfirmationDialog}
            onConfirmDialogClose={() => {
              setItem({});
              setShouldOpenConfirmationDialog(false);
            }}
            onYesClick={() => {
              dispatch(deleteCertificate(item));
              setShouldOpenConfirmationDialog(false);
              setCertificate({ ...initState });
            }}
            text={"Bạn có chắc chắn xóa?"}
            Yes={"Xóa"}
            No={"Hủy"}
          />
        )}
        <Grid className="" container spacing={1}>
          <Grid item lg={2} md={2} sm={2} xs={12}>
            <TextValidator
              className="w-100"
              label={
                <span className="font">
                  <span style={{ color: "red" }}> * </span>
                  Tên văn bằng
                </span>
              }
              onChange={(event) => handleInputChange(event)}
              type="text"
              name="certificateName"
              value={certificate ? certificate?.certificateName : ""}
              validators={["required", "maxStringLength: 100"]}
              errorMessages={[
                "Vui lòng điền tên văn bằng.",
                "Tên văn bằng quá dài.",
              ]}
              variant="outlined"
              size="small"
            />
          </Grid>
          <Grid item lg={2} md={2} sm={2} xs={12}>
            <TextValidator
              className="w-100"
              label={
                <span className="font">
                  <span style={{ color: "red" }}> * </span>
                  Ngày cấp
                </span>
              }
              onChange={(event) => handleDateChange(event)}
              type="date"
              name="issueDate"
              value={
                certificate?.issueDate
                  ? format(new Date(certificate?.issueDate), "yyyy-MM-dd")
                  : ""
              }
              validators={["required", "isNotFutureDate"]}
              errorMessages={[
                "Vui lòng chọn ngày cấp.",
                "Không quá ngày hiện tại.",
              ]}
              variant="outlined"
              size="small"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>
          <Grid item lg={3} md={3} sm={3} xs={12}>
            <TextValidator
              className="w-100"
              label={
                <span className="font">
                  <span style={{ color: "red" }}> * </span>
                  Nội dung
                </span>
              }
              onChange={(event) => handleInputChange(event)}
              type="text"
              name="content"
              value={certificate ? certificate?.content : ""}
              validators={["required", "maxStringLength: 225"]}
              errorMessages={[
                "Vui lòng không để trống.",
                "Nội dung văn bằng quá dài.",
              ]}
              variant="outlined"
              size="small"
            />
          </Grid>
          <Grid item lg={3} md={3} sm={3} xs={12}>
            <TextValidator
              className="w-100"
              label={
                <span className="font">
                  <span style={{ color: "red" }}> * </span>
                  Lĩnh vực
                </span>
              }
              onChange={(event) => handleInputChange(event)}
              type="text"
              name="field"
              value={certificate ? certificate?.field : ""}
              validators={["required", "maxStringLength: 100"]}
              errorMessages={[
                "Vui lòng không để trống.",
                "Lĩnh vực không quá dài.",
              ]}
              variant="outlined"
              size="small"
            />
          </Grid>
          <Grid item lg={2} md={2} sm={2} xs={12}>
            <Button
              className="my"
              variant="contained"
              color="primary"
              type="submit"
              onClick={() => {
                setItem({
                  ...certificate,
                });
              }}
            >
              Lưu
            </Button>
            <Button
              className="my"
              variant="contained"
              color="secondary"
              onClick={() => {
                setCertificate({ ...initState });
              }}
            >
              Hủy
            </Button>
          </Grid>
        </Grid>
      </ValidatorForm>

      <TableDialog data={certificateList} columns={columns} />
    </>
  );
};

export default Certificate;
