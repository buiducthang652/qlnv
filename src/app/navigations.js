import ConstantList from "./appConfig";

const createDashboardNavigation = () => ({
  name: "Dashboard.dashboard",
  icon: "dashboard",
  path: ConstantList.ROOT_PATH + "dashboard/analytics",
  isVisible: true,
});

const createEmployeeNavigation = () => ({
  name: "Nhân viên",
  isVisible: true,
  icon: "engineering",
  children: [
    {
      name: "Tạo mới nhân viên",
      isVisible: true,
      path: ConstantList.ROOT_PATH + "employee_manager/employee",
      icon: "keyboard_arrow_right",
    },
    {
      name: "Danh sách nhân viên",
      isVisible: true,
      path: ConstantList.ROOT_PATH + "employee_manager/approved",
      icon: "keyboard_arrow_right",
    },
    {
      name: "Kết thúc nhân viên",
      isVisible: true,
      path: ConstantList.ROOT_PATH + "employee_manager/endEmployee",
      icon: "keyboard_arrow_right",
    },
  ],
});

const createManageNavigation = () => ({
  name: "Quản lý",
  isVisible: true,
  icon: "dashboard",
  children: [
    {
      name: "Cập nhật diễn biến",
      isVisible: true,
      path: ConstantList.ROOT_PATH + "employee_manager/development",
      icon: "keyboard_arrow_right",
    },
    {
      name: "Danh sách nhân viên",
      isVisible: true,
      path: ConstantList.ROOT_PATH + "employee_manager/approvedManage",
      icon: "keyboard_arrow_right",
    },
    {
      name: "Kết thúc nhân viên",
      isVisible: true,
      path: ConstantList.ROOT_PATH + "employee_manager/endEmployeeManage",
      icon: "keyboard_arrow_right",
    },
  ],
});

export const user_navigation = [
  createDashboardNavigation(),
  createEmployeeNavigation(),
];
export const manage_navigation = [
  createDashboardNavigation(),
  createManageNavigation(),
];
export const navigations = [createDashboardNavigation()];
